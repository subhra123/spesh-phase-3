<?php $page_name=basename($_SERVER['SCRIPT_FILENAME']); 
$adminchk=mysql_fetch_array(mysql_query("select * from ".PREFIX."admin where admin_id='".$_SESSION["login_id"]."'"));
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Medilink-Global - MacAdmin</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">


  <!-- Stylesheets -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Font awesome icon -->
  <link rel="stylesheet" href="css/font-awesome.min.css"> 
  <!-- jQuery UI -->
  <link rel="stylesheet" href="css/jquery-ui.css"> 
  <!-- Calendar -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <!-- prettyPhoto -->
  <link rel="stylesheet" href="css/prettyPhoto.css">  
  <!-- Star rating -->
  <link rel="stylesheet" href="css/rateit.css">
  <!-- Date picker -->
  <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
  <!-- CLEditor -->
  <link rel="stylesheet" href="css/jquery.cleditor.css"> 
  <!-- Data tables -->
  <link rel="stylesheet" href="css/jquery.dataTables.css"> 
  <!-- Bootstrap toggle -->
  <link rel="stylesheet" href="css/jquery.onoff.css">
  <!-- Main stylesheet -->
  <link href="css/style.css" rel="stylesheet">
  <!-- Widgets stylesheet -->
  <link href="css/widgets.css" rel="stylesheet"> 
  <!-- Drop Down -->
  <link href="css/chosen.min.css" rel='stylesheet'>
  <!-- jQuery -->
<script src="js/respond.min.js" type="text/javascript"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/refresh.js" type="text/javascript"></script>
<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
function showonlybuilder(thechosenone) {
	  var newboxesfraser = document.getElementsByTagName("div");
	  for(var x=0; x<newboxesfraser.length; x++) {
			name = newboxesfraser[x].getAttribute("class");
			if (name == 'newboxesfraser-2') {
				  if (newboxesfraser[x].id == thechosenone) {
						if (newboxesfraser[x].style.display == 'block') {
							  newboxesfraser[x].style.display = 'none';
						}
						else {
							  newboxesfraser[x].style.display = 'block';
						}
				  }else {
						newboxesfraser[x].style.display = 'none';
				  }
			}
	  }
}


//-->
</script>
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <![endif]-->
  <!-- Favicon -->
<link rel="shortcut icon" href="img/favicon/favicon.png">

</head>

<body>
<header style="padding:5px;">
    <div class="container">
      <div class="row">

        <!-- Logo section -->
        <div class="col-md-2">
          <!-- Logo. -->
          <div class="logo">
            <h1><a href="dashboard.php"><img src="img/logo.png" border="0" name="medilink"></a></h1>
          </div>
          <!-- Logo ends -->
        </div>
        <!-- Button section -->
        <div class="col-md-9" style="text-align:center; padding-left:43px;" id="chatrcd">
          <!-- Buttons -->
        </div>

        <!-- Data section -->
		
        <!-- Search form -->
     <!--   <form class="navbar-form navbar-left" role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>-->
		<!--  Links --> 
       
        <ul class="nav navbar-nav pull-right">
          <li class="dropdown pull-right">            
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <i class="fa fa-user"></i><?php echo $_SESSION["name"];  ?><b class="caret"></b> 
              <input type="hidden" name="activepage" id="activepage" >             
            </a>
            
            <!-- Dropdown menu -->
            <ul class="dropdown-menu">
              <li><a href="manageprofile.php"><i class="fa fa-user"></i> Profile</a></li>
              <li><a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
          </li>
          
        </ul>

      </div>
    </div>
  </header>
<!--top_menu_bar_div-->
<div class="menubgcolordiv">
	<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<img src="img/menutab.png" border="0" name="menu">
		  </button>
	</div>
    <div class="topmenu_navbar_bottom">
		<nav class="collapse navbar-collapse topmenumarginleft">
			<ul class="nav navbar-nav">
			<li><a class="<?php if($page_name=="dashboard.php") { echo "active"; }?>" href="dashboard.php">Dashboard</a></li>
			<?php if($adminchk['usertype']=='admin'){?>
			<li class="dropdown"><a href="#" class="dropdown-toggle <?php  if($page_name=="manageuser.php" || $page_name=="createuser.php" || strpos($_SERVER['REQUEST_URI'],'edituser.php')) { echo "active"; }?>" data-toggle="dropdown">User <b class="caret"></b></a>
			<ul class="dropdown-menu">
				  <li><a class="<?php  if($page_name=="manageuser.php" || strpos($_SERVER['REQUEST_URI'],'edituser.php')) { echo "active"; }?>" href="manageuser.php">Manage User</a></li>
				  <li><a href="createuser.php" class="<?php  if($page_name=="createuser.php") { echo "active"; }?>">Create User</a></li>
			</ul>                              
			</li> 
			<?php } if($adminchk['usertype']=='member'){?>
			<li><a href="chat.php" class="<?php  if(strpos($_SERVER['REQUEST_URI'],'chat.php')) { echo "active"; }?>">Chat</a></li>
			<li><a href="contactus.php" class="<?php  if($page_name=="contactus.php") { echo "active"; }?>">Contact</a></li>                     
			<li><a href="healthadvisory.php" class="<?php  if($page_name=="healthadvisory.php") { echo "active"; }?>">Health Advisory</a></li>
			<?php } ?>
			<li><a href="alert.php" class="<?php  if($page_name=="alert.php") { echo "active"; }?>">Alert</a></li>
			<li><a href="resetpass.php" class="<?php  if($page_name=="resetpass.php") { echo "active"; }?>">Reset Password</a></li>
			<li><a href="resetversion.php" class="<?php  if($page_name=="resetversion.php") { echo "active"; }?>">OS Version</a></li>
			<li><a href="versioninfo.php" class="<?php  if($page_name=="versioninfo.php") { echo "active"; }?>">Version Info</a></li>
			</ul>
		</nav>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
<script src="js/active_status.js" type="text/javascript"></script>
<!--end_top_menu_bar_div-->
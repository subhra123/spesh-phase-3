<?php
session_start();
error_reporting(0);
define("PREFIX","db_");
$con = new mysqli("localhost", "root", "bbsr1234", "medilink");		//oditek.in
if ($con->connect_error)die("Connection failed: ");	

function db_insert($table,$values, $columns=array()){
	global $con;
	if($table=="" || $values=="")return false;
	$columnstr=$valuestr="";
	if(count($columns)>0){
		$columnstr=implode(",", $columns);
		$columnstr="(".$columnstr.")";
	}
	foreach($values as $key=>$val){
		if($valuestr !=""){
			$valuestr.=", '".$con->real_escape_string($val)."'";
		}else{
			$valuestr="'".$con->real_escape_string($val)."'";
		}
	}
	$sql = "INSERT INTO ".$table." ".$columnstr." VALUES (".$valuestr.")";
	if($con->query($sql)){
		return $con->insert_id;
	}else{
		return false;
	}
}
function db_get_data($str){
	global $con;
	$rows=array();
	$result=$con->query($str);
	while($row=$result->fetch_assoc()){
		$rows[]=$row;
	}
	return $rows;
}

function db_update($table,$columns, $values, $condition=""){
	global $con;
	if($table=="" || $columns=="" || $values=="" || count($columns) != count($values)){
		return false;
	}
	$updatestr="";
	for($i=0;$i<count($columns);$i++){
		if($updatestr==""){
			$updatestr=" SET ".$columns[$i]." = '".$con->real_escape_string($values[$i])."'";
		}else{
			$updatestr.=" , ".$columns[$i]." = '".$con->real_escape_string($values[$i])."'";
		}
	}
	if($condition !=""){
		$condition=" WHERE ".$condition;
	}
	$sql = "UPDATE ".$table." ".$updatestr.$condition;
	if($con->query($sql)){
		return $con->affected_rows;
	}else{
		return false;
	}
}
function prepare_param($str){
	global $con;
	return $con->real_escape_string($str);
}
function db_delete($table,$condition=""){
	global $con;
	if($table=="")return false;
	if($condition !=""){
		$condition=" WHERE ".$condition;
	}
	$sql = "DELETE FROM ".$table." ".$condition;
	if($con->query($sql)){
		return $con->affected_rows;
	}else{
		return false;
	}
}
function sendMail($to,$from,$subject,$msg_body,$reply_to=''){
	$headers = "MIME-Version: 1.0" . "\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\n";
	$headers .= 'From: '.$from . "\n";
	if($reply_to!=''){
		$headers .= 'Reply-To: '.$reply_to . "\n";
	}
	$id=mail($to,$subject,$msg_body,$headers);
	if($id){
		return 1;
	}else{
		return 0;
	}
}
?>
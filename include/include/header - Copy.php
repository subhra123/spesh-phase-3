<?php $page_name=basename($_SERVER['SCRIPT_FILENAME']); 
$adminchk=mysql_fetch_array(mysql_query("select * from ".PREFIX."admin where admin_id='".$_SESSION["login_id"]."'"));
$userrights=$_SESSION["userrights"];

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Medilink-Global - MacAdmin</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">


  <!-- Stylesheets -->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">
    <link href="icons/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	
    <!-- PAGE LEVEL PLUGIN STYLES -->
    <!-- THEME STYLES - Include these on every page. -->
  <!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">-->
  <link rel="stylesheet" type="text/css" media="all" href="css/angular-datepicker.css" />
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style1.css" rel="stylesheet">
  <!-- Font awesome icon -->
  <link rel="stylesheet" href="css/font-awesome.min.css"> 
  <!-- jQuery UI -->
  <link rel="stylesheet" href="css/jquery-ui.css"> 
  <!-- Calendar -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <!-- prettyPhoto -->
  <link rel="stylesheet" href="css/prettyPhoto.css">  
  <!-- Star rating -->
  <link rel="stylesheet" href="css/rateit.css">
  <!-- Date picker -->
  <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
  <!-- CLEditor -->
  <link rel="stylesheet" href="css/jquery.cleditor.css"> 
  <!-- Data tables -->
  <link rel="stylesheet" href="css/jquery.dataTables.css"> 
  <!-- Bootstrap toggle -->
  <link rel="stylesheet" href="css/jquery.onoff.css">
  <!-- Main stylesheet -->
  <link href="css/style.css" rel="stylesheet">
  <!-- Widgets stylesheet -->
  <link href="css/widgets.css" rel="stylesheet"> 
  <!-- Drop Down -->
  <link href="css/chosen.min.css" rel='stylesheet'>
  <!-- Select -->
  <link href="css/bootstrap-multiselect.css" rel='stylesheet'>
  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  
  
<script src="js/respond.min.js" type="text/javascript"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/refresh.js" type="text/javascript"></script>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
<!--
function showonlybuilder(thechosenone) {
	  var newboxesfraser = document.getElementsByTagName("div");
	  for(var x=0; x<newboxesfraser.length; x++) {
			name = newboxesfraser[x].getAttribute("class");
			if (name == 'newboxesfraser-2') {
				  if (newboxesfraser[x].id == thechosenone) {
						if (newboxesfraser[x].style.display == 'block') {
							  newboxesfraser[x].style.display = 'none';
						}
						else {
							  newboxesfraser[x].style.display = 'block';
						}
				  }else {
						newboxesfraser[x].style.display = 'none';
				  }
			}
	  }
}


//-->
</script>
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <![endif]-->
  <!-- Favicon -->
<link rel="shortcut icon" href="img/favicon/favicon.png">

</head>

<body>
<header style="padding:5px;">
    <div class="container">
      <div class="row">

        <!-- Logo section -->
        <div class="col-md-2">
          <!-- Logo. -->
          <div class="logo">
            <h1><a href="dashboard.php"><img src="img/logo.png" border="0" name="medilink"></a></h1>
          </div>
          <!-- Logo ends -->
        </div>
        <!-- Button section -->
        <div class="col-md-9" style="text-align:center; padding-left:43px;" id="chatrcd">
          <!-- Buttons -->
        </div>

        <!-- Data section -->
		
        <!-- Search form -->
     <!--   <form class="navbar-form navbar-left" role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>-->
		<!--  Links --> 
       
        <ul class="nav navbar-nav pull-right">
          <li class="dropdown pull-right">            
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <i class="fa fa-user"></i><?php echo $adminchk['name'];  ?><b class="caret"></b> 
              <input type="hidden" name="activepage" id="activepage" >             
            </a>
            
            <!-- Dropdown menu -->
            <ul class="dropdown-menu">
              <li><a href="manageprofile.php"><i class="fa fa-user"></i> Profile</a></li>
              <li><a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
          </li>
          
        </ul>

      </div>
    </div>
  </header>
<!--top_menu_bar_div-->
<div class="menubgcolordiv">
	<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<img src="img/menutab.png" border="0" name="menu">
		  </button>
	</div>
    <div class="topmenu_navbar_bottom">
		<nav class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
			<li><a class="<?php if($page_name=="dashboard.php") { echo "active"; }?>" href="dashboard.php">Dashboard</a></li>
<?php 	if($adminchk['usertype']=='admin'){?>
			<li class="dropdown"><a href="#" class="dropdown-toggle <?php  if($page_name=="manageuser.php" || $page_name=="createuser.php" || strpos($_SERVER['REQUEST_URI'],'edituser.php')) { echo "active"; }?>" data-toggle="dropdown">User <b class="caret"></b></a>
			<ul class="dropdown-menu">
				  <li><a class="<?php  if($page_name=="manageuser.php" || strpos($_SERVER['REQUEST_URI'],'edituser.php')) { echo "active"; }?>" href="manageuser.php">Manage User</a></li>
				  <li><a href="createuser.php" class="<?php  if($page_name=="createuser.php") { echo "active"; }?>">Create User</a></li>
			</ul>                              
			</li> 
<?php 	} 
		if(in_array("Chat",$userrights)){
			$class=(strpos($_SERVER['REQUEST_URI'],'chat.php'))?'class="active"':'';
			echo'<li><a href="chat.php" '.$class.'>Mobile Chat</a></li>';
		}
		if(in_array("Alert",$userrights)){
			$class=(strpos($_SERVER['REQUEST_URI'],'alert.php'))?'class="active"':'';
			echo'<li><a href="alert.php" '.$class.'>Alert</a></li>';
		}
		/*if(in_array("Reset Password",$userrights)){
			$class=(strpos($_SERVER['REQUEST_URI'],'resetpass.php'))?'class="active"':'';
			echo'<li><a href="resetpass.php" '.$class.'>Reset Password</a></li>';
		}*/
		if(in_array("Health Tips",$userrights)){
			$class=(strpos($_SERVER['REQUEST_URI'],'healthtips.php'))?'class="active"':'';
			echo'<li><a href="healthtips.php" '.$class.'>Health Tips</a></li>';
		}
		if(in_array("Content",$userrights)){
			$class=(strpos($_SERVER['REQUEST_URI'],'contentlisting.php') || strpos($_SERVER['REQUEST_URI'],'newcontent.php'))?'class="active"':'';
			echo'<li><a href="contentlisting.php" '.$class.'>Content</a></li>';
		}
		if(in_array("Social",$userrights)){
			$class=(strpos($_SERVER['REQUEST_URI'],'socialmediacrm.php') || strpos($_SERVER['REQUEST_URI'],'socialmediacrm.php'))?'class="active"':'';
			echo'<li><a href="socialmediacrm.php" '.$class.'>Social Media</a></li>';
		}
		if(in_array("OS Version",$userrights) || in_array("Info",$userrights)){
		?>
		<li class="dropdown"><a href="#" class="dropdown-toggle <?php  if($page_name=="resetversion.php" || $page_name=="versioninfo.php") { echo "active"; }?>" data-toggle="dropdown">OS Version<b class="caret"></b></a>
			<ul class="dropdown-menu">
				<?php if(in_array("OS Version",$userrights)){ ?>
				  <li><a class="<?php  if($page_name=="resetversion.php") { echo "active"; }?>" href="resetversion.php">Reset Version</a></li>
				<?php }if(in_array("Info",$userrights)){ ?>
				  <li><a href="versioninfo.php" class="<?php  if($page_name=="versioninfo.php") { echo "active"; }?>">Version Info</a></li>
				<?php } ?>
			</ul>                              
		</li>
		<?php } ?>
		 <li><a href="liveadmin.php" class="<?php if($page_name=="liveadmin.php") { echo "active"; }?>">Portal Chat</a></li>
		 <?php if(in_array("Contact",$userrights) || in_array("Health Advisory",$userrights)){ ?>
		<li class="dropdown"><a href="#" class="dropdown-toggle <?php  if($page_name=="contactus.php" || $page_name=="healthadvisory.php") { echo "active"; }?>" data-toggle="dropdown">Customer Service <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<?php if(in_array("Contact",$userrights)){ ?>
				  <li><a class="<?php  if($page_name=="contactus.php") { echo "active"; }?>" href="contactus.php">Contact</a></li>
				<?php }if(in_array("Health Advisory",$userrights)){ ?>
				  <li><a href="healthadvisory.php" class="<?php  if($page_name=="healthadvisory.php") { echo "active"; }?>">Health Advisory</a></li>
				<?php } ?>
			</ul>                              
		</li>
		<?php } ?> 
		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Deals/Products<b class="caret"></b></a>
			<ul class="dropdown-menu">				
			  <?php 
				if($_SESSION['usertype']!='supplier')
				{
				if(in_array("addsupplier",$userrights)){
					echo '<li><a href="deal.php#/supplier">Add Supplier</a></li>';
				}if(in_array("addgst",$userrights)){
					echo '<li><a ui-sref="gst">Add GST</a></li>';
				}
				}
				?>
			  <li><a href="deal.php#/catagory">Add Category</a></li>
			  <li><a  href="deal.php#/newproduct">Add New Product</a></li>				
			  <li><a href="deal.php#/productdata">Add Product Stock</a></li>
			  <li><a  href="deal.php#/productview">View Product</a></li>
			</ul>                              
		</li>
		<?php if(in_array("E-Vouchers",$userrights)){ ?>
		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">E-Vouchers<b class="caret"></b></a>
			<ul class="dropdown-menu">				
			  <li><a href="product.php#/dashboard">Dashboard</a></li>	
              <li><a href="product.php#/merchant">Add merchant</a></li>			
			  <li><a href="product.php#/voucherCode">Generate Voucher Code</a></li>
			  <li><a href="product.php#/voucherList">E-Voucher List</a></li>
			</ul>                              
		</li>
		<?php } if(in_array("App Complain",$userrights) || in_array("Web Complain",$userrights) || in_array("E-mail Complain",$userrights)){?>
		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Complaints<b class="caret"></b></a>
			<ul class="dropdown-menu">	
			  <li><a href="complaints.php">View Complains</a></li>			  				
			  <li><a href="addpayor.php">Add Payor</a></li>
			  <li><a href="addcorporate.php">Add Corporate</a></li>
			  <li><a href="addprovider.php">View Provider</a></li>
			  <li><a href="addCRMCategory.php">Add CRM Category</a></li>
			  <li><a href="addcrmstatus.php">Add CRM Status</a></li>
			</ul>                              
		</li>
		<?php } ?>
		</nav>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
 <script src="js/active_status.js" type="text/javascript"></script>
<!--end_top_menu_bar_div-->

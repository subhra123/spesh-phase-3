<?php
require_once '../include/dbconfig.php';
$id=$_GET['a_i'];
$d_id=decrypt($id);
if(isset($_POST["userbtnsubmit"])){
  $sqlchk=mysqli_query($connect,"select * from db_app_user where appuser_id='".$d_id."' ");
  if (mysqli_num_rows($sqlchk) > 0) {
    $row=mysqli_fetch_array($sqlchk);
    $exp_date=$row['exp_time'];
    $currentDate=date('Y-m-d H:i:s');
    $to_time = strtotime($currentDate);
    $from_time = strtotime($exp_date);
    $diff=round(abs($to_time - $from_time) / 60,2);
    if ($diff > 120) {
      //$message="This link has already expired. Please reset your password again.";
      echo "<script>alert('This link has already expired. Please reset your password again.');</script>";
     // echo json_encode($message);
    }else{
      $password=$_POST['password'];
      $hashed_pass=password_hash($password, PASSWORD_BCRYPT);
      $qry = "UPDATE db_app_user SET password='$hashed_pass' WHERE appuser_id='$d_id'";
      $sql=mysqli_query($connect,$qry);
      if ($sql) {
        //$message="Your password has been reset successfully.";
        echo "<script>alert('Your password has been reset successfully.');</script>";
       // echo json_encode($message);
      }
    }
  }else{
    //$message="Invalid user";
    echo "<script>alert('Invalid user');</script>";
    //echo json_encode($message);
  }
}
function encrypt($id){
  $key=md5('SpeshOditek', true);
  $id = base_convert($id, 10, 36); // Save some space
  $data = mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $id, 'ecb');
  $data = bin2hex($data);
  return $data;
}
function decrypt($encrypted_id)
{
  $key=md5('SpeshOditek', true);
  $data = pack('H*', $encrypted_id); // Translate back to binary
  $data = mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $data, 'ecb');
  $data = base_convert($data, 36, 10);
  return $data;
}
?>
<!DOCTYPE html>
<html>

<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">
    <link href="../icons/font-awesome/css/font-awesome.min.css" rel="stylesheet">
   <link href="../css/style.css" rel="stylesheet">
    <script type="text/javascript">
        function validatePassForm(){
          var s=document.frmfeed;
          if (s.password.value=='') {
            alert('Please enter the password');
            return false;
          }else if (s.con_password.value=='') {
            alert('Please enter the password again');
            return false;
          }else if (s.password.value != s.con_password.value) {
            alert('Password did not match');
            return false;
          }else{

          }
        }
    </script>
</head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center login-header" style="padding:40px 0px;">

      </div>
      <!--/span-->
    </div>
    <!--/row-->
    <div class="col-md-6 marginautodiv floatnonediv well">
      <div class="totalaligndiv">
        <div class="alert alert-info text-center">
          <img src="../img/logo.png" style="width:100px; height:auto" />
          <BR> Please enter your Password to reset again.
        </div>
        <form name="frmfeed" id="frmfeed" enctype="multipart/form-data" method="post" onSubmit="return validatePassForm();">
          <fieldset>
            <div class="input-group input-group-lg">
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>
              <input type="password" name="password" id="txtname" class="form-control" placeholder="Password">
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="input-group input-group-lg">
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>
              <input type="password" name="con_password" id="txtpwd" class="form-control" placeholder="Confirm Password">
            </div>

            <div class="clearfix"></div>
            <br>
            <div class="input-prepend">
            </div>
            <div class="clearfix"></div>
            <p class="text-center tbpaddingdiv2">
            <button type="submit" class="btn btn-primary" name="userbtnsubmit" id="btnsubmit" style="width:270px;">Reset Your Password</button>
            </p>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</body>

</html>
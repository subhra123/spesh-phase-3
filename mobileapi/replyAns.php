<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Spesh</title>
</head>

<body>
<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%">
    <tbody>
        <tr>
            <td style="padding:0in 0in 0in 0in">
                <div align="center">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;background:#4CAF50">
                        <tbody>
                            <tr style="height:23.25pt">
                                <td colspan="3" style="padding:0in 0in 0in 0in;height:23.25pt">
                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                </td>
                            </tr>
                            <tr style="height:3.75pt">
                                <td colspan="3" style="padding:0in 0in 0in 0in;height:3.75pt">
                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                </td>
                            </tr>
                            <tr>
                                <td width="32" style="width:24.0pt;padding:0in 0in 0in 0in">
                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                </td>
                                <td style="padding:0in 0in 0in 0in">
                                    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;background:white">
                                        <tbody>
                                            <tr style="height:15.75pt">
                                                <td colspan="3" style="padding:0in 0in 0in 0in;height:15.75pt">
                                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0in 0in 0in 0in">
                                                    <table border="0" cellpadding="0">
                                                        <tbody>
                                                            <tr style="height:29.25pt">
                                                                <td width="20" style="width:15.0pt;padding:.75pt .75pt .75pt .75pt;height:29.25pt">
                                                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                                                </td>
                                                                <td style="padding:.75pt .75pt .75pt .75pt;height:29.25pt">
                                                                    <p class="MsoNormal"><span style="color:#00aeef"><a href="javascript:void(0)" target="_blank"><span style="color:#00aeef;text-decoration:none"><img src="http://45.79.90.53/portal/img/logo.png" border="0" width="125" height="125" class="medilink"></span></a>&nbsp;<u></u><u></u></span>
                                                                    </p>
                                                                </td>
                                                                <td width="497" style="width:372.75pt;padding:.75pt .75pt .75pt .75pt;height:29.25pt">
                                                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td style="padding:0in 0in 0in 0in"></td>
                                                <td style="padding:0in 0in 0in 0in"></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0in 0in 0in 0in">
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%">
                                                        <tbody>
                                                            <tr>
                                                                <td width="58" style="width:43.5pt;padding:0in 0in 0in 0in">
                                                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                                                </td>
                                                                <td style="padding:0in 0in 0in 0in">
                                                                    <p><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><br><br>Dear <b><?php echo $name; ?></b>, <br><br>
																	<br><br><?php echo $message; ?>
																		<br><br>
                                                                        <br>At your service,
                                                                        <br><b>Team Spesh</b>
                                                                        <br>
                                                                        <br><em><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"> You have received this notification because you have either subscribed to it, or are involved in it.</span></em> <u></u><u></u></span>
                                                                    </p>
                                                                </td>
                                                                <td width="58" style="width:43.5pt;padding:0in 0in 0in 0in">
                                                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td style="padding:0in 0in 0in 0in"></td>
                                                <td style="padding:0in 0in 0in 0in"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="32" style="width:24.0pt;padding:0in 0in 0in 0in">
                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                </td>
                            </tr>
                            <tr style="height:22.5pt">
                                <td style="padding:0in 0in 0in 0in;height:22.5pt">
                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                </td>
                                <td style="background:white;padding:0in 0in 0in 0in;height:22.5pt">
                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                </td>
                                <td style="padding:0in 0in 0in 0in;height:22.5pt">
                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                </td>
                            </tr>
                            <tr style="height:8.25pt">
                                <td colspan="3" style="padding:0in 0in 0in 0in;height:8.25pt">
                                    <p class="MsoNormal" align="center" style="text-align:center">&nbsp;<u></u><u></u></p>
                                </td>
                            </tr>
                            <tr style="height:16.5pt">
                                <td colspan="3" style="padding:0in 0in 0in 0in;height:16.5pt">
                                    <p class="MsoNormal">&nbsp;<u></u><u></u></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </tbody>
</table>
</body>
</html>

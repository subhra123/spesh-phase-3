<?php
if(isset($_FILES['file'])){
	// print_r($_FILES['file']);
	 for($i=0;$i<count($_FILES['file']['name']);$i++){
		// header("HTTP/1.0 401 Unauthorized");
		// print_r($_FILES['file']);
		 $file_name = $_FILES['file']['name'][$i]['image'];
         $file_size =$_FILES['file']['size'][$i]['image'];
         $file_tmp =$_FILES['file']['tmp_name'][$i]['image'];
         $file_type=$_FILES['file']['type'][$i]['image'];   
         $file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
         $extensions = array("jpeg","jpg","png","pdf","ppt","docx");        
         if(in_array($file_ext,$extensions )=== false){
		      header("HTTP/1.0 401 Unauthorized");
              $errors[]="image extension not allowed, please choose a JPEG or PNG file.";
			  print_r($errors);
			  return;
         }
         if($file_size > 2097152){
		    header("HTTP/1.0 401 Unauthorized");
            $errors[]='File size cannot exceed 2 MB';
			print_r($errors);
			return;
         }               
         if(empty($errors)==true){
            move_uploaded_file($file_tmp,"../upload/".$file_name);
            echo " uploaded file: " . "upload/" . $file_name.$file_size;
         }else{
            print_r($errors);
         }
	 }
}else{
	$errors= array();
	header("HTTP/1.0 401 Unauthorized");
    $errors[]="No file found";
    print_r($errors);
}
?>
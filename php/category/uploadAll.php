<?php
require_once '../../include/dbconfig.php'; 
if(isset($_FILES['file'])){
	$image_name_array = array();
	$prefix_array[0] = "Identity";
	$prefix_array[1] = "REG";
	$prefix_array[2] = "BIKE1";
	$prefix_array[3] = "BIKE2";
	$status=0;
	// print_r($_FILES['file']);
	 for($i=0;$i<count($_FILES['file']['name']);$i++){
		// header("HTTP/1.0 401 Unauthorized");
		// print_r($_FILES['file']);
		 $file_name = $_FILES['file']['name'][$i]['image'];
         $file_size =$_FILES['file']['size'][$i]['image'];
         $file_tmp =$_FILES['file']['tmp_name'][$i]['image'];
         $file_type=$_FILES['file']['type'][$i]['image'];   
         $file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
         $extensions = array("jpeg","jpg","png","pdf","ppt","docx");        
         if(in_array($file_ext,$extensions )=== false){
		      header("HTTP/1.0 401 Unauthorized");
              $errors[]="image extension not allowed, please choose a JPEG or PNG file.";
			  print_r($errors);
			  return;
         }
         if($file_size > 2097152){
		    header("HTTP/1.0 401 Unauthorized");
            $errors[]='File size cannot exceed 2 MB';
			print_r($errors);
			return;
         }
		 
         if(empty($errors)==true){
			 	$prefix = $prefix_array[$i];
			 	$today = date("YmdHis");
			 	$file_name = $prefix."_".$today.rand(1000,9999).".".$file_ext; 
             	$image_name_array[$i] = $file_name;
				move_uploaded_file($file_tmp,"../../upload/".$file_name);
            	$status=1;
				//$success[]='123';
				//print_r($success);
			 //echo " uploaded file: " . "upload/" . $file_name.$file_size;
         }else{
            print_r($errors);
         }
		 
	 }
	 if($status == 1)
	 {
	 		$qry ='INSERT INTO db_image_temp (image1,image2,image3,image4) values ("'.$image_name_array[0].'","'.$image_name_array[1].'","'.$image_name_array[2].'","'.$image_name_array[3].'" )';
			$qry_res = mysql_query($qry);
			$id = mysql_insert_id();
		 	$success = $id;
		 	print_r($success);
	 }
	
}else{
	$errors= array();
	header("HTTP/1.0 401 Unauthorized");
    $errors[]="No file found";
    print_r($errors);
}
?>
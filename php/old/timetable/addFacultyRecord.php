<?php
require_once '../../include/dbconfig.php'; 
$output = array();

$dept_id = $_SESSION["admin_dept_id"];

$session_id= $_POST['session_id'];
$day_id= $_POST['day_id'];
$time_id= $_POST['time_id'];
$table_id= $_POST['table_id'];
$faculty_id= $_POST['faculty_id'];

if($faculty_id != 0 && $session_id != 0 )
{
	/////////////// check if faculty is already assigned in same time slot and day or not...
	$qry = "select db_time_table.table_id , db_course.course_name , db_semester.semester , db_section.section_name ";
	$qry .="from  db_time_table ";
	$qry .="left join db_time_table_info on db_time_table.table_id= db_time_table_info.id ";
	$qry .="left join db_course on  db_time_table_info.course_id= db_course.course_id ";
	$qry .="left join db_semester on  db_time_table_info.semester_id= db_semester.semester_id ";
	$qry .="left join db_section on  db_time_table_info.section_id= db_section.section_id ";
	$qry .="where db_time_table_info.session_id = '".$session_id."' and db_time_table.day_id='".$day_id."' and db_time_table.time_id='".$time_id."' and db_time_table.faculty_id = '".$faculty_id."' and db_time_table.table_id != '".$table_id."' ";
	
	//echo $qry;
	$result = mysqli_query($connect, $qry);
	
	$Num = mysqli_num_rows($result);
	if($Num > 0)
	{
		$row =mysqli_fetch_assoc($result);
		$row_id = $row['table_id'];
		$course_name = $row['course_name'];
		$semester = $row['semester'];
		$section_name = $row['section_name'];
		
		$error = "Selected faculty is already assigned in ".$course_name." : Semester ".$semester." : ".$section_name;
		
		$output["results"] = 0;
		$output["error"] = $error;
		print json_encode($output);
		return;
	}

}

/////////////

$result = mysqli_query($connect, "select *  from db_time_table where table_id='".$table_id."' and day_id='".$day_id."' and time_id='".$time_id."' ");
	
$Num = mysqli_num_rows($result);
if(!$Num)
{
	$qry ='INSERT INTO db_time_table (table_id,day_id,time_id,faculty_id) values ("'.$table_id.'","'.$day_id.'","'.$time_id.'","'.$faculty_id.'" )';
	//echo $qry; 
	$qry_res = mysqli_query($connect,$qry);
 	$output["results"] = 1;
}
else
{
	$row =mysqli_fetch_assoc($result);
	$row_id = $row['id'];
	$qry ='UPDATE db_time_table set  faculty_id="'.$faculty_id.'" where id="'.$row_id.'" ';
$qry_res = mysqli_query($connect,$qry);
	$output["results"] = 1;
}

print json_encode($output);

?>


<?php
require_once '../../include/dbconfig.php'; 
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$colg_id=$request->colg_id;
$dept_id=$request->dept_id;
$user_id=$request->user_id;
$qry = "select a.unit_plan_id,a.clg_id,a.dept_id,a.unit_id,a.user_id , b.session, d.semester , e.section_name , f.subject_name , g.unit_name , h.user_name ,k.dept_name,l.colg_name,i.plan,i.date,i.plan_id,c.short_name ";
$qry .="from  db_unit_plan a ";
$qry .="left join db_session b on a.session_id= b.session_id ";
$qry .="left join db_semester d on  a.semester_id= d.semester_id ";
$qry .="left join db_section e on  a.section_id= e.section_id ";
$qry .="left join db_subject f on  a.subject_id= f.subject_id ";
$qry .="left join db_unit g on  a.unit_id= g.unit_id ";
$qry .="left join db_user h on  a.user_id= h.user_id ";
$qry .="left join db_department k on  a.dept_id= k.dept_id ";
$qry .="left join db_profile l on  a.clg_id= l.profile_id ";
$qry .="left join db_plan i on  a.unit_plan_id= i.unit_plan_id ";
$qry .="left join db_course c on  a.course_id= c.course_id ";
$qry .=" where a.clg_id='".$colg_id."' and a.dept_id='".$dept_id."' and a.user_id='".$user_id."' and a.status='0' ORDER BY a.unit_plan_id desc ";
$result = mysqli_query($connect, $qry);
$data = array();
while ($row =mysqli_fetch_assoc($result)) {
	$data[] = $row;
}
print json_encode($data);
?>
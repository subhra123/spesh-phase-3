<?php
    if(isset($_FILES['file'])){    
    $errors= array();        
    $file_name = $_FILES['file']['name'];
    $file_size =$_FILES['file']['size'];
    $file_tmp =$_FILES['file']['tmp_name'];
    $file_type=$_FILES['file']['type'];   
    $file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
    $extensions = array("jpeg","jpg","png");        
    if(in_array($file_ext,$extensions )=== false){
		 header("HTTP/1.0 401 Unauthorized");
         $errors[]="image extension not allowed, please choose a JPEG or PNG file.";
    }
    if($file_size > 2097152){
		header("HTTP/1.0 401 Unauthorized");
        $errors[]='File size cannot exceed 2 MB';
    }               
    if(empty($errors)==true){
		//$today=('date')(new Date(),'yyyy-MM-dd HH:mm:ss');
        move_uploaded_file($file_tmp,"../upload/".$file_name);
        echo " uploaded file: " . "upload/" . $file_name;
    }else{
        print_r($errors);
    }
}
else{
    $errors= array();
	header("HTTP/1.0 401 Unauthorized");
    $errors[]="No image found";
    print_r($errors);
}
?>
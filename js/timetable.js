function adminCheckSubjectRecord(day,time,tableid){
	var period = 0;
	var subject_name = "";
	var subject_id = document.getElementById("subject_"+day+time).value;
	var max_period = document.getElementById("max_period").value;
	if(subject_id == '')  subject_id = 0 ;
	
	period = getSubjectPeriod(day,time);
	
	var total_val = parseInt(time,10) + parseInt(period,10) ;
	max_period = parseInt(max_period,10);
	//alert(""+max_period+"::"+total_val);
	
	if(total_val > max_period+1 )
	{
		subject_name = getSubjectname(day,time);
		resetTimeFiled(day,time);
		//resetMultiPeriod(tableid,period,day,time);
		alert("Sorry, you can't configure ["+subject_name+"] of total class "+period+" in class no "+time+".");
		return;
	}
	//if(subject_id > 0){
		if(period > 1){ setMultiPeriod(tableid,period,day,time); }
		else { resetMultiPeriod(tableid,period,day,time); };
	//}
	
	if(subject_id == -1 ) // NO_CLASS
	{
		resetMultiPeriod(tableid,period,day,time);
		setNoClassField(tableid,day,time);
		return;
	}
	else
	{
		//resetMultiPeriod(tableid,period,day,time);
		resetNoClassField(day,time);
	}
	
	var dataString = "day_id="+day+"&time_id="+time+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+period;
	//alert(":::"+dataString);
	$.ajax({ 
	type: "POST",url: "php/timetable/addSubjectRecord.php" ,data: dataString,cache: false,
	success: function(html)
		{ 
			//alert("subject_id::"+subject_id+"::"+day+"::"+time);
			if(subject_id != 0)
					document.getElementById("subject_"+day+time).style.backgroundColor="#CCF2CC";
				else
					document.getElementById("subject_"+day+time).style.backgroundColor="#FFE0D9";
		}	 
	});
}
	



function adminCheckFacultyRecord(session_id,day,time,tableid){
	
	var user_id = document.getElementById("faculty_"+day+time).value;
	if(user_id == '') user_id = 0 ;
	var faculty_name="";
	var period = getSubjectPeriod(day,time);
	if(user_id > 0) faculty_name = getFacultyName(day,time);	
	
	var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+time+"&table_id="+tableid+"&faculty_id="+user_id;
	//alert(""+dataString);
	$.ajax({ 
	type: "POST",url: "php/timetable/addFacultyRecord.php" ,data: dataString,cache: false,
	success: function(html)
			{ 
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.results)
				if(dobj.results == 1)
				{
					if(user_id > 0)
						document.getElementById("faculty_"+day+time).style.backgroundColor="#CCF2CC";
					else
					{
						faculty_name = "";
						document.getElementById("faculty_"+day+time).style.backgroundColor="#FFE0D9";
					}
					setMultiFaculty(period,tableid,day,time,user_id,faculty_name,session_id);
				}
				else
				{
					alert(dobj.error);
					document.getElementById("faculty_"+day+time).value = 0;
					document.getElementById("faculty_"+day+time).style.backgroundColor="#FFE0D9";
					setMultiFaculty(period,tableid,day,time,0,"",session_id);
				}
			} 
			});
}
	
	
	
	
function adminCheckVenueRecord(session_id,day,time,tableid){
	
	var venue_id = document.getElementById("venue_"+day+time).value;
	if(venue_id == '') 	venue_id = 0 ;
	var venue_name="";
	var period = getSubjectPeriod(day,time);
	if(venue_id > 0) venue_name = getVenueName(day,time);	
	/*
	var i=1;
	for(i ; i < period ;i++){
		var value = time+i;
		document.getElementById("lab_venue_"+day+value).innerHTML  = venue_name;
		var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+value+"&table_id="+tableid+"&venue_id="+venue_id;
		$.ajax({ 
		type: "POST",url: "php/timetable/addVenueRecord.php" ,data: dataString,cache: false,
			success: function(html)
			{	 
				
			}	 
		});
	}*/
	
	var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+time+"&table_id="+tableid+"&venue_id="+venue_id;
	$.ajax({ 
	type: "POST",url: "php/timetable/addVenueRecord.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html);
				//alert(dobj.results)
			if(dobj.results == 1)
			{
				if(venue_id > 0)
					document.getElementById("venue_"+day+time).style.backgroundColor="#CCF2CC";
				else
				{
					venue_name = "";
					document.getElementById("venue_"+day+time).style.backgroundColor="#FFE0D9";
				}
				setMultiVenue(period,tableid,day,time,venue_id,venue_name,session_id);
			}
			else
			{
				alert(dobj.error);
				document.getElementById("venue_"+day+time).value = 0;
				document.getElementById("venue_"+day+time).style.backgroundColor="#FFE0D9";
				setMultiVenue(period,tableid,day,time,0,"",session_id);
			}		
		} 
	});
}


function updatePublish(){
		var session_id = document.getElementById("session_name").value;
		var section_id = document.getElementById("section_name").value;
		var course_id = document.getElementById("course_name").value;
		var semester_id = document.getElementById("semester").value;
		if(session_id > 0 && section_id > 0 && course_id > 0 && semester_id > 0){
			if(confirm("Would you like to publish Time Table ?")){
				var value = document.getElementById("publish_name").value;
				var dataString = "session_id="+session_id+"&section_id="+section_id+"&course_id="+course_id+"&semester_id="+semester_id+"&value="+value;
				//alert("Data:"+dataString);
				$.ajax({ 
				type: "POST",url: "php/timetable/updatePublish.php" ,data: dataString,cache: false,
					success: function(html)
					{ 
						
					} 
					});
			}
		}
		
}
 
 
function setMultiPeriod(tableid,period,day,time){
	var dataString = "day_id="+day+"&time_id="+time+"&table_id="+tableid;
	$.ajax({ 
	type: "POST",url: "php/timetable/getFieldRecord.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html);
			if(dobj.results == 1)
			{
				var s_type = dobj.subject_type;
				resetToNoField(tableid,s_type,day,time);
			}
			setMultiField(tableid,period,day,time);
		} 
	});
}


function resetMultiPeriod(tableid,period,day,time){
	var dataString = "day_id="+day+"&time_id="+time+"&table_id="+tableid;
	//alert("Data:"+dataString);
	$.ajax({ 
	type: "POST",url: "php/timetable/getFieldRecord.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html);
			if(dobj.results == 1){
				var s_type = dobj.subject_type;
				//alert("Current::"+s_type);	
				resetToNoField(tableid,s_type,day,time);
			}
		} 
	});
}


function resetToNoField(tableid,period,day,time){
	var i=1;
	document.getElementById("faculty_"+day+time).value = 0;
	document.getElementById("venue_"+day+time).value = 0;
	for(i ; i < period ;i++){
		var value = time+i;
		document.getElementById("lab_subject_"+day+value).style.display = "none";
		document.getElementById("lab_faculty_"+day+value).style.display = "none";
		document.getElementById("lab_venue_"+day+value).style.display = "none";
			
		document.getElementById("lab_subject_"+day+value).innerHTML = "";
		document.getElementById("lab_faculty_"+day+value).innerHTML = "";
		document.getElementById("lab_venue_"+day+value).innerHTML = "";
			
		document.getElementById("subject_"+day+value).style.display = "block";
		document.getElementById("faculty_"+day+value).style.display = "block";
		document.getElementById("venue_"+day+value).style.display = "block";
			
		document.getElementById("subject_"+day+value).value = 0;
		document.getElementById("faculty_"+day+value).value = 0;
		document.getElementById("venue_"+day+value).value = 0;
			
		document.getElementById("subject_"+day+value).style.backgroundColor="#FFE0D9";
		document.getElementById("faculty_"+day+value).style.backgroundColor="#FFE0D9";
		document.getElementById("venue_"+day+value).style.backgroundColor="#FFE0D9";
	}
	
	if(period > 1){
		var subject_id = 0;
		var faculty_id = 0;
		var venue_id = 0;
		var subject_type = 0;
		var value = time+1;
		var dataString = "day_id="+day+"&time_id="+value+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+subject_type+"&faculty_id="+faculty_id+"&venue_id="+venue_id;
		//alert("Data:"+dataString);
		$.ajax({ 
		type: "POST",url: "php/timetable/updateAllRecord.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
				if(period > 2)
				{
					var value = time+2;
					var dataString = "day_id="+day+"&time_id="+value+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+subject_type+"&faculty_id="+faculty_id+"&venue_id="+venue_id;
					//alert("Data:"+dataString);
					$.ajax({ 
					type: "POST",url: "php/timetable/updateAllRecord.php" ,data: dataString,cache: false,
						success: function(html)
						{}});
				}
			} 
		});
	}
}


function setMultiField(tableid,period,day,time){
	var subject_name="";
	var faculty_name="";
	var venue_name="";
	var subject_id = document.getElementById("subject_"+day+time).value;
	var faculty_id = document.getElementById("faculty_"+day+time).value;
	var venue_id = document.getElementById("venue_"+day+time).value;
	
	if( subject_id > 0)
		subject_name = getSubjectname(day,time);
	if(faculty_id > 0)
		faculty_name = getFacultyName(day,time);
	if(venue_id > 0)
		venue_name = getVenueName(day,time);	
	
	var i=1;
	for(i ; i < period ;i++){
		var value = time+i;
		document.getElementById("subject_"+day+value).style.display = "none";
		document.getElementById("lab_subject_"+day+value).style.display = "block";
		document.getElementById("lab_subject_"+day+value).innerHTML  = subject_name;
			
		document.getElementById("faculty_"+day+value).style.display = "none";
		document.getElementById("lab_faculty_"+day+value).style.display = "block";
		document.getElementById("lab_faculty_"+day+value).innerHTML  = faculty_name;
			
		document.getElementById("venue_"+day+value).style.display = "none";
		document.getElementById("lab_venue_"+day+value).style.display = "block";
		document.getElementById("lab_venue_"+day+value).innerHTML  = venue_name;
	}
	if(period > 1){
		var subject_type = 0;
		var value = time+1;
		var dataString = "day_id="+day+"&time_id="+value+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+subject_type+"&faculty_id="+faculty_id+"&venue_id="+venue_id;
		//alert("Data:"+dataString);
		$.ajax({ 
		type: "POST",url: "php/timetable/updateAllRecord.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
				if(period > 2){
					var value = time+2;
					var dataString = "day_id="+day+"&time_id="+value+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+subject_type+"&faculty_id="+faculty_id+"&venue_id="+venue_id;
					//alert("Data:"+dataString);
					$.ajax({ 
					type: "POST",url: "php/timetable/updateAllRecord.php" ,data: dataString,cache: false,
						success: function(html)
						{}});
				}
			} 
		});
	}
}

function setMultiFaculty(period,tableid,day,time,user_id,faculty_name,session_id){
	//alert("INSIDE setMultiFaculty");
	if(faculty_name.length == 0){
		resetFaculty(tableid,day,time);	
	}
	//alert(":::aaaa");
	if(period > 1)
	{
		var value = time+1;
		document.getElementById("lab_faculty_"+day+value).innerHTML  = faculty_name;
		var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+value+"&table_id="+tableid+"&faculty_id="+user_id;
		$.ajax({ 
		type: "POST",url: "php/timetable/addFacultyRecord.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				if(dobj.results == 1){
					if(period > 2){
						var value = time+2;
						document.getElementById("lab_faculty_"+day+value).innerHTML  = faculty_name;
						var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+value+"&table_id="+tableid+"&faculty_id="+user_id;
						$.ajax({ 
						type: "POST",url: "php/timetable/addFacultyRecord.php" ,data: dataString,cache: false,
							success: function(html)
							{
								var dobj=jQuery.parseJSON(html);
								if(dobj.results == 1){
									
								}
								else{
									alert(dobj.error);
									document.getElementById("faculty_"+day+time).value = 0;
									document.getElementById("faculty_"+day+time).style.backgroundColor="#FFE0D9";
									setMultiFaculty(period,tableid,day,time,0,"",session_id);
								}
								
							} 
						});
					}
				}
				else{
					alert(dobj.error);
					document.getElementById("faculty_"+day+time).value = 0;
					document.getElementById("faculty_"+day+time).style.backgroundColor="#FFE0D9";
					setMultiFaculty(period,tableid,day,time,0,"",session_id);
				}
				
			}
		});
	}
}


function resetFaculty(tableid,day,time){
	var faculty_id = 0;
	var dataString = "day_id="+day+"&time_id="+time+"&table_id="+tableid+"&faculty_id="+faculty_id;
	//alert("::"+dataString);
		$.ajax({ 
		type: "POST",url: "php/timetable/addFacultyRecord.php" ,data: dataString,cache: false,
			success: function(html){}
		});
}


function setMultiVenue(period,tableid,day,time,venue_id,venue_name,session_id){
	if(venue_name.length == 0){
		resetVenue(tableid,day,time);	
	}
	
	if(period > 1)
	{
		var value = time+1;
		document.getElementById("lab_venue_"+day+value).innerHTML  = venue_name;
		var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+value+"&table_id="+tableid+"&venue_id="+venue_id;
		$.ajax({ 
		type: "POST",url: "php/timetable/addVenueRecord.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				if(dobj.results == 1){
					if(period > 2){
						var value = time+2;
						document.getElementById("lab_venue_"+day+value).innerHTML  = venue_name;
						var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+value+"&table_id="+tableid+"&venue_id="+venue_id;
						$.ajax({ 
						type: "POST",url: "php/timetable/addVenueRecord.php" ,data: dataString,cache: false,
							success: function(html)
							{
								var dobj=jQuery.parseJSON(html);
								if(dobj.results == 1){
									
								}
								else{
									alert(dobj.error);
									document.getElementById("venue_"+day+time).value = 0;
									document.getElementById("venue_"+day+time).style.backgroundColor="#FFE0D9";
									setMultiVenue(period,tableid,day,time,0,"",session_id);
								}
								
							} 
						});
					}
				}
				else{
					alert(dobj.error);
					document.getElementById("venue_"+day+time).value = 0;
					document.getElementById("venue_"+day+time).style.backgroundColor="#FFE0D9";
					setMultiVenue(period,tableid,day,time,0,"",session_id);
				}
				
			}
		});
	}
}

function resetVenue(tableid,day,time){
	var venue_id = 0;
	var dataString = "day_id="+day+"&time_id="+time+"&table_id="+tableid+"&venue_id="+venue_id;
		$.ajax({ 
		type: "POST",url: "php/timetable/addVenueRecord.php" ,data: dataString,cache: false,
			success: function(html){}
		});
}




function setNoClassField(tableid,day,time){
	document.getElementById("faculty_"+day+time).style.display = "none";
	document.getElementById("venue_"+day+time).style.display = "none";
	document.getElementById("lab_faculty_"+day+time).style.display = "block";
	document.getElementById("lab_venue_"+day+time).style.display = "block";
	
	document.getElementById("lab_faculty_"+day+time).innerHTML = "";
	document.getElementById("lab_venue_"+day+time).innerHTML = "";
	
	var subject_type = 0;
	var subject_id = -1;
	var faculty_id = 0;
	var venue_id = 0;
	var dataString = "day_id="+day+"&time_id="+time+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+subject_type+"&faculty_id="+faculty_id+"&venue_id="+venue_id;
		$.ajax({ 
		type: "POST",url: "php/timetable/updateAllRecord.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
				document.getElementById("subject_"+day+time).style.backgroundColor="#CCF2CC";
			} 
		});

}

function resetNoClassField(day,time){
	document.getElementById("faculty_"+day+time).style.display = "block";
	document.getElementById("venue_"+day+time).style.display = "block";
	document.getElementById("faculty_"+day+time).value = 0;
	document.getElementById("venue_"+day+time).value = 0;
	document.getElementById("faculty_"+day+time).style.backgroundColor="#FFE0D9";
	document.getElementById("venue_"+day+time).style.backgroundColor="#FFE0D9";
	document.getElementById("lab_faculty_"+day+time).style.display = "none";
	document.getElementById("lab_venue_"+day+time).style.display = "none";
}

function resetTimeFiled(day,time){
	
	document.getElementById("faculty_"+day+time).style.display = "block";
	document.getElementById("venue_"+day+time).style.display = "block";
	document.getElementById("subject_"+day+time).value = 0;
	document.getElementById("faculty_"+day+time).value = 0;
	document.getElementById("venue_"+day+time).value = 0;
	document.getElementById("subject_"+day+time).style.backgroundColor="#FFE0D9";
	document.getElementById("faculty_"+day+time).style.backgroundColor="#FFE0D9";
	document.getElementById("venue_"+day+time).style.backgroundColor="#FFE0D9";
	document.getElementById("lab_faculty_"+day+time).style.display = "none";
	document.getElementById("lab_venue_"+day+time).style.display = "none";
}
	
function getSubjectname(day,time){
		var subject_name = document.getElementById("subject_"+day+time).selectedOptions[0].text;
		return subject_name;
}
	
function getFacultyName(day,time){
		var name = document.getElementById("faculty_"+day+time).selectedOptions[0].text;
		if(name.length > 20)
			name = name.substring(0, 20);
		return name;
}
	
function getVenueName(day,time){
		var name = document.getElementById("venue_"+day+time).selectedOptions[0].text;
		return name;
}	

function getSubjectPeriod(day,time){
	var period=0;
	var subject_id = document.getElementById("subject_"+day+time).value;
	if(subject_id > 0)
		period = document.getElementById("subjecttype_"+subject_id).value;
	
	return period;
}


	
	
function savePrincipalData1(){
	//alert("aaa");
	var table_id = document.getElementById("tableid").value ;
	if(table_id == ""){
		alert("Select Semester...");
		return;
	}
	var status = document.getElementById("principal_approve").value ;
	var remark = document.getElementById("principal_remarks").value ;
	var dataString = "table_id="+table_id+"&status="+status+"&remark="+remark;
	//alert("Data:"+dataString);
	$.ajax({ 
	type: "POST",url: "php/princpaltime/updatePrincpalData.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html);
			if(dobj.results == 1){
				alert("Succesfully Saved...");		
			}
		} 
	});
}

	
	
		
var Admin=angular.module('Spesh',['ui.router', '720kb.datepicker','ngMessages','ngCapsLock','ui.bootstrap','ngFileUpload','angularUtils.directives.dirPagination','angularjs-dropdown-multiselect']);
Admin.run(function($rootScope, $state) {
      $rootScope.$state = $state;
    });
Admin.config(function($stateProvider, $urlRouterProvider,$locationProvider) {
	$urlRouterProvider.otherwise('/');
	$stateProvider
	 .state('/', { /*....This state defines All type of user login...*/
            url: '/',
            templateUrl: 'dashboardview/login.html',
			controller: 'loginController'
        })
		.state('forgetpass', { /*....This state defines All type of user login...*/
            url: '/forgetpass',
            templateUrl: 'dashboardview/forgetpass.html',
			controller: 'forgetpassController'
        })
		.state('resetPass', { /*....This state defines All type of user login...*/
            url: '/resetPass?m_i',
            templateUrl: 'dashboardview/resetPass.html',
			controller: 'resetPassController'
        })
		/*.........START OF ROOT ADMIN STATES.........*/
		.state('dashboard', { /* This state defines The root admin login page. */
        url: '/dashboard',
		templateUrl: 'dashboardview/dashboard.html',
		controller: 'adminController'
        })
		
		
		.state('dashboard.category', { /* This state defines the department management page */
        url: '/category',
        templateUrl: 'dashboardview/category.html',
        controller: 'adminCategoryController'
    	})
		
		.state('dashboard.category.cat', { /* This state defines the department management page */
        url: '/manage_category',
        templateUrl: 'dashboardview/manage_category.html',
        controller: 'adminCatCategoryController'
    	})
		
		.state('dashboard.category.subcat', { /* This state defines the department management page */
        url: '/manage_subcategory',
        templateUrl: 'dashboardview/manage_subcategory.html',
        controller: 'adminSubcatCategoryController'
    	})
		.state('dashboard.category.city', { /* This state defines the department management page */
        url: '/city',
        templateUrl: 'dashboardview/city.html',
        controller: 'adminCityController'
    	})
		.state('dashboard.category.special', { /* This state defines the department management page */
        url: '/special',
        templateUrl: 'dashboardview/special.html',
        controller: 'adminSpecialController'
    	})
        .state('dashboard.gallery', {
        url: '/gallery',
        templateUrl: 'dashboardview/gallery.html',
        controller: 'adminGalleryController'
    	})
         .state('dashboard.gallery.upload', { 
        url: '/upload',
        templateUrl: 'dashboardview/upload.html',
        controller: 'adminGalleryUploadController'
    	})
		.state('dashboard.customer', { 
        url: '/customer',
        templateUrl: 'dashboardview/customer.html',
        controller: 'adminCustomerController'
    	})
		.state('dashboard.customer.new', { 
        url: '/new',
        templateUrl: 'dashboardview/customerNew.html',
        controller: 'adminCustomerNewController'
    	})
        .state('map', { 
        url: '/map?latitude&longitude&title',
        templateUrl: 'dashboardview/map.html',
        controller: 'adminCustomerMapController'
    	})
		.state('dashboard.customer.view', { 
        url: '/view',
        templateUrl: 'dashboardview/customerView.html',
        controller: 'adminCustomerViewController'
    	})
		.state('dashboard.customer.image', { 
        url: '/image',
        templateUrl: 'dashboardview/customerImage.html',
        controller: 'adminCustomerImageController'
    	})
		.state('dashboard.customer.time', { 
        url: '/time',
        templateUrl: 'dashboardview/customerTime.html',
        controller: 'adminCustomerTimeController'
    	})
        .state('dashboard.customer.addimage', { 
        url: '/addimage',
        templateUrl: 'dashboardview/customerAddImage.html',
        controller: 'adminCustomerAddImageController'
    	})
        .state('dashboard.customer.addbusiness', { 
        url: '/addbusiness',
        templateUrl: 'dashboardview/customerAddBusiness.html',
        controller: 'adminCustomerAddBusinessController'
        })
        .state('dashboard.customer.addlink', { 
        url: '/addlink',
        templateUrl: 'dashboardview/addlink.html',
        controller: 'adminCustomerAddlinkController'
        })
		.state('dashboard.online', { /* This state defines the department management page */
        url: '/view_online',
        templateUrl: 'dashboardview/online.html',
        controller: 'adminOnlineController'
    	})
		.state('business', { /* This state defines the department management page */
        url: '/business',
        templateUrl: 'businessview/business.html',
        controller: 'businessController'
    	})
		.state('business.customer', { 
        url: '/customer',
        templateUrl: 'businessview/customer.html',
        controller: 'businessCustomerController'
    	})
		.state('business.customer.view', { 
        url: '/view',
        templateUrl: 'businessview/customerView.html',
        controller: 'businessCustomerViewController'
    	})
		.state('business.customer.image', { 
        url: '/image',
        templateUrl: 'businessview/customerImage.html',
        controller: 'businessCustomerImageController'
    	})
       .state('business.customer.time', { 
        url: '/time',
        templateUrl: 'businessview/customerTime.html',
        controller: 'businessCustomerTimeController'
    	})
		.state('dashboard.analytics', { 
        url: '/analytics',
        templateUrl: 'dashboardview/analytics.html',
        controller: 'adminAnalyticsController'
    	})
		.state('dashboard.analytics.summary', { 
        url: '/summary',
        templateUrl: 'dashboardview/summary.html',
        controller: 'adminSummaryController'
    	})
		.state('dashboard.analytics.details', { 
        url: '/details',
        templateUrl: 'dashboardview/details.html',
        controller: 'adminDetailController'
    	})
		.state('business.analytics', { 
        url: '/analytics',
        templateUrl: 'businessview/analytics.html',
        controller: 'businessAnalyticsController'
    	})
		.state('business.analytics.summary', { 
        url: '/summary',
        templateUrl: 'businessview/summary.html',
        controller: 'businessSummaryController'
    	})
		.state('business.analytics.details', { 
        url: '/details',
        templateUrl: 'businessview/details.html',
        controller: 'businessDetailController'
    	})
		.state('dashboard.analytics.subcategorysummary', { 
        url: '/subcategorysummary',
        templateUrl: 'dashboardview/subcategorysummary.html',
        controller: 'subcategorySummaryController'
    	})
		.state('dashboard.analytics.subcategorydetails', { 
        url: '/subcategorydetails',
        templateUrl: 'dashboardview/subcategorydetails.html',
        controller: 'subcategoryDetailsController'
    	})
        .state('dashboard.manage', { 
        url: '/manage',
        templateUrl: 'dashboardview/manage.html',
        controller: 'adminManageController'
        })
        .state('dashboard.manage.app', { 
        url: '/app',
        templateUrl: 'dashboardview/manageapp.html',
        controller: 'adminManageAppController'
        })
	/*$locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    });	*/
})

var btime=angular.module('Spesh');
btime.controller('businessCustomerTimeController',function($scope,$state,$http,$window,$timeout,focusTimeInputField){
    $scope.buttonName='Add';
	var memb_id='';
	var da_id='';
	var time_id='';
    var prevDays='';
    var prevTimeIds=[];
    $http({
		method:'GET',
		url:"php/customerInfo.php?action=businessTimedisp",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('res',response.data);
		if(response.data !='null'){
			$scope.listOfBusiTimeData=response.data;
		}
        if(response.data =='null'){
			$scope.listOfBusiTimeData=[];
		}
	},function errorCallback(response) {
	})
	$scope.listOfDays=[{
		name:'Select a day',
		value:''
    }]
    $scope.example14model = [];
    $scope.example14settings = {
        scrollableHeight: '200px',
        scrollable: true,
        enableSearch: true
    };
    $http({
           method:'POST',
           url:'php/customerInfo.php?action=bussichildrest',
           headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
       }).then(function successCallback(response){
           //console.log('child',response.data.result);
           if(response.data.isExist==1){
               $scope.example14data = [];
               angular.forEach(response.data.result,function(obj){
                   var data={'label':obj.child_rest_name,'id':obj.child_member_id};
                   $scope.example14data.push(data);
               })
                $scope.showchild=true;
              // console.log('examl',$scope.example14model);
           }else{
               $scope.showchild=false;
           }
       },function errorCallback(response) {
       })
    $scope.example2settings = {
        displayProp: 'id'
    };
	/*$scope.days=$scope.listOfDays[0];
		$http({
		method:'GET',
		url:"php/customerInfo.php?action=day",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('day res',response.data);
			angular.forEach(response.data,function(obj){
				var data={'name':obj.day_name,'value':obj.day_id};
				$scope.listOfDays.push(data);
			})
		},function errorCallback(response) {
		})*/
    $scope.example15model = [];
    $scope.example15settings = {
      scrollableHeight: '200px',
      scrollable: true,
      enableSearch: true
    };
    $http({
    method:'GET',
    url:"php/customerInfo.php?action=day",
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).then(function successCallback(response){
      console.log('day res',response.data);
      $scope.example15data = [];
      angular.forEach(response.data,function(obj){
        //var data={'name':obj.day_name,'value':obj.day_id};
        //$scope.listOfDays.push(data);
        var data={'label':obj.day_name,'id':obj.day_id};
        $scope.example15data.push(data);
      })
    },function errorCallback(response) {
    })
        
        /*$scope.addTimeDetails=function(billdata){
            //console.log('times',$scope.restaurant);
            if(billdata.$valid){
               if($scope.days.value==null || $scope.days.value==''){
                   alert('Please select day');
                   focusTimeInputField.borderColor('day');
                }else if($scope.time==null || $scope.time==''){
                    alert('Please select the from time');
                    focusTimeInputField.borderColor('time');
                }else if($scope.time1==null || $scope.time1==''){
                    alert('Please select the To time');
                    focusTimeInputField.borderColor('time1');
                }else{
                    var time='';
                    time=$scope.time+'-'+$scope.time1;
                    var sibBusiness=[];
                    if($scope.example14model.length > 0){
                           sibBusiness=$scope.example14model;
                    }else{
                           sibBusiness=[];
                    }
                    if($scope.buttonName=='Add'){
                        var addTimeData=$.param({'action':'busitimeAdd','day_id':$scope.days.value,'time':time,'sibling':sibBusiness});
                        $http({
                            method:'POST',
                            url:'php/customerInfo.php',
                            data:addTimeData,
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        }).then(function successCallback(response){
                           //  console.log('succ',response.data);
                            alert(response.data['msg']);
                            $state.go('business.customer.time',{}, { reload: true });
                        },function errorCallback(response) {
                            //console.log('err',response.data);
                             alert(response.data['msg']);
                        })
                    }
                    if($scope.buttonName=='Update'){
                        if(prevTimeIds.length > 0){
                            prevIds=prevTimeIds
                        }else{
                            prevIds=[];
                        }
                        var updateTimeData=$.param({'action':'busiTimeUpdate','day_id':$scope.days.value,'time':time,'time_id':time_id,'sibling':sibBusiness,'prevDays':prevDays,'prevIds':prevIds});
                        $http({
                            method:'POST',
                            url:'php/customerInfo.php',
                            data:updateTimeData,
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        }).then(function successCallback(response){
                            console.log('succ',response.data);
                            alert(response.data['msg']);
                            $state.go('business.customer.time',{}, { reload: true });
                        },function errorCallback(response) {
                            console.log('err',response.data);
                             alert(response.data['msg']);
                        })
                    }
                }
            }
        }*/
        $scope.addTimeDetails=function(billdata){
            //console.log('times',$scope.restaurant);
            if(billdata.$valid){
               /*if($scope.days.value==null || $scope.days.value==''){
                   alert('Please select day');
                   focusTimeInputField.borderColor('day');
                }*/if($scope.example15model.length == 0){
                   alert('Please select day');
                }else if($scope.time==null || $scope.time==''){
                    alert('Please select the from time');
                    focusTimeInputField.borderColor('time');
                }else if($scope.time1==null || $scope.time1==''){
                    alert('Please select the To time');
                    focusTimeInputField.borderColor('time1');
                }else{
                    var time='';
                    time=$scope.time+'-'+$scope.time1;
                    var sibBusiness=[];
                    var mulDays=[];
                    if($scope.example14model.length > 0){
                           sibBusiness=$scope.example14model;
                    }else{
                           sibBusiness=[];
                    }
                    if($scope.example15model.length > 0){
                           mulDays=$scope.example15model;
                    }else{
                           mulDays=[];
                    }
                    if($scope.buttonName=='Add'){
                        var addTimeData=$.param({'action':'busitimeAdd','muldays':mulDays,'time':time,'sibling':sibBusiness});
                        $http({
                            method:'POST',
                            url:'php/customerInfo.php',
                            data:addTimeData,
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        }).then(function successCallback(response){
                           //  console.log('succ',response.data);
                            alert(response.data['msg']);
                            $state.go('business.customer.time',{}, { reload: true });
                        },function errorCallback(response) {
                            //console.log('err',response.data);
                             alert(response.data['msg']);
                        })
                    }
                    if($scope.buttonName=='Update'){
                        if(prevTimeIds.length > 0){
                            prevIds=prevTimeIds
                        }else{
                            prevIds=[];
                        }
                        var updateTimeData=$.param({'action':'busiTimeUpdate','muldays':mulDays,'time':time,'time_id':time_id,'sibling':sibBusiness,'prevDays':prevDays,'prevIds':prevIds});
                        $http({
                            method:'POST',
                            url:'php/customerInfo.php',
                            data:updateTimeData,
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        }).then(function successCallback(response){
                            console.log('succ',response.data);
                            alert(response.data['msg']);
                            $state.go('business.customer.time',{}, { reload: true });
                        },function errorCallback(response) {
                            console.log('err',response.data);
                             alert(response.data['msg']);
                        })
                    }
                }
            }
        }
        $scope.getPeerBusinessList=function(memid,isPeer){
               var childrest=$.param({'rest_name':memid,'action':'childrest'});
               $http({
                   method:'POST',
                   url:'php/customerInfo.php',
                   data:childrest,
                   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
               }).then(function successCallback(response){
                   //console.log('child',response.data.result);
                   if(response.data.isExist==1){
                       $scope.example14data = [];
                       angular.forEach(response.data.result,function(obj){
                           var data={'label':obj.child_rest_name,'id':obj.child_member_id};
                           $scope.example14data.push(data);
                       })
                        $scope.showchild=true;
                      // console.log('examl',$scope.example14model);
                   }else{
                       $scope.showchild=false;
                   }
               },function errorCallback(response) {
               })
         }
        $scope.editTimeData=function(timeId,dayId){
            da_id=dayId;
            time_id=timeId;
            var editTimeData=$.param({'action':'busiTimeEdit','time_id':time_id,'day_id':da_id});
            $http({
                method:'POST',
                url:'php/customerInfo.php',
                data:editTimeData,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function successCallback(response){
               // $scope.restaurant={};
               // $scope.restaurant.value=response.data[0].member_id;
                //$scope.days.value=response.data[0].day_id;
                var data={'id':response.data[0].day_id};
                $scope.example15model.push(data);
                var timearr='';
                timearr=response.data[0].time.split('-');
                $scope.time=timearr[0];
                $scope.time1=timearr[1];
               // prevDays=$scope.days.value;
                prevDays=response.data[0].day_id;
                prevTimeIds=[];
                $scope.getPeerBusinessList(response.data[0].member_id,response.data[0].peer_business);
                if(response.data[0].peer_business==1){
                   var peerIDS=[];
                   var str_array=response.data[0].peer_business_ids.split(',');
                   angular.forEach(str_array,function(obj){
                      var peerData={'peerids':obj.replace(/^\s*/, "").replace(/\s*$/, "")};
                      peerIDS.push(peerData);
                   })
                   var childData=$.param({'action':'getChildBusiness','peer':peerIDS});
                   $http({
                       method:'POST',
                       url:"php/customerInfo.php",
                       data:childData,
                       headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                   }).then(function successCallback(response){
                       //console.log('peer',response.data);
                       angular.forEach(response.data,function(obj){
                           var data={'id':obj.member_id};
                           $scope.example14model.push(data);
                            prevTimeIds.push(data);
                       })
                      // console.log('peer',$scope.example14data);
                       $scope.showchild=true;
                   },function errorCallback(response) {
                   })
               }
                $scope.buttonName='Update';
                $scope.ClearbuttonName="Cancel";
            $scope.showCancel=true;
            },function errorCallback(response) {
            })
        }
        $scope.clearTimeData=function(){
	           $state.go('business.customer.time',{}, { reload: true });
        }
        $scope.deleteTimeData=function(timeId){
            time_id=timeId;
            var delTimeData=$.param({'action':'timeDel','time_id':time_id});
            if($window.confirm('Are you sure to delete this record ?')){
                $http({
                    method:'POST',
                    url:"php/customerInfo.php",
                    data:delTimeData,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).then(function successCallback(response){
                     alert(response.data['msg']);
			        $state.go('business.customer.time',{}, { reload: true });
                },function errorCallback(response) {
                    alert(response.data['msg']);
			        $state.go('business.customer.time',{}, { reload: true });
                })
            }
        }
})
/*btime.directive('timePicker', [function () {
        return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            model: '=ngModel'
        },
        link: function ($scope, element, attrs, ngModelCtrl) {
            element.timepicker();
            element.on('changeTime', function () {
              $scope.$apply(function () {
                $scope.model = element.val();
              });
            });
   
        }
    }
}]);*/
btime.factory('focusTimeInputField',function($timeout, $window) {
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
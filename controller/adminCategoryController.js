var dept=angular.module('Spesh');
dept.controller('adminCategoryController',function($scope,$http,$state,$window){
	$scope.Depttabs = {
    1: ($state.current.name === 'dashboard.category.cat'),
    2: ($state.current.name === 'dashboard.category.subcat'),
	3: ($state.current.name === 'dashboard.category.city'),
	4: ($state.current.name === 'dashboard.category.special')
    };
});
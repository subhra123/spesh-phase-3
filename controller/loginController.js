var loginAdmin=angular.module('Spesh');
loginAdmin.controller('loginController',function($scope,$http,$location,$window,$state,loginField){
	var ele=$window.document.getElementById('txtname');
	ele.focus();
	ele.style.borderColor="#cccccc";
	$scope.user_name = ''; 
    $scope.user_pass = '';
	$scope.user_type= '';
	$scope.user_login=function(){
	if($scope.user_name==''){
		alert('user name filed should not keep blank');
		loginField.borderColor('txtname');
	}else if($scope.user_pass==''){
		alert('password filed should not keep blank');
		loginField.borderColor('txtpwd');
	}else{
		var userData={'user_name':$scope.user_name,'user_pass':$scope.user_pass};
		$http({
			method: 'POST',
			url: "php/Login/login.php",
			data: userData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('login',response);
			//alert("aa"+response.data['msg']);
			if(response.data['user_type']=='1'){
			$location.path('dashboard');
			}
			if(response.data['user_type']=='4'){
				$location.path('home');
			}
			if(response.data['user_type']=='2'){
				//console.log('log',response.data['user_type']);
				//$location.path('business');
				 $state.go('business.customer.view');
			}
			if(response.data['user_type']=='3' && response.data['role_id'] !='0' && response.data['colg_id'] != '0' ){
				//console.log('log',response.data['user_type']);
				$location.path('hod');
			}
			if(response.data['user_type']=='5'){
				$location.path('user');
			}
			
		},function errorCallback(response) {
			//alert(""+response.data['msg'].length);
			if(response.data['msg'].length > 0)
				alert(response.data['msg']);
			$scope.user_name=null;
			$scope.user_pass=null;
		});
	  
	}
	}
	$scope.clearField=function(id){
		loginField.clearBorderColor(id);
	}
});
loginAdmin.factory('loginField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
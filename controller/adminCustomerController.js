var custom=angular.module('Spesh');
custom.controller('adminCustomerController',function($scope,$http,$state,$window){
	$scope.cattabs = {
    1: ($state.current.name === 'dashboard.customer.new'),
    2: ($state.current.name === 'dashboard.customer.view'),
	3: ($state.current.name === 'dashboard.customer.image'),
	4: ($state.current.name === 'dashboard.customer.time'),
    5: ($state.current.name === 'dashboard.customer.addimage'),
    6: ($state.current.name === 'dashboard.customer.addbusiness'),
    7: ($state.current.name === 'dashboard.customer.addlink'),
    };
});
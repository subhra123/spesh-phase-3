var dashboard=angular.module('Spesh');
dashboard.controller('adminManageAppController',function($scope,$http,$location,$window,$state,Upload,focusInputField){	
     //$scope.profileData=[];
	 
	 $scope.readcolg = "true";
	 $scope.buttonName="Cancel";
	 $scope.cancelButton=false;
	 
	 $scope.submitbuttonName="Add";
	 $scope.submitButton=true;
	 
	 var catid='';							  
	
	
	$http({
		method: 'GET',
		url: "php/manageapp/manageapp.php?action=getVersion",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('res',response.data);
		if (response.data=='null') {
			$scope.objVersionData=[];
		}
		if (response.data.length > 0) {
			$scope.objVersionData=response.data;
		}else{
			$scope.objVersionData=[];
		}
	},function errorCallback(response) {
		
	});
		//////////////////////////////////////////////////////////////////////////////////////////////
	$scope.addCategory=function(billdata){
		if(billdata.$valid){
			if($scope.package==null || $scope.package==''){
				focusInputField.borderColor('package');
				alert('Please Package');
			}else if($scope.theValue==null || $scope.theValue==''){
				alert('Please add version');
			}else if($scope.dcomment==null || $scope.dcomment==''){
				focusInputField.borderColor('dcomment');
				alert('Please add description');
			}else if($scope.ismand=='' && $scope.ismand !=0){
				alert('Please select the Mandotary type');
			}else if($scope.isdevice==''){
				alert('Please select device type');
			}else{
				if ($scope.submitbuttonName=="Add") {
					var vdata=$.param({'action':'addversion','package':$scope.package,'version':$scope.theValue,'comment':$scope.dcomment,'isMandotary':$scope.ismand,'isDevice':$scope.isdevice});
					$http({
						method:'POST',
						url:'php/manageapp/manageapp.php',
						data:vdata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						//console.log('add data',response.data);
						alert(response.data['msg']);
						$state.go('dashboard.manage.app',{},{reload:true});
						
					},function errorCallback(response) {
						alert(response.data['msg']);
					})
				}
				if ($scope.submitbuttonName=="Update") {
					var vdata=$.param({'action':'updateversion','package':$scope.package,'version':$scope.theValue,'comment':$scope.dcomment,'isMandotary':$scope.ismand,'isDevice':$scope.isdevice,'vid':catid});
					$http({
						method:'POST',
						url:'php/manageapp/manageapp.php',
						data:vdata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						//console.log('add data',response.data);
						alert(response.data['msg']);
						$state.go('dashboard.manage.app',{},{reload:true});
						
					},function errorCallback(response) {
						alert(response.data['msg']);
					})
				}
			}
		}	
		else{
			if(billdata.theValue.$invalid){
                alert('This field needs only number(e.g-0,1..9)');
            }	
		}
	
	}
		
	////////////////////////////////////////////////////////////////////////
	$scope.editVersionData =function(cat_id){
		
		catid = cat_id;
		var vdata=$.param({'action':'editversion','vid':catid});
		$http({
		method:'POST',
		url:'php/manageapp/manageapp.php',
		data:vdata,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
		$scope.package=response.data[0].package;
		$scope.theValue=parseFloat(response.data[0].version);
		$scope.dcomment=response.data[0].description;	
		$scope.ismand=response.data[0].mandotary;
		$scope.isdevice=response.data[0].device;		
		$scope.cancelButton=true;
	 	$scope.submitbuttonName="Update";
		//alert(response.data['msg']);
		//$state.go('dashboard.category.cat',{},{reload:true});
							
		},function errorCallback(response) {
		alert(response.data['msg']);
		})
	}
	
	
	$scope.removeCatagoryData =function(cat_id){
		if(confirm("Are you sure want to delete category?")){
			catid = cat_id;
			var supplierData= "cat_id="+cat_id;
			//alert(supplierData);
			$http({
			method:'POST',
			url:'php/category/deleteCategory.php',
			data:supplierData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('dashboard.category.cat',{},{reload:true});
			},function errorCallback(response) {
			alert(response.data['msg']);
			})
		
		}
	}
	
	
	
	$scope.cancelUpdate =function(){
		
		$state.go('dashboard.manage.app',{},{reload:true});
		
	
	}
	
		
	//////////////////////////////////////////////////////////////////////////
	
	
	$scope.clearField=function(id){
		focusInputField.clearBorderColor(id);
	}
	
	$scope.clearProfileData=function(){
		$state.go('dashboard.manage.app',{}, { reload: true });
	}
});

dashboard.factory('focusInputField',function($timeout, $window) {
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
dashboard.directive('customOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.customOnChange);
      element.bind('change', onChangeHandler);
    }
  };
});
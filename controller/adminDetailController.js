var details=angular.module('Spesh');
details.controller('adminDetailController',function($http,$state,$scope,$window,$timeout,$filter,browserDetection){
	$http({
		method:'GET',
		url:"php/analytics.php?action=detail",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('response detail',response.data);
		$scope.clickDetail=response.data;
		//var events=$scope.clickDetail;
	},function errorCallback(response) {
	})
	var date = null
    $scope.getFilteredEvents = function(from, to) {
		//console.log('select date',typeof(from),typeof(to));
		var browser=browserDetection.detectBrowserType();
		if(browser=='Chrome'){
			  if (!(from && to)) {
				return $scope.clickDetail;
			  }
		
			  function getMidnight(date) {
				return (new Date(new Date(date).getFullYear(), new Date(date).getMonth(), new Date(date).getDate()))
			  }
			  from = getMidnight(from);
		
			  to = getMidnight(to).getTime() + (24 * 3600-1) * 1000;
			  console.log('events',from,to);
			  return $scope.clickDetail.filter(function(event) {
				 return getMidnight(new Date(event.date) + 24 * 3600 * 1000).getTime() >= (new Date(from)).getTime() && getMidnight(event.date).getTime() <= (new Date(to)).getTime();
			  })
		}else{
			 if (!(from && to)) {
				 return $scope.clickDetail;
			 }
			 function getMidnight1(date) {
				 return (new Date(new Date(date.replace(/-/g, "/")).getFullYear(), new Date(date.replace(/-/g, "/")).getMonth(), new Date(date.replace(/-/g, "/")).getDate())); 
			  }
			 from = getMidnight1(from);
			 to = getMidnight1(to).getTime() + (24 * 3600-1) * 1000;
			 // console.log('events safari',from,to);
			 return $scope.clickDetail.filter(function(event) {
				// console.log('filter',event);
			     return new Date(event.date.replace(/-/g, "/")).getTime() >= (new Date(from)).getTime() && getMidnight1(event.date).getTime() <= (new Date(to)).getTime();
		    })
		}
    }
	$scope.generateExcelSheet=function(){
	   //console.log('data',$scope.listOfReportData);
	   //console.log('export',document.getElementById('exportable').innerHTML);
	  /* var blob = new Blob([document.getElementById('exportable').innerHTML], {
			            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
		            });
	 saveAs(blob, "Report.xls");*/
	//alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) FROM ?',[$scope.listOfReportData]);
			var uri = 'data:application/vnd.ms-excel;base64,'
			, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
			, base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
			, format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
			  var table='exportable';
			  var name='Report';
			 // console.log('table',table,name);
			if (!table.nodeType) table = document.getElementById(table)
			var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
			window.location.href = uri + base64(format(template, ctx));
		  
   }
	$scope.exportData=function(){
		$scope.expData=[];
       // console.log('data',$scope.labelResults)
		angular.forEach($scope.labelResults,function(obj){
			var data={'summary_id':obj.member_id};
			$scope.expData.push(data);
		});
		//console.log('exp',$scope.expData);
		var searchdata=$.param({'action':'search','summary':$scope.expData});
		$http({
			method:'POST',
			url:"php/analytics.php",
			data:searchdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			 // console.log('res',response.data);
            if(response.data !='null'){
                $scope.listOfReportData=response.data;
            }
            if(response.data =='null'){
                $scope.listOfReportData=[];
            }
			   if($scope.listOfReportData){
					$timeout(function(){
						$scope.generateExcelSheet();
					},3000);
				}
		},function errorCallback(response) {
		})
	}
})
details.factory('browserDetection',function($window,$timeout){
	return{
		detectBrowserType:function(){
			var objappVersion = navigator.appVersion; 
            var objAgent = navigator.userAgent; 
            var objbrowserName = navigator.appName; 
            var objfullVersion = ''+parseFloat(navigator.appVersion); 
            var objBrMajorVersion = parseInt(navigator.appVersion,10); 
            var objOffsetName,objOffsetVersion,ix;
			if ((objOffsetVersion=objAgent.indexOf("Chrome"))!=-1){
				objbrowserName = "Chrome";
			}else if((objOffsetVersion=objAgent.indexOf("MSIE"))!=-1){
				objbrowserName = "Microsoft Internet Explorer";
			}else if((objOffsetVersion=objAgent.indexOf("Firefox"))!=-1){
				objbrowserName = "Firefox";
			}else if((objOffsetVersion=objAgent.indexOf("Safari"))!=-1){
				objbrowserName = "Safari";
			}else if((objOffsetName=objAgent.lastIndexOf(' ')+1) < (objOffsetVersion=objAgent.lastIndexOf('/'))){
				objbrowserName = objAgent.substring(objOffsetName,objOffsetVersion);
		        objbrowserName = objAgent.substring(objOffsetName,objOffsetVersion);
			}else{
			}
			return objbrowserName;
		}
	}
})

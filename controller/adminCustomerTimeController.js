var time=angular.module('Spesh');
time.controller('adminCustomerTimeController',function($scope,$state,$http,$window,$timeout,focusTimeInputField){
	$scope.buttonName='Add';
	var memb_id='';
	var da_id='';
	var time_id='';
    var prevSubCatId='';
    var prevDays='';
    var prevTimeIds=[];
    $http({
		method:'GET',
		url:"php/customerInfo.php?action=timedisp",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('res',response.data);
		if(response.data !='null'){
			$scope.listOfTimeData=response.data;
		}
        if(response.data =='null'){
			$scope.listOfTimeData=[];
		}
	},function errorCallback(response) {
	})
	/*$scope.listOfRestaurant=[{
		name:'Select Business Name',
		value:''
	}]
	$scope.restaurant=$scope.listOfRestaurant[0];*/
    $scope.listOfRestaurant=[];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=restaurant",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.rest_name,'value':obj.member_id};
			$scope.listOfRestaurant.push(data);
		})
	},function errorCallback(response) {
	})
	/*$scope.listOfDays=[{
		name:'Select a day',
		value:''
    }]
	$scope.days=$scope.listOfDays[0];
		$http({
		method:'GET',
		url:"php/customerInfo.php?action=day",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('day res',response.data);
			angular.forEach(response.data,function(obj){
				var data={'name':obj.day_name,'value':obj.day_id};
				$scope.listOfDays.push(data);
			})
		},function errorCallback(response) {
		})*/
    $scope.example15model = [];
        $scope.example15settings = {
            scrollableHeight: '200px',
            scrollable: true,
            enableSearch: true
        };
    $http({
    method:'GET',
    url:"php/customerInfo.php?action=day",
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).then(function successCallback(response){
      console.log('day res',response.data);
      $scope.example15data = [];
      angular.forEach(response.data,function(obj){
        //var data={'name':obj.day_name,'value':obj.day_id};
        var data={'label':obj.day_name,'id':obj.day_id};
        //$scope.listOfDays.push(data);
        $scope.example15data.push(data);
      })
    },function errorCallback(response) {
    })
        $scope.example14model = [];
        $scope.example14settings = {
            scrollableHeight: '200px',
            scrollable: true,
            enableSearch: true
        };
        $scope.getPeerBusiness=function(){
            var childrest=$.param({'rest_name':$scope.restaurant.value,'action':'childrest'});
               $http({
                   method:'POST',
                   url:'php/customerInfo.php',
                   data:childrest,
                   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
               }).then(function successCallback(response){
                   //console.log('child',response.data.result);
                   if(response.data.isExist==1){
                       $scope.example14data = [];
                       angular.forEach(response.data.result,function(obj){
                           var data={'label':obj.child_rest_name,'id':obj.child_member_id};
                           $scope.example14data.push(data);
                       })
                        $scope.showchild=true;
                      // console.log('examl',$scope.example14model);
                   }else{
                       $scope.showchild=false;
                   }
               },function errorCallback(response) {
               })
        }
        $scope.example2settings = {
            displayProp: 'id'
        };
        /*$scope.addTimeDetails=function(billdata){
            //console.log('times',$scope.restaurant);
            if(billdata.$valid){
                if($scope.restaurant==undefined){
                   alert('Please select Business Name');
                   focusTimeInputField.borderColor('restau');
                }else if($scope.days.value==null || $scope.days.value==''){
                   alert('Please select day');
                   focusTimeInputField.borderColor('day');
                }else if($scope.time==null || $scope.time==''){
                    alert('Please select the from time');
                    focusTimeInputField.borderColor('time');
                }else if($scope.time1==null || $scope.time1==''){
                    alert('Please select the To time');
                    focusTimeInputField.borderColor('time1');
                }else{
                    var time='';
                    time=$scope.time+'-'+$scope.time1;
                    var sibBusiness=[];
                    if($scope.example14model.length > 0){
                           sibBusiness=$scope.example14model;
                    }else{
                           sibBusiness=[];
                    }
                    if($scope.buttonName=='Add'){
                        var addTimeData=$.param({'action':'timeAdd','member_id':$scope.restaurant.value,'day_id':$scope.days.value,'time':time,'sibling':sibBusiness});
                        $http({
                            method:'POST',
                            url:'php/customerInfo.php',
                            data:addTimeData,
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        }).then(function successCallback(response){
                           //  console.log('succ',response.data);
                            alert(response.data['msg']);
                            $state.go('dashboard.customer.time',{}, { reload: true });
                        },function errorCallback(response) {
                            //console.log('err',response.data);
                             alert(response.data['msg']);
                        })
                    }
                    if($scope.buttonName=='Update'){
                        if(prevTimeIds.length > 0){
                            prevIds=prevTimeIds
                        }else{
                            prevIds=[];
                        }
                        var updateTimeData=$.param({'action':'timeUpdate','member_id':$scope.restaurant.value,'day_id':$scope.days.value,'time':time,'time_id':time_id,'sibling':sibBusiness,'prevDays':prevDays,'prevIds':prevIds});
                        $http({
                            method:'POST',
                            url:'php/customerInfo.php',
                            data:updateTimeData,
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        }).then(function successCallback(response){
                            console.log('succ',response.data);
                            alert(response.data['msg']);
                            $state.go('dashboard.customer.time',{}, { reload: true });
                        },function errorCallback(response) {
                            console.log('err',response.data);
                             alert(response.data['msg']);
                        })
                    }
                }
            }
        }*/
        $scope.addTimeDetails=function(billdata){
            //console.log('times',$scope.restaurant);
            if(billdata.$valid){
                if($scope.restaurant==undefined){
                   alert('Please select Business Name');
                   focusTimeInputField.borderColor('restau');
                }/*else if($scope.days.value==null || $scope.days.value==''){
                   alert('Please select day');
                   focusTimeInputField.borderColor('day');
                }*/else if($scope.example15model.length == 0){
                   alert('Please select day');
                }else if($scope.time==null || $scope.time==''){
                    alert('Please select the from time');
                    focusTimeInputField.borderColor('time');
                }else if($scope.time1==null || $scope.time1==''){
                    alert('Please select the To time');
                    focusTimeInputField.borderColor('time1');
                }else{
                    var time='';
                    time=$scope.time+'-'+$scope.time1;
                    var sibBusiness=[];
                    var mulDays=[];
                    if($scope.example14model.length > 0){
                           sibBusiness=$scope.example14model;
                    }else{
                           sibBusiness=[];
                    }
                    if($scope.example15model.length > 0){
                           mulDays=$scope.example15model;
                    }else{
                           mulDays=[];
                    }
                    if($scope.buttonName=='Add'){
                        var addTimeData=$.param({'action':'timeAdd','member_id':$scope.restaurant.value,'muldays':mulDays,'time':time,'sibling':sibBusiness});
                        $http({
                            method:'POST',
                            url:'php/customerInfo.php',
                            data:addTimeData,
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        }).then(function successCallback(response){
                           //  console.log('succ',response.data);
                            alert(response.data['msg']);
                            $state.go('dashboard.customer.time',{}, { reload: true });
                        },function errorCallback(response) {
                            //console.log('err',response.data);
                             alert(response.data['msg']);
                        })
                    }
                    if($scope.buttonName=='Update'){
                        if(prevTimeIds.length > 0){
                            prevIds=prevTimeIds
                        }else{
                            prevIds=[];
                        }
                        var updateTimeData=$.param({'action':'timeUpdate','member_id':$scope.restaurant.value,'muldays':mulDays,'time':time,'time_id':time_id,'sibling':sibBusiness,'prevDays':prevDays,'prevIds':prevIds});
                        $http({
                            method:'POST',
                            url:'php/customerInfo.php',
                            data:updateTimeData,
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        }).then(function successCallback(response){
                            console.log('succ',response.data);
                            alert(response.data['msg']);
                            $state.go('dashboard.customer.time',{}, { reload: true });
                        },function errorCallback(response) {
                            console.log('err',response.data);
                             alert(response.data['msg']);
                        })
                    }
                }
            }
        }
         $scope.getPeerBusinessList=function(memid,isPeer){
               var childrest=$.param({'rest_name':memid,'action':'childrest'});
               $http({
                   method:'POST',
                   url:'php/customerInfo.php',
                   data:childrest,
                   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
               }).then(function successCallback(response){
                   //console.log('child',response.data.result);
                   if(response.data.isExist==1){
                       $scope.example14data = [];
                       angular.forEach(response.data.result,function(obj){
                           var data={'label':obj.child_rest_name,'id':obj.child_member_id};
                           $scope.example14data.push(data);
                       })
                        $scope.showchild=true;
                      // console.log('examl',$scope.example14model);
                   }else{
                       $scope.showchild=false;
                   }
               },function errorCallback(response) {
               })
         }
       $scope.editTimeData=function(timeId,memberId,dayId){
            memb_id=memberId;
            da_id=dayId;
            time_id=timeId;
            var editTimeData=$.param({'action':'timeEdit','time_id':time_id,'member_id':memb_id,'day_id':da_id})
            $http({
                method:'POST',
                url:'php/customerInfo.php',
                data:editTimeData,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function successCallback(response){
                $scope.restaurant={};
                $scope.restaurant.value=response.data[0].member_id;
                var data={'id':response.data[0].day_id};
                 $scope.example15model.push(data);
                //$scope.days.value=response.data[0].day_id;
                var timearr='';
                timearr=response.data[0].time.split('-');
                $scope.time=timearr[0];
                $scope.time1=timearr[1];
                prevDays=response.data[0].day_id;
                prevTimeIds=[];
                $scope.getPeerBusinessList(response.data[0].member_id,response.data[0].peer_business);
                if(response.data[0].peer_business==1){
                   var peerIDS=[];
                   var str_array=response.data[0].peer_business_ids.split(',');
                   angular.forEach(str_array,function(obj){
                      var peerData={'peerids':obj.replace(/^\s*/, "").replace(/\s*$/, "")};
                      peerIDS.push(peerData);
                   })
                   var childData=$.param({'action':'getChildBusiness','peer':peerIDS});
                   $http({
                       method:'POST',
                       url:"php/customerInfo.php",
                       data:childData,
                       headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                   }).then(function successCallback(response){
                       //console.log('peer',response.data);
                       angular.forEach(response.data,function(obj){
                           var data={'id':obj.member_id};
                           $scope.example14model.push(data);
                           prevTimeIds.push(data);
                       })
                      // console.log('peer',$scope.example14data);
                       $scope.showchild=true;
                   },function errorCallback(response) {
                   })
               }
                $scope.buttonName='Update';
                $scope.ClearbuttonName="Cancel";
            $scope.showCancel=true;
            },function errorCallback(response) {
            })
        }
        $scope.clearTimeData=function(){
	           $state.go('dashboard.customer.time',{}, { reload: true });
        }
        $scope.deleteTimeData=function(timeId){
            time_id=timeId;
            var delTimeData=$.param({'action':'timeDel','time_id':time_id});
            if($window.confirm('Are you sure to delete this record ?')){
                $http({
                    method:'POST',
                    url:"php/customerInfo.php",
                    data:delTimeData,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).then(function successCallback(response){
                    console.log('del',response.data);
                     alert(response.data['msg']);
			        $state.go('dashboard.customer.time',{}, { reload: true });
                },function errorCallback(response) {
                    alert(response.data['msg']);
			        $state.go('dashboard.customer.time',{}, { reload: true });
                })
            }
        }
    
});
time.directive('timePicker', [function () {
        return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            model: '=ngModel'
        },
        link: function ($scope, element, attrs, ngModelCtrl) {
            element.timepicker();
            element.on('changeTime', function () {
              $scope.$apply(function () {
                $scope.model = element.val();
              });
            });
   
        }
    }
}]);
time.filter('startsLetter', function () {
    return function (items, search) {
		//console.log('items',items);
		if(items != undefined){
			if (!search) {
			return items;
		  }
		  search = search.toLowerCase();
		  return items.filter(function(element) {
			return element.rest_name.toLowerCase().indexOf(search) != -1;
		  });
		}
    };
});
time.factory('focusTimeInputField',function($timeout, $window) {
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
var dashboard=angular.module('Spesh');
dashboard.controller('adminCatCategoryController',function($scope,$http,$location,$window,$state,Upload,focusInputField){	
     //$scope.profileData=[];
	 var ele=$window.document.getElementById('catname');
	 ele.focus();
	 ele.style.borderColor="#cccccc";
	 
	 $scope.readcolg = "true";
	 $scope.buttonName="Cancel";
	 $scope.cancelButton=false;
	 
	 $scope.submitbuttonName="Add";
	 $scope.submitButton=true;
	 
	 var catid='';							  
	
	
	$http({
		method: 'GET',
		url: "php/category/getAllCatageoryData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.objUserData=response.data;
	},function errorCallback(response) {
		
	});
	
	$scope.listOfSpecial=[{
		name:'Select  Special',
		value:''
	}]
	$scope.special=$scope.listOfSpecial[0];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=special",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.special_name,'value':obj.special_id};
			$scope.listOfSpecial.push(data);
		})
	},function errorCallback(response) {
	})
	 	
		//////////////////////////////////////////////////////////////////////////////////////////////
		$scope.addCategory=function(billdata){
			
		if(billdata.$valid){
			console.log('order',$scope.theValue==null,$scope.theValue=='');
			if($scope.submitbuttonName == "Add"){
			if($scope.special.value==null || $scope.special.value==''){
				focusInputField.borderColor('special');
				alert('Please add Special');
			}else if($scope.catname==null || $scope.catname==''){
				focusInputField.borderColor('catname');
				alert('Please add category name');
			}else if($scope.theValue==null || $scope.theValue==''){
				alert('Please add order number');
			}else if($scope.catdesc==null || $scope.catdesc==''){
				focusInputField.borderColor('catdesc');
				alert('Please add category description');
			}else{
					var supplierData=$("#billdata").serialize();
					supplierData+="&special="+$scope.special.value;
					//console.log('supplier',supplierData);
					$http({
							method:'POST',
							url:'php/category/addCategory.php',
							data:supplierData,
							headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
						}).then(function successCallback(response){
							//console.log('add data',response.data);
							alert(response.data['msg']);
							$state.go('dashboard.category.cat',{},{reload:true});
							
						},function errorCallback(response) {
							alert(response.data['msg']);
							
						})
				}
			}
			else if($scope.submitbuttonName == "Update"){
				if($scope.special.value==null || $scope.special.value==''){
					focusInputField.borderColor('special');
					alert('Please add Special');
			    }else if($scope.catname==null || $scope.catname==''){
					focusInputField.borderColor('catname');
					alert('Please add category name');
				}else if($scope.theValue==null || $scope.theValue==''){
                    alert('Please add order number');
                }else if($scope.catdesc==null || $scope.catdesc==''){
					focusInputField.borderColor('catdesc');
					alert('Please add category description');
				}else{
						var supplierData=$("#billdata").serialize();
							supplierData +="&cat_id="+catid+"&special="+$scope.special.value;
							//alert("::"+supplierData);
						$http({
								method:'POST',
								url:'php/category/updateCategory.php',
								data:supplierData,
								headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
							}).then(function successCallback(response){
								
								alert(response.data['msg']);
								$state.go('dashboard.category.cat',{},{reload:true});
								
							},function errorCallback(response) {
								alert(response.data['msg']);
								
							})
					}
			
			}
			
			
		}	
		else{
			if(billdata.theValue.$invalid){
                alert('This field needs only number(e.g-0,1..9)');
            }	
		}
	
	}
		
	/////////////////////////////////////////////////////////////////////////
	
	$scope.swapValueToUp=function(index,catid){
		var upid='';
		var upindex=parseInt(index)-1;
		for(var i=0;i<=$scope.objUserData.length;i++){
			if(upindex==i){
				upid=$scope.objUserData[i]['cat_id'];
			}
		}
		var swapid=$.param({'action':'up','currentid':catid,'upid':upid});
		$http({
			method:'POST',
			url:"php/category/swapValueToUp.php",
			data:swapid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
				//console.log('res',response.data);
				$scope.objUserData='';
				$scope.objUserData=response.data;
		},function errorCallback(response) {
		})
	}
	$scope.swapValueToDown=function(index,catid){
		var downid='';
		var upindex=parseInt(index)+1;
		for(var i=0;i<=$scope.objUserData.length;i++){
			if(upindex==i){
				downid=$scope.objUserData[i]['cat_id'];
			}
		}
		var swapid=$.param({'action':'down','currentid':catid,'downid':downid});
		$http({
			method:'POST',
			url:"php/category/swapValueToUp.php",
			data:swapid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
				//console.log('res',response.data);
				$scope.objUserData='';
				$scope.objUserData=response.data;
		},function errorCallback(response) {
		})
	}
	$scope.editCatagoryData =function(cat_id){
		
		catid = cat_id;
		var supplierData= "cat_id="+cat_id;
		$http({
		method:'POST',
		url:'php/category/getCatData.php',
		data:supplierData,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
		console.log('res',response.data);
		$scope.special.value=response.data[0].special;
		$scope.catname=response.data[0].cat_name;
		$scope.catdesc=response.data[0].cat_desc;	
		$scope.theValue=parseInt(response.data[0].order_no);			
		$scope.cancelButton=true;
	 	$scope.submitbuttonName="Update";
		//alert(response.data['msg']);
		//$state.go('dashboard.category.cat',{},{reload:true});
							
		},function errorCallback(response) {
		alert(response.data['msg']);
		})
	}
	
	
	$scope.removeCatagoryData =function(cat_id){
		if(confirm("Are you sure want to delete category?")){
			catid = cat_id;
			var supplierData= "cat_id="+cat_id;
			//alert(supplierData);
			$http({
			method:'POST',
			url:'php/category/deleteCategory.php',
			data:supplierData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('dashboard.category.cat',{},{reload:true});
			},function errorCallback(response) {
			alert(response.data['msg']);
			})
		
		}
	}
	
	
	
	$scope.cancelUpdate =function(){
		
		$state.go('dashboard.category.cat',{},{reload:true});
		
	
	}
	
		
	//////////////////////////////////////////////////////////////////////////
	
	$scope.onFileSelect1 = function($files,id) {
		 fileURL1=$files;
		 $scope.imageData1='';
		 $scope.clearField(id);
	}
	
	$scope.uploadFile1 = function(event){
    	var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded1; 
             reader.readAsDataURL(file);
     }
    };
	
	$scope.imageIsLoaded1 = function(e){
	
    $scope.$apply(function() {
		$scope.myImage1=e.target.result;
		$scope.attchImage1='';
		$scope.attchImage1=$scope.myImage1;
    });
	}
	
	////////////////////////////////////////////////////////////////////////
	
	$scope.onFileSelect2 = function($files,id) {
		 fileURL2=$files;
		 $scope.imageData2='';
		 $scope.clearField(id);
	}
	
	$scope.uploadFile2 = function(event){
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
			//alert("aaa111");
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded2; 
             reader.readAsDataURL(file);
     }
    };
	
	$scope.imageIsLoaded2 = function(e){
	
    $scope.$apply(function() {
		$scope.myImage2=e.target.result;
		$scope.attchImage2='';
		$scope.attchImage2=$scope.myImage2;
    });
	}
	///////////////////////////////////////////////////////////////////////
	
	$scope.onFileSelect3 = function($files,id) {
		 fileURL3=$files;
		 $scope.imageData3='';
		 $scope.clearField(id);
	}
	
	$scope.uploadFile3 = function(event){
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
			//alert("aaa111");
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded3; 
             reader.readAsDataURL(file);
     }
    };
	
	$scope.imageIsLoaded3 = function(e){
	
    $scope.$apply(function() {
		$scope.myImage3=e.target.result;
		$scope.attchImage3='';
		$scope.attchImage3=$scope.myImage3;
    });
	}
	
	////////////////////////////////////////////////////////////////////
	
	
	$scope.onFileSelect4 = function($files,id) {
		 fileURL4=$files;
		 $scope.imageData4='';
		 $scope.clearField(id);
	}
	
	$scope.uploadFile4 = function(event){
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
			//alert("aaa111");
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded4; 
             reader.readAsDataURL(file);
     }
    };
	
	$scope.imageIsLoaded4 = function(e){
	
    $scope.$apply(function() {
		$scope.myImage4=e.target.result;
		$scope.attchImage4='';
		$scope.attchImage4=$scope.myImage4;
    });
	}
	
	///////////////////////////////////////////////////////////////////////
	/*
	$scope.onFileSelect1 = function($files,id) {
		 fileURL=$files;
		 $scope.imageData='';
		 focusInputField.clearBorderColor(id);
	}
	
	$scope.uploadFile1 = function(event){
		//alert("BBB::");
		//console.log('event',event.target.files);
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
			//alert("aaa111");
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded1; 
             reader.readAsDataURL(file);
     }
    };
	$scope.imageIsLoaded1 = function(e){
		//alert("bbbb");
    $scope.$apply(function() {
		//var data={'image':e.target.result};
       // $scope.stepsModel.push(data);
	   alert("aaa");
		$scope.myImage=e.target.result;
		$scope.attchImage1='';
		$scope.attchImage1=$scope.myImage;
    });
	}
	
	$scope.uploadFile2 = function(event){
		//alert("aaa::"+event.target.files);
		console.log('event',event.target.files);
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
			//alert("aaa111");
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded2; 
             reader.readAsDataURL(file);
     }
    };
	$scope.imageIsLoaded2 = function(e){
		//alert("bbbb");
    $scope.$apply(function() {
		//var data={'image':e.target.result};
       // $scope.stepsModel.push(data);
		$scope.myImage=e.target.result;
		$scope.attchImage2='';
		$scope.attchImage2=$scope.myImage;
    });
	}
	
	$scope.uploadFile3 = function(event){
		//alert("aaa::"+event.target.files);
		console.log('event',event.target.files);
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
			//alert("aaa111");
         var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded3; 
             reader.readAsDataURL(file);
     }
    };
	$scope.imageIsLoaded3 = function(e){
		//alert("bbbb");
    $scope.$apply(function() {
		//var data={'image':e.target.result};
       // $scope.stepsModel.push(data);
		$scope.myImage=e.target.result;
		$scope.attchImage3='';
		$scope.attchImage3=$scope.myImage;
    });
	}
	*/
	
	
	$scope.checkBikeType=function(id)
	{
		$scope.clearField(id);
	}


	$scope.checkGenderType=function(id)
	{
		$scope.clearField(id);
	}
	
	$scope.checkServiceType=function(id)
	{
		$scope.clearField(id);
	}
	
	
	$scope.checkIdentityType=function(id)
	{
		$scope.clearField(id);
		var element = document.getElementById(id);
		var value = element.options[element.selectedIndex].text;
		 $scope.imagetype= value;
		if(element.selectedIndex > 0)
			document.getElementById("identitylabel").innerHTML = value+" No :";
		else
		{
			document.getElementById("identitylabel").innerHTML = "Identity Proof No :";
			 $scope.imagetype="Identity Proof";
		}
	}
	
	$scope.checkCATType=function(id)
	{
		$scope.clearField(id);
	}
	
	
	
	
	$scope.editProfileData=function(pid){
		id=pid;
		var editid={'id':id};
		$http({
			method:'POST',
			url:"php/profile/editProfileData.php",
			data:editid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colgname1=response.data[0].colg_name;
		    $scope.shortname=response.data[0].short_name;
		    $scope.address=response.data[0].address;
		    $scope.contno=response.data[0].cont_no;
		    $scope.colgemail=response.data[0].email;
			$scope.colgcode=response.data[0].code;
			$scope.buttonName="Update";
			$scope.ClearbuttonName="Cancel"
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	
	$scope.deleteProfileData=function(pid){
		var id=pid;
		var editid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this profile');
		if(conmesg){
			$http({
				method:'POST',
				url:"php/profile/deleteProfileData.php",
				data:editid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				$state.go('dashboard.profile',{}, { reload: true });
			},function errorCallback(response) {
				alert(response.data['msg']);
				//$state.go('dashboard.profile',{}, { reload: true });
			});
		}
	}
	
	$scope.clearField=function(id){
		focusInputField.clearBorderColor(id);
	}
	
	$scope.clearProfileData=function(){
		$state.go('dashboard.profile',{}, { reload: true });
	}
	if($scope.colgname1=='' || $scope.colgname1==null){
		focusInputField.clearBorderColor('colgmname1');
	}else if($scope.shortname==null || $scope.shortname== ''){
		focusInputField.clearBorderColor('procolgsubtitle');
	}else if($scope.contno==null || $scope.contno==''){
		focusInputField.clearBorderColor('procolgcontactno');
	}else if($scope.colgemail==null || $scope.colgemail==''){
		focusInputField.clearBorderColor('procolgmail');
	}else if($scope.colgcode==null || $scope.colgcode==''){
		focusInputField.clearBorderColor('procolgcode');
	}else{
		focusInputField.clearBorderColor('procolgaddress');
	}
});

dashboard.factory('focusInputField',function($timeout, $window) {
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
dashboard.directive('customOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.customOnChange);
      element.bind('change', onChangeHandler);
    }
  };
});
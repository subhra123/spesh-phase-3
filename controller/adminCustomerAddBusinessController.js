var custom=angular.module('Spesh');
custom.controller('adminCustomerAddBusinessController',function($http,$state,$scope,$window,$timeout,Upload){
	$scope.attchImage1="img/no_image.jpg";
	var fileURL='';
	$scope.mobscreen=1;
	$scope.listOfRestaurant=[];
	var id='';
	$scope.imageData='';
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=restaurant",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('res',response.data);
		angular.forEach(response.data,function(obj){
			var spec=obj.special;
			//console.log('obj data',spec);
			var found = spec.split(",").indexOf("1") > -1;
			if (found==true) {
				var data={'name':obj.rest_name,'value':obj.member_id};
			    $scope.listOfRestaurant.push(data);
			}
		})
	},function errorCallback(response) {
	})
	$scope.buttonName="Add";
	$scope.uploadFile = function(event){
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var reader = new FileReader();
            reader.onload = $scope.imageIsLoaded; 
            reader.readAsDataURL(file);
        }
    };
	$scope.imageIsLoaded = function(e){
    $scope.$apply(function() {
        $scope.attchImage1=e.target.result;
    });
	}
	$scope.onFileSelect = function($files) {
		fileURL=$files;
		$scope.imageData='';
	}
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=getPromotion",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.listOfBusiData=response.data;
	},function errorCallback(response) {
	})
	$scope.addBusinessDetails=function(billdata){
		console.log('radio',$scope.mobscreen);
		if(billdata.$valid){
			if ($scope.restaurant.value==null || $scope.restaurant.value=='') {
				alert('Please select the business name');
			}else if($scope.mobscreen=='' && $scope.mobscreen !=0 ){
				alert('Please select any of one option to display this business on mobile screen');
			}else if ($scope.file==null && $scope.imageData =='') {
				alert('Please select the logo');
			}else{
				if($scope.buttonName=="Add"){
					var file=fileURL;
	                var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
	                var newpath=today+"_"+ file.name;
	                file = Upload.rename(file, newpath);
	                $scope.upload = Upload.upload({
	                	url: 'php/upload.php',
	                	method: 'POST',
	                	file: file
	                }).success(function(data, status, headers, config) {
	                	var addData=$.param({'action':'addBusinessForMobile','member_id':$scope.restaurant.value,'status':$scope.mobscreen,'image':newpath});
	                	$http({
	                		method:'POST',
	                		url:"php/customerInfo.php",
	                		data:addData,
	                		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	                	}).then(function successCallback(response){
	                		console.log('succ',response.data);
	                		alert(response.data['msg']);
	                		$state.go('dashboard.customer.addbusiness',{}, { reload: true });
	                	},function errorCallback(response) {
	                	})
	                }).error(function(data, status) {
	                })
	            }
	            if($scope.buttonName=="Update"){
	            	if($scope.imageData!=''){
	            		var updateData=$.param({'action':'updateBusinessForMobile','member_id':$scope.restaurant.value,'status':$scope.mobscreen,'image':$scope.imageData,'bid':id});
	            		$http({
	            			method:'POST',
	            			url:"php/customerInfo.php",
	            			data:updateData,
	            			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	            		}).then(function successCallback(response){
	            			alert(response.data['msg']);
	                		$state.go('dashboard.customer.addbusiness',{}, { reload: true });
	            		},function errorCallback(response) {
	            		})
	            	}
	            	if ($scope.imageData =='') {
	            		var file=fileURL;
	                    var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
	                    var newpath=today+"_"+ file.name;
	                    file = Upload.rename(file, newpath);
	                    $scope.upload = Upload.upload({
		                	url: 'php/upload.php',
		                	method: 'POST',
		                	file: file
		                }).success(function(data, status, headers, config) {
		                	var updateData=$.param({'action':'updateBusinessForMobile','member_id':$scope.restaurant.value,'status':$scope.mobscreen,'image':newpath,'bid':id});
		                	$http({
		                		method:'POST',
		                		url:"php/customerInfo.php",
		                		data:updateData,
		                		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		                	}).then(function successCallback(response){
		                		alert(response.data['msg']);
		                		$state.go('dashboard.customer.addbusiness',{}, { reload: true });
		                	},function errorCallback(response) {
		                	})
		                }).error(function(data, status) {
		                })
	            	}
	            }
			}
		}
	}
	$scope.editBusinessData=function(bid){
		id=bid;
		var editData=$.param({'action':'editPromotion','bid':id});
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:editData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.restaurant={};
			$scope.restaurant.value=response.data[0].member_id;
			$scope.mobscreen=response.data[0].status;
			if(response.data[0].image !=''){
				$scope.attchImage1="upload/"+response.data[0].image;
				$scope.imageData=response.data[0].image;
			}
			$scope.buttonName="Update";
			$scope.ClearbuttonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		})
	}
	$scope.clearBusinessData=function(){
	    $state.go('dashboard.customer.addbusiness',{}, { reload: true });
    }
    $scope.inactiveBusinessData=function(bid,status){
    	if (status==1) {
    		var sta=0;
    	}else{
    		var sta=1;
    	}
    	var data=$.param({'action':'changeStatus','status':sta,'bid':bid});
    	$http({
    		method:'POST',
    		url:"php/customerInfo.php",
    		data:data,
    		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    	}).then(function successCallback(response){
    		 $state.go('dashboard.customer.addbusiness',{}, { reload: true });
    	},function errorCallback(response) {
    	})
    }
    $scope.deleteBusinessData=function(bid){
    	id=bid;
    	var delData=$.param({'action':'deletePromotion','bid':id});
    	if($window.confirm('Are you want to delete ?')){
		   $http({
			   method:'POST',
			   url:"php/customerInfo.php",
			   data:delData,
			   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		   }).then(function successCallback(response){
			   alert(response.data['msg']);
			   $state.go('dashboard.customer.addbusiness',{}, { reload: true });
		   },function errorCallback(response) {
			   alert(response.data['msg']);
			   $state.go('dashboard.customer.addbusiness',{}, { reload: true });
		   })
	    }
    }

});
custom.filter('startsLetter', function () {
    return function (items, search) {
		//console.log('items',items);
		if(items != undefined){
			if (!search) {
			return items;
		  }
		  search = search.toLowerCase();
		  return items.filter(function(element) {
			return element.rest_name.toLowerCase().indexOf(search) != -1;
		  });
		}
    };
});
var plan=angular.module('Channabasavashwara');
plan.controller('princpalPlanController',function($scope,$http,$state,$window,focusStreamField){
	
	
	$scope.user_readonly= false;
	$scope.buttonName="Add";
	$scope.showAddUnit=false;
	
	$scope.unit_list=true;
	$scope.plan_list=false;
	
	$scope.topic = "";
	$scope.clearButtonName="Cancel";
	$scope.showCancel=false;
	var id='';
	
	$scope.listOfStream=[{
		name:'Select Stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
	
	$scope.listPlanData=[{
		name:'Select Lecture Plan',
		value:'0'
	}
	];
	$scope.plan=$scope.listPlanData[0];
	$scope.listOfdept=null;
	$scope.listOfdept=[{
		name:'Select Department',
		value:''
	}]
	$scope.dept_name=$scope.listOfdept[0];
	$scope.listOfCourse=[{
		name:'Select Course',
		value:''
		}];
	$scope.course_name=$scope.listOfCourse[0];
	$scope.listOfFillterdept=[{
		name:'select department',
		value:''
	}]
	$scope.deptName=$scope.listOfFillterdept[0];
	/*$http({
		method: 'GET',
		url:"php/course/getUserCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.course_name,'value':obj.course_id};
			$scope.listOfCourse.push(data);
		});
	},function errorCallback(response) {
		
	});*/
		
	
	$scope.listOfLession=[{
		name:'Select Lession plan',
		value:''
	}
	];
	
	$scope.new_unit_name="";
	$scope.viewUnitData=[];
	$scope.viewPlanData=[];
	$scope.listUnitData=[];
	$scope.listUnitData=[{
		name:'Select Unit',
		value:''
	}
	];
	$scope.unit_name=$scope.listUnitData[0];
	
	
	$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Academic Year',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];
	
	
	/*$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Subject',
		value: ''
	}]*/
	/*$scope.listOfStream=null;
	$scope.listOfStream=[{
		name:'Select Stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];*/
	
	$scope.listSubject=[{
			name: 'Select Subject',
			value: ''
		}]
	/*$http({
		method: 'GET',
		url:"php/stream/readCollegeStreamData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.stream_name,'value':obj.stream_id};
			$scope.listOfStream.push(data);
		});
	},function errorCallback(response) {
		
	});*/
	$scope.sub_name = $scope.listSubject[0];
	
	/*$http({
		method: 'GET',
		url: "php/plan/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});*/
	
	
		$http({
		method:'GET',
		url:"php/userplan/getLession.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.lession_name,'value':obj.lession_id};
			$scope.listOfLession.push(data);
		});
		},function errorCallback(response) {
		});



		$scope.lession=$scope.listOfLession[0];
		
	
		
	
	$scope.noSemesters=[{
		name:'Select Semester',
		value:''
	}];
	
	$scope.semester=$scope.noSemesters[0];
	
	$scope.listOfSubject=[{
		name:'Select Subject',
		value:''
	}];
	
	
	$scope.subject_name=$scope.listOfSubject[0];
	
	$scope.listOfSection=[{
		name:'Select section',
		value:''
	}];
	$scope.listFaculty=[];
	$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
	$scope.faculty_name=$scope.listFaculty[0];	
	/*$http({
		method:'GET',
		url:"php/userplan/getSectionData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj2){
			var data={'name':obj2.section_name,'value':obj2.section_id};
			$scope.listOfSection.push(data);
		});
	},function errorCallback(response) {
	});*/
	$scope.section=$scope.listOfSection[0];
	$scope.noSemesters=[];
	
	
	$scope.selectedCourse=function(id){
			//alert("::"+id);
			focusStreamField.removeBorderColor(id);
				
		     $scope.noSemesters=null;
			 $scope.noSemesters=[];
			 var key=['I','II','III','IV','V','VI','VII','VIII','IX','X'];
			 var userid={'course_id':$scope.course_name.value};
			 $http({
				 method:'POST',
				 url:"php/course/getPrincipalPlanCollegeSemester.php",
				 data: userid,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			 }).then(function successCallback(response){
				 if($scope.course_name.name==response.data[0].course_name){
					 for(var i=0;i<response.data[0].semester;i++){
						 var val=i+1;
						 var rom_val=key[i];
						 var sem={'name':rom_val,'value':val};
						 $scope.noSemesters.push(sem);
					 }
				 }
			 },function errorCallback(response) {
				 $scope.noSemesters=null;
			 });
	}
	
	
	$scope.selectedSemester=function(id){
		focusStreamField.removeBorderColor(id);
		
		$scope.listOfSubject=null;
		$scope.listOfSubject=[];
		$scope.listOfSubject=[{
			name: 'Select Subject',
			value: ''
		}]
		//var colgid=$scope.colg_name.value;
		var deptid=$scope.dept_name.value;
		var strmid=$scope.stream_name.value;
		var course_id = $scope.course_name.value;
		var semester_id = $scope.semester.value;
		//alert("semester_id::"+semester_id);
		var userdata={'dept_id':deptid,'stream_id':strmid,'course_id':course_id,'semester_id':semester_id};
		$http({
			method: 'POST',
			url: "php/userplan/getPrincipalSubjectData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			var Subject={'name':obj.type+" : "+obj.subject_name , 'value':obj.subject_id};
			$scope.listOfSubject.push(Subject);
			
			});
		},function errorCallback(response) {
		});	
		
	}
	
	/*........START Get  department from unit_plan table....*/
	
	$scope.deptName=$scope.listOfFillterdept[0];
		$http({
		method:'POST',
		url:"php/princpalplan/getDeptforUnitPlan.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('wds',response.data);
		angular.forEach(response.data,function(clg){
			var dept={'name':clg.dept_name,'value':clg.dept_id};
			$scope.listOfFillterdept.push(dept);
		});
	},function errorCallback(response) {
	});
	$scope.getRecordUsingDept=function(){
		if($scope.deptName.value!=''){
		var userdata={'dept_id':$scope.deptName.value};
		$http({
		method:'POST',
		url:"php/princpalplan/readAllCompletedColgDeptUnitData.php",
		data:userdata,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.showTable=true;
		$scope.viewUnitData=response.data;
		//console.log('respon',response.data);
	},function errorCallback(response) {
	});
		}else{
			$scope.showTable=false;
		}
	}
	$scope.setDepartment=function(deptid){
		$http({
		method:'POST',
		url:"php/princpalplan/getDeptforUnitPlan.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('wds',response.data);
		angular.forEach(response.data,function(clg){
			var dept={'name':clg.dept_name,'value':clg.dept_id};
			$scope.listOfFillterdept.push(dept);
			if(clg.dept_id==deptid){
				$scope.deptName.value=deptid;
				$scope.showTable=true;
			}
		});
	},function errorCallback(response) {
	});
	}
	
	
	/*........END Get  department from unit_plan table....*/
	
	/*$http({
		method:'GET',
		url:"php/userplan/readPrincipalPlanUnitData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('res',response.data);
		$scope.viewUnitData=response.data;
	},function errorCallback(response) {
	});*/
	
	
	
	$scope.addPlanData=function(){
		//alert("::"+$scope.buttonName);
		if($scope.buttonName=="Add"){
		if($scope.stream_name.value==null || $scope.stream_name.value==''){
			alert('Please select stream');
			focusStreamField.borderColor('stream_name');
		}else if($scope.dept_name.value==null || $scope.dept_name.value==''){
			alert('Please select Department');
			focusStreamField.borderColor('dept_name');
		}else if($scope.course_name.value==""){
			alert('Please select course');
			focusStreamField.borderColor('course_name');
		}else if($scope.semester.value==""){
			alert('Please select semester');
			focusStreamField.borderColor('semester');
		}else if($scope.subject_name.value==""){
			alert('Please select subject');
			focusStreamField.borderColor('subject_name');
		}else if($scope.faculty_name.value==null || $scope.faculty_name.value==''){
			alert('Please select Faculty');
			focusStreamField.borderColor('faculty_name');
		}else if($scope.section.value==""){
			alert('Please select section');
			focusStreamField.borderColor('section');
		}else if($scope.session_name.value==""){
			alert('Please select Academic Year');
			focusStreamField.borderColor('session_name');
		}else if($scope.unit_name.value==""){
			alert('Please select unit');
			focusStreamField.borderColor('unit_name');
		}else if($scope.date==null){
			alert('Please select date');
			focusStreamField.borderColor('date');
		}else if($scope.plan.name==null || $scope.plan.value=='0'  ){
			alert('Please add Lecture Plan');
			focusStreamField.borderColor('plan');
		}else if($scope.topic==''){
			alert('Please add the topic');
			focusStreamField.borderColor('topic');
		}else{
			
			//alert(":::Topic:"+$scope.topic);
			
			var dataString ="stream_id="+$scope.stream_name.value+"&dept_id="+$scope.dept_name.value+"&user_id="+$scope.faculty_name.value+ "&course_id="+$scope.course_name.value+"&semester="+$scope.semester.value+"&subject_id="+$scope.subject_name.value+"&section_id="+$scope.section.value+"&session_id="+$scope.session_name.value+"&unit_id="+$scope.unit_name.value+"&date="+$scope.date+"&lession_plan="+$scope.plan.name+"&topic="+$scope.topic;
			//alert("dataString:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/userplan/addPrincipalPlanData.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				if(dobj.result == 0)
				{
					alert(dobj.error);
				}
				else
				{
					var userdata={'dept_id':$scope.dept_name.value,'user_id':$scope.faculty_name.value};
					$scope.viewUnitData= null;
					$http({
					method:'POST',
					url:"php/userplan/readPrincipalUnitData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
					//console.log('res',response.data);
					$scope.viewUnitData=response.data;
					if(response.data.dept_id != ''){
						$scope.setDepartment(response.data[0].dept_id);
					}
					},function errorCallback(response) {
					});
					$scope.date = null;
					$scope.plan = null;
					$scope.topic = null;
					alert("New Plan added successfully...");
				}
			} 
			});
			
			
			
			
		}
		
		
		}
		if($scope.buttonName=="Update"){
			//alert("aaaaaaa");
			if($scope.date==null){
			alert('Please select date');
			}else if($scope.plan.name==null || $scope.plan.value=='0' ){
			alert('Please add  Lecture plan');
			}else if($scope.topic==null){
			alert('Please add topic');
			}else{
			
			var dataString = "unit_plan_id="+temp_unit_id+"&plan_id="+id+"&date="+$scope.date+"&lession_plan="+$scope.plan.name+"&topic="+$scope.topic;
			//alert("::"+dataString);
			
			$.ajax({ 
			type: "POST",url: "php/userplan/updatePlanData.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.result);
				if(dobj.result == 0)
				{
					alert(dobj.error);
				}
				else
				{
					var updatedata={'unit_id':temp_unit_id};
		
					$scope.viewPlanData=null;
					$scope.viewPlanData=[];
			
					$http({
						method:'POST',
						url:"php/userplan/readPlanData.php",
						data:updatedata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						console.log('res',response.data);
						$scope.viewPlanData=response.data;
						//document.getElementById("unit_list").style.display = "none";
						//document.getElementById("plan_list").style.display = "block";
					},function errorCallback(response) {
						
						
					});
					
					$scope.date = null;
					$scope.plan = null;
					$scope.topic = null;
					$scope.clearPlanData();
					alert("Plan updated successfully...");
				
				}
			} 
			});
			
			
			
			
			
		}
		}
	}
	
	$scope.addUnit=function(){
		if($scope.session_name.value==''){
			alert('Please select Academic Year');
		}else if($scope.course_name.value==''){
			alert('Please select the course name');
		}else if($scope.semester.value==''){
			alert('Please select the semester');
		}else if($scope.subject_name.value==''){
			alert('Please select the subject name');
		}else if($scope.section.value==''){
			alert('Please select the section');
		}else{
				//alert("Add Unit");
				document.getElementById("addunit").style.display ="block";
			}
	}
	
	
	$scope.checkTableData=function(id){
		
		focusStreamField.removeBorderColor(id);
		
		if($scope.session_name.value != "" )
		{
			$scope.listUnitData=[];
			$scope.listUnitData=[{
				name:'Select Unit',
				value:''
			}
			];
	        var unitdata={'dept_id':$scope.dept_name.value,'user_id':$scope.faculty_name.value};
			$http({
				method:'POST',
				url:"php/userplan/getPrincipalUnitName.php",
				data:unitdata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data, function(obj){
				var Session={'name':obj.unit_name , 'value':obj.unit_id};
					$scope.listUnitData.push(Session);
				});
				
			},function errorCallback(response) {
				
				
			});
		}
		
	}
	
	
	$scope.closeUnitDialog=function(){
			document.getElementById("addunit").style.display ="none";
	}
	
	$scope.addNewUnitName=function(){

		//alert("::"+$scope.new_unit_name);
		var unit_name = $scope.new_unit_name;
		//alert("unit_name::"+unit_name+"::");
		if(unit_name == "")
		{
			alert("Add Unit Name");
			return;
		}
		var session_id = $scope.session_name.value;
		var course_id = $scope.course_name.value;
		var semester_id = $scope.semester.value;
		var subject_id = $scope.subject_name.value;
		var section_id = $scope.section.value;
		
		
		var dataString = "session_id="+session_id+"&course_id="+course_id+"&semester_id="+semester_id+"&subject_id="+subject_id+"&section_id="+section_id+"&unit_name="+unit_name;
			
			$.ajax({ 
			type: "POST",url: "php/userplan/addUnitName.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.result);
				if(dobj.result == 0)
				{
					alert(dobj.error);
				}
				else
				{
					//$scope.checkTableData();
					document.getElementById("addunit").style.display ="none";
					
					$scope.viewUnitData= null;
					$http({
					method:'GET',
					url:"php/userplan/readUnitData.php",
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
					//console.log('res',response.data);
					$scope.viewUnitData=response.data;
					},function errorCallback(response) {
					});
					
					
					
					
				}
			} 
			});
	}
	
	$scope.getLecturePlan=function(plan_name){
		   //console.log('plan name',plan_name,$scope.plan);
		   $scope.listPlanData=null;
			$scope.listPlanData=[{
				name:'Select Lecture Plan',
				value:'0'}];
			$scope.plan=$scope.listPlanData[0];
			$http({
				method:'GET',
				url:"php/userplan/getPlanName.php",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				// console.log('plan ',response);
				angular.forEach(response.data, function(obj){
				var Session={'name':obj.plan_name , 'value':obj.lp_id};
					$scope.listPlanData.push(Session);
					if(obj.plan_name==plan_name){
						$scope.plan.value=obj.lp_id;
					
					}
				});
			},function errorCallback(response) {
			});
	}
	
	$scope.editStream=function(strmid,sesid,secid){
		//console.log('sec',strmid,sesid,secid);
		$http({
		method: 'GET',
		url:"php/stream/readPricpalStreamData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.stream_name,'value':obj.stream_id};
			$scope.listOfStream.push(data);
			$scope.stream_name.value=strmid;
		});
	},function errorCallback(response) {
		
	});
	$http({
		method: 'GET',
		url: "php/timetable/readPricpalPlanSession.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		//console.log('session',response);
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
			$scope.session_name.value=sesid;
		});
	},function errorCallback(response) {
		
	});
	$http({
		method: 'GET',
		url: "php/timetable/readPricpalPlanSection.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		//console.log('sec',response);
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.section_name);									
			var Section={'name':obj.section_name , 'value':obj.section_id};
			$scope.listOfSection.push(Section);
			$scope.section.value=secid;
		});
	},function errorCallback(response) {
		
	});		
	
	}
	$scope.editDept=function(strmid,deptid,crsid){
		$scope.listOfdept=null;
		$scope.listOfdept=[{
		name:'Select Department',
		value:''
	    }]
	    $scope.dept_name=$scope.listOfdept[0];
		
		var courseid={'stream_id':strmid}
		$http({
			method:'POST',
			url:"php/deptsubject/readPrincipalDeptValue.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.dept_name,'value':obj.dept_id};
				$scope.listOfdept.push(data);
				$scope.dept_name.value=deptid;
			});
		},function errorCallback(response) {
		});
		
		$scope.listOfCourse=null;
		$scope.listOfCourse=[{
			name:'Select Course',
			value:''
		}]
		$scope.course_name=$scope.listOfCourse[0];
		$http({
			method:'POST',
			url:"php/course/readPrincipalPlanCourseData.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('course1',response);
			angular.forEach(response.data,function(obj){
				var data={'name':obj.course_name,'value':obj.course_id};
				$scope.listOfCourse.push(data);
				$scope.course_name.value=crsid;
			});
		},function errorCallback(response) {
		});
	}
	$scope.editFaculty=function(deptid,userid){
		$scope.listFaculty=null;
		$scope.listFaculty=[];
		$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
	$scope.faculty_name=$scope.listFaculty[0];
	var deptid={'deptid':deptid}
	$http({
		method: 'POST',
		url: "php/princpaltime/readPrincipalPlanFaculty.php",
		data:deptid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		if(response.data=="null"){
			$scope.listFaculty=null;
		$scope.listFaculty=[];
		$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
		}else{
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.user_name);									
			var Faculty={'name':obj.role+" : "+obj.user_name , 'value':obj.user_id};
			$scope.listFaculty.push(Faculty);
			$scope.faculty_name.value=userid;
		});
		}
	},function errorCallback(response) {
		
	});
	}
	$scope.editSemester=function(crsid,semid){
		     $scope.noSemesters=null;
			 $scope.noSemesters=[{
		name:'Select Semester',
		value:''
	}];
	
	$scope.semester=$scope.noSemesters[0];
			 var key=['I','II','III','IV','V','VI','VII','VIII','IX','X'];
			 var userid={'course_id':crsid};
			 $http({
				 method:'POST',
				 url:"php/course/getPrincipalPlanCollegeSemester.php",
				 data: userid,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			 }).then(function successCallback(response){
				// if($scope.course_name.name==response.data[0].course_name){
					 for(var i=0;i<response.data[0].semester;i++){
						 var val=i+1;
						 var rom_val=key[i];
						 var sem={'name':rom_val,'value':val};
						 $scope.noSemesters.push(sem);
						 $scope.semester.value=semid;
					 }
				 
			 },function errorCallback(response) {
				 $scope.noSemesters=null;
			 });
	}
	$scope.editSubject=function(strmid,deptid,crsid,semid,subid){
		$scope.listOfSubject=null;
		$scope.listOfSubject=[{
			name: 'Select Subject',
			value: ''
		}]
		$scope.subject_name=$scope.listOfSubject[0];
		//var colgid=$scope.colg_name.value;
		
		//alert("semester_id::"+semester_id);
		var userdata={'dept_id':deptid,'stream_id':strmid,'course_id':crsid,'semester_id':semid};
		$http({
			method: 'POST',
			url: "php/userplan/getPrincipalSubjectData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			var Subject={'name':obj.type+" : "+obj.subject_name , 'value':obj.subject_id};
			$scope.listOfSubject.push(Subject);
			$scope.subject_name.value=subid;
			});
		},function errorCallback(response) {
		});	
	}
	$scope.editUnit=function(deptid,userid,unitid){
		
			$scope.listUnitData=null;
			$scope.listUnitData=[{
				name:'Select Unit',
				value:''
			}
			];
			$scope.unit_name=$scope.listUnitData[0];
	        var unitdata={'dept_id':deptid,'user_id':userid};
			$http({
				method:'POST',
				url:"php/userplan/getPrincipalUnitName.php",
				data:unitdata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data, function(obj){
				var Session={'name':obj.unit_name , 'value':obj.unit_id};
					$scope.listUnitData.push(Session);
					$scope.unit_name.value=unitid;
				});
				
			},function errorCallback(response) {
				
				
			});
		
	}
	$scope.editPlanData=function(pid,unit_id){
		id=pid;
		temp_unit_id = unit_id;
		var planid={'plan_id':id};
		$http({
			method:'POST',
			url:"php/userplan/editPlanData.php",
			data:planid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.editStream(response.data[0].stream_id,response.data[0].session_id,response.data[0].section_id);
			$scope.editDept(response.data[0].stream_id,response.data[0].dept_id,response.data[0].course_id);
			$scope.editFaculty(response.data[0].dept_id,response.data[0].user_id);
			$scope.editSemester(response.data[0].course_id,response.data[0].semester_id);
			$scope.editSubject(response.data[0].stream_id,response.data[0].dept_id,response.data[0].course_id,response.data[0].semester_id,response.data[0].subject_id);
			$scope.editUnit(response.data[0].dept_id,response.data[0].user_id,response.data[0].unit_id)
			$scope.getLecturePlan(response.data[0].plan);
			$scope.date=response.data[0].date;
			//$scope.plan=response.data[0].plan;
			$scope.topic=response.data[0].topic;
			$scope.user_readonly= true;
			$scope.readcolg=true;
			$scope.colgdis=true;
			$scope.readstrm=true;
			$scope.strmdis=true;
			$scope.readdept=true;
			$scope.deptdis=true;
			$scope.userread=true;
			$scope.userdis=true;
			$scope.showCancel =  true;
			$scope.buttonName="Update";
		
		},function errorCallback(response) {
		});
		
	}
	
	
	$scope.showPlanData=function(unit_id){
		//alert("unit_id:"+unit_id);
		var updatedata={'unit_id':unit_id};
		
		$scope.viewPlanData=null;
		$scope.viewPlanData=[];
			
			$http({
				method:'POST',
				url:"php/userplan/readPlanData.php",
				data:updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('res',response.data);
				$scope.viewPlanData=response.data;
				document.getElementById("unit_list").style.display = "none";
				document.getElementById("plan_list").style.display = "block";
			},function errorCallback(response) {
				
				
			});
		
		
	
	}
	
	$scope.showUnitData=function(){
		document.getElementById("unit_list").style.display = "block";
		document.getElementById("plan_list").style.display = "none";
	
	}
	
	
	
	$scope.deleteFrmUnitPlan=function(unit_id){
		var udata={'unit_id':unit_id};
		$http({
			method:'POST',
			url:"php/userplan/deleteAllData.php",
			data:udata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			if(response.data['msg']){
				$state.go('dashboard.plan.facultyplan',{}, { reload: true });
			}
		},function errorCallback(response) {
		})
	}
	
	
	
	
	$scope.deletePlanData=function(pid,unit_id){
		id=pid;
		var planid={'plan_id':id , 'unit_id':unit_id};
		var deleteUser = $window.confirm('Are you sure you want to delete?');
		if(deleteUser){
			$http({
			method: 'POST',
			url: "php/userplan/deletePlanData.php",
			data:planid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			
				var updatedata={'unit_id':unit_id};
		
					$scope.viewPlanData=null;
					$scope.viewPlanData=[];
			
					$http({
						method:'POST',
						url:"php/userplan/readPlanData.php",
						data:updatedata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						//console.log('res',response.data);
						$scope.viewPlanData=response.data;
						$scope.deleteFrmUnitPlan(unit_id);
					},function errorCallback(response) {
						
						
					});
		
					
		
		
		},function errorCallback(response) {
			//alert(response.data);
			//$state.go('user.plan',{}, { reload: true });
		});
		}
	}
	$scope.clearPlanData=function(){
		
		$scope.user_readonly= false;
		$scope.showCancel =  false;
		$scope.buttonName ="Add";
		$state.go('principal.plan.facultyplan',{}, { reload: true });
	}
	
	$scope.listSearchSession=[];
	$http({
		method: 'GET',
		url: "php/plan/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSearchSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	$scope.listOfSearchCourse=[];
	$http({
		method:'GET',
		url:"php/userplan/getCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj1){
			var data={'name':obj1.short_name,'value':obj1.course_id};
			$scope.listOfSearchCourse.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.selectedSearchCourse=function(){
		     $scope.listOfSearchSemester=null;
			 $scope.listOfSearchSemester=[];
			 var key=['I','II','III','IV','V','VI','VII','VIII','IX','X'];
			 var userid={'id':$scope.courseSearch.value};
			 //console.log('cid',userid);
			 $http({
				 method:'POST',
				 url:"php/userplan/getSemesterData.php",
				 data: userid,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			 }).then(function successCallback(response){
				// console.log('cid res',response.data);
				 if($scope.courseSearch.name==response.data[0].short_name){
					 for(var i=0;i<response.data[0].semester;i++){
						 var val=i+1;
						 var rom_val=key[i];
						 var sem={'name':rom_val,'value':val};
						 $scope.listOfSearchSemester.push(sem);
					 }
				 }
			 },function errorCallback(response) {
				 $scope.noSemesters=null;
			 });
	}
	$scope.selectedSearchSemester=function(){
		$scope.listOfSearchSubject=null;
		$scope.listOfSearchSubject=[];
		
		var course_id = $scope.courseSearch.value;
		var semester_id = $scope.semesterSearch.value;
		//alert("semester_id::"+semester_id);
		var userdata={'course_id':course_id,'semester_id':semester_id};
		$http({
			method: 'POST',
			url: "php/userplan/getSubjectData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			//alert("::"+obj.subject_name);									
			var Subject={'name':obj.subject_name , 'value':obj.subject_id};
			$scope.listOfSearchSubject.push(Subject);
			
			});
			
			
			
		},function errorCallback(response) {
		});	
	}
	$scope.listOfSearchSection=[];
	$http({
		method:'GET',
		url:"php/userplan/getSectionData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj2){
			var data={'name':obj2.section_name,'value':obj2.section_id};
			$scope.listOfSearchSection.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.listOfSearchUnit=[];
	$http({
				method:'POST',
				url:"php/userplan/getFilterUnitData.php",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('unit',response.data);
				angular.forEach(response.data,function(obj1){
					var unitvalue={'name':obj1.unit_name,'value':obj1.unit_name};
					$scope.listOfSearchUnit.push(unitvalue)
				})
			},function errorCallback(response) {
			});
	
	$scope.clearField=function(id){
		//alert("Clear:"+id);
		focusStreamField.removeBorderColor(id);
	}
	
	$scope.removeBorder=function(id){
		if($scope.colg_name != ''){
			focusStreamField.removeBorderColor(id);
		}
	}
	
	$scope.GetDeptData=function(id){
		focusStreamField.removeBorderColor(id);
		$scope.listOfdept=null;
		$scope.listOfdept=[{
		name:'Select Department',
		value:''
	    }]
	    $scope.dept_name=$scope.listOfdept[0];
		
		var courseid={'stream_id':$scope.stream_name.value}
		$http({
			method:'POST',
			url:"php/deptsubject/readPrincipalDeptValue.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.dept_name,'value':obj.dept_id};
				$scope.listOfdept.push(data);
			});
		},function errorCallback(response) {
		});
		
		$scope.listOfCourse=null;
		$scope.listOfCourse=[{
			name:'Select Course',
			value:''
		}]
		$scope.course_name=$scope.listOfCourse[0];
		$http({
			method:'POST',
			url:"php/course/readPrincipalPlanCourseData.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('course1',response);
			angular.forEach(response.data,function(obj){
				var data={'name':obj.course_name,'value':obj.course_id};
				$scope.listOfCourse.push(data);
			});
		},function errorCallback(response) {
		});
		
	}
	
 
	
	
		$http({
		method: 'GET',
		url:"php/stream/readPricpalStreamData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.stream_name,'value':obj.stream_id};
			$scope.listOfStream.push(data);
		});
	},function errorCallback(response) {
		
	});
	$http({
		method: 'GET',
		url: "php/timetable/readPricpalPlanSession.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		console.log('session',response);
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	$http({
		method: 'GET',
		url: "php/timetable/readPricpalPlanSection.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		console.log('sec',response);
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.section_name);									
			var Section={'name':obj.section_name , 'value':obj.section_id};
			$scope.listOfSection.push(Section);
		});
	},function errorCallback(response) {
		
	});		
	
	$scope.selectFaculty=function(id){
		focusStreamField.removeBorderColor(id);
		$scope.listFaculty=null;
		$scope.listFaculty=[];
		$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
	var deptid={'deptid':$scope.dept_name.value}
	$http({
		method: 'POST',
		url: "php/princpaltime/readPrincipalPlanFaculty.php",
		data:deptid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		if(response.data=="null"){
			$scope.listFaculty=null;
		$scope.listFaculty=[];
		$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
		}else{
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.user_name);									
			var Faculty={'name':obj.role+" : "+obj.user_name , 'value':obj.user_id};
			$scope.listFaculty.push(Faculty);
		});
		}
	},function errorCallback(response) {
		
	});
	
	}
	$http({
				method:'GET',
				url:"php/userplan/getPlanName.php",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data, function(obj){
				var Session={'name':obj.plan_name , 'value':obj.lp_id};
					$scope.listPlanData.push(Session);
				});
			},function errorCallback(response) {
			});
	
});


plan.factory('focusStreamField',function($timeout,$window){
	return{
		borderColor:function(id){
			var element = $window.document.getElementById(id);
			 if(element){
				 element.focus();
			     element.style.borderColor = "red";
			 }
		},
		removeBorderColor:function(id){
			$timeout(function(){
				var element = $window.document.getElementById(id);
				if(element){
					 element.style.borderColor = "#cccccc";
				}
			})
		}
	}
});
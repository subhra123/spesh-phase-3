var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('adminResourceTimeController',function($scope,$http,$state,$window,venueField){
	$scope.buttonName="Update";
	$scope.periods = "";
	var id='';
	$scope.listOfCollege=[{
		name:'Select college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	
		
	
	$scope.selectDays=function(){
			document.getElementById("addtimedata").style.display  = "none";
			$("#detailsstockid").html("");
			if($scope.colg_name.value > 0)
			{
				var dataString = "clg_id="+$scope.colg_name.value;
				$.ajax({ 
				type: "POST",url: "php/resource/getTimeData.php" ,data: dataString,cache: false,
				success: function(html)
				{ 
					//alert("Received data");
					var dobj=jQuery.parseJSON(html);
					//alert(":::Subject CNT::"+dobj.subject_count);
					$scope.updateFields(dobj);
					
					//alert("::"+dobj.subject_record);
					//alert("::"+dobj.subject_record1);
				} 
				});
			}
	}
	
	$scope.updateFields=function(dobj){
			var no_period = dobj.no_periods;
			//console.log('no of periods',no_period);
			document.getElementById("periods").value  = no_period;
			if(no_period==0)
				alert("No of classes is not defined in Working Day module...");
			else
			{
				$scope.periods = no_period;
				$("#detailsstockid").html(dobj.data);
				document.getElementById("addtimedata").style.display  = "block";
				//document.getElementById("periods").value  = no_period;
				//alert("::"+no_period);
				
			}
			
		}
		
		
	$scope.addTimeData=function(){	
		var no_periods  = document.getElementById('periods').value;
		var dataString = "clg_id="+$scope.colg_name.value+"&periods="+no_periods;
		for(var i=0; i < no_periods; i++)
		{
			var id = i +1;
			var value  = document.getElementById('field'+id).value;
			if(value.length > 0)
				dataString += "&field"+id+"="+value;
			else
			{
				alert("Period "+id+" value is empty...." );
				return;
			}	
		}
		//alert("dataString:"+dataString);
		$.ajax({ 
		type: "POST",url: "php/resource/UpdateTimeData.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			//alert("RECEIVED");
			var dobj=jQuery.parseJSON(html);
			alert(dobj.msg);
		} 
		});
		
		
		
	}
		
			
	
	$scope.removeBorder=function(id){
		//console.log('sc',mod);
		if(id.value!= ''){
			venueField.clearBorderColor(id);
		}
		
	}

	$scope.clearField=function(id){
		venueField.clearBorderColor(id);
	}
	$scope.cancelSessionData=function(){
		$state.go('dashboard.res.day',{}, { reload: true });
	}
});
resourseclass.factory('venueField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});


function checkTimeData(id){
	//alert(""+id);
	var value  = document.getElementById('field'+id).value;
	if(value.length > 0)
	{
		document.getElementById("field"+id).style.backgroundColor="#CCF2CC";
	}
	else
	{
		document.getElementById("field"+id).style.backgroundColor="#FFE0D9";
	}

}


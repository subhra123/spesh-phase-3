var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('resourcePrincpalsectionController',function($scope,$http,$state,$window,sectionField){
	$scope.buttonName="Add";
	var id='';
	$http({
		method:'GET',
		url:"php/Role/readCollegeSectionData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.sectionData=response.data;
	},function errorCallback(response) {
	});
	$scope.addSectionData=function(){
		if($scope.buttonName=="Add"){
			if($scope.section==null || $scope.section==''){
				alert('Section field could not blank...');
				sectionField.borderColor('resourcesection');
			}else{
				var userdata={'section':$scope.section};
				$http({
					method:'POST',
					url:"php/Role/addCollegeSectionData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//console.log('response',response,$scope.sectionData,$scope.sectionData.length);
					alert(response.data['msg']);
					$scope.section=null;
					$scope.sectionData.unshift(response.data);
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.section==response.data.section_name){
						sectionField.borderColor('resourcesection');
					}else{
					$state.go('principal.resourse.section',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.section==null || $scope.section==''){
				alert('Section field could not blank...');
				sectionField.borderColor('resourcesection');
			}else{
				var updatedata={'section':$scope.section,'id':id};
				$http({
					method:'POST',
					url:"php/Role/UpdateCollegeSectionData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.resourse.section',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.section==response.data.section_name){
						sectionField.borderColor('resourcesection');
					}else{
					$state.go('principal.resourse.section',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editSectionData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/Role/editCollegeSectionData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.section=response.data[0].section_name;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteSectionData=function(sid){
		id=sid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Section');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/Role/deleteCollegeSectionData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data);
			$state.go('principal.resourse.section',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data);
			$state.go('principal.resourse.section',{}, { reload: true });
		});
		}
	}
	$scope.clearField=function(id){
		sectionField.clearBorderColor(id);
	}
	$scope.cancelSectionData=function(){
		$state.go('principal.resourse.section',{}, { reload: true });
	}
});
resourseclass.factory('sectionField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
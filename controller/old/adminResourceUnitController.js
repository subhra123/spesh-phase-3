var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('adminResourceUnitController',function($scope,$http,$state,$window,unitField){
	$scope.buttonName="Add";
	var id='';
	$scope.listOfCollege=[{
		name:'Select college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'GET',
		url:"php/resource/readUnitData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.unitData=response.data;
	},function errorCallback(response) {
	});
	$scope.addUnitData=function(){
		if($scope.buttonName=="Add"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select college');
				unitField.borderColor('colg_name');
			}else if($scope.unitname==null || $scope.unitname==''){
				alert('Unit field could not be blank...');
				unitField.borderColor('resourceunit');
			}else{
				var userdata={'colg_name':$scope.colg_name.value,'unit':$scope.unitname};
				$http({
					method:'POST',
					url:"php/resource/addUnitData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					//$scope.colg_name.value='';
					$scope.unitname=null;
					//$scope.unitData.unshift(response.data);
					//$state.go('dashboard.res.unit',{}, { reload: true });
					if($scope.unitData=='null'){
						$scope.unitData=[];
						$scope.unitData.push(response.data);
					}else{
						$scope.unitData.unshift(response.data);
					}
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.unitname==response.data.unit_name){
						unitField.borderColor('resourceunit');
					}else{
					//$state.go('dashboard.res.unit',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select college');
				unitField.borderColor('colg_name');
			}else if($scope.unitname==null || $scope.unitname==''){
				alert('Unit field could not be blank...');
				unitField.borderColor('resourceunit');
			}else{
				var updatedata={'colg_name':$scope.colg_name.value,'unit':$scope.unitname,'id':id};
				$http({
					method:'POST',
					url:"php/resource/UpdateUnitData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.res.unit',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.unitname==response.data.unit_name){
						unitField.borderColor('resourceunit');
					}else{
					$state.go('dashboard.res.unit',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editUnitData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/resource/editUnitData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colg_name.value=response.data[0].colg_id;
			$scope.unitname=response.data[0].unit_name;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.colgread=true;
			$scope.colgdis=true;
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteUnitData=function(sid){
		id=sid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Unit');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/resource/deleteUnitData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('dashboard.res.unit',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			//$state.go('dashboard.res.unit',{}, { reload: true });
		});
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			unitField.clearBorderColor(id);
		}
	}

	$scope.clearField=function(id){
		unitField.clearBorderColor(id);
	}
	$scope.cancelUnitData=function(){
		$state.go('dashboard.res.unit',{}, { reload: true });
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
});
resourseclass.factory('unitField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
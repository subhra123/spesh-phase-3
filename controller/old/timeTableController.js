var dashboard=angular.module('Channabasavashwara');
dashboard.controller('timeTableController',function($scope,$http,$state,$window,$rootScope){
	//$scope.session="2015/16";
	//$scope.type;
	$scope.listOfCourseName=[{
		name: 'Select Couse Name',
		value: ''
	}]
	
	$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Session',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];	
	
	
	$scope.listPublish=[];
	$scope.listPublish=[{
		name: 'NO',
		value: '0'
	},{
		name: 'YES',
		value: '1'
	}]
	
	$scope.publish_name=$scope.listPublish[0];	
	
	
	$scope.listSection=[];
	$scope.listSection=[{
		name: 'Select Section',
		value: ''
	}]
	$scope.section_name=$scope.listSection[0];
	
	
	$scope.noSemesters=[];
	$scope.noSemesters = [{
    name :'Select Semester',
    value: ''
    }];	
	
	$scope.semester=$scope.noSemesters[0];
	
	$http({
		method: 'GET',
		url: "php/timetable/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	

/*
	$http({
		method: 'GET',
		url: "php/timetable/readVenue.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.venue_name);									
			var Venue={'name':obj.venue_name , 'value':obj.venue_id};
			$scope.listVenues.push(Venue);
		});
	},function errorCallback(response) {
		
	});
	*/
	
	$http({
		method: 'GET',
		url: "php/timetable/readSection.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.section_name);									
			var Section={'name':obj.section_name , 'value':obj.section_id};
			$scope.listSection.push(Section);
		});
	},function errorCallback(response) {
		
	});
	
	
	
	$http({
		method: 'GET',
		url: "php/timetable/getCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var user={'name':obj.course_name,'value':obj.course_id};
			$scope.listOfCourseName.push(user);
		});
	},function errorCallback(response) {
	});
	
	$scope.course_name  = $scope.listOfCourseName[0];
	
	$scope.selectedCourse=function(){
		
		$scope.noSemesters=null;
		$scope.noSemesters=[];
		$scope.noSemesters = [{
    	name :'Select Semester',
    	value: ''
    	}];	
		
		var id = $scope.course_name.value;
		//alert("id::"+id);
		var userdata={'course_id':id};
		$http({
			method: 'POST',
			url: "php/subject/readSemester.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			var no_semester = obj.semester;
			//alert(""+no_semester);
			var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
			for(var i = 0; i < no_semester; i++)
			{
				var val = i+1;
				var roman_val = key[i];
				//alert("::"+val+":::"+roman_val);
				var seme = {'name':roman_val , 'value':val};
				$scope.noSemesters.push(seme);
			}
			//var semester={'name':obj.course_name , 'value':obj.course_id};
			//$scope.noSemesters.push(semester);
			
			});
			
			
			
		},function errorCallback(response) {
		});	
		
	}
	
	
	 $scope.noOfDays = [];
     $scope.listOfTimeData=[];
       /* $scope.days = {
          '0': "Monday",
          '1': 'Tuesday',
          '2': 'Wednesday',
          '3': 'Thursday',
          '4': 'Friday'
        }*/
        
        /*$scope.hours = [
          '9AM :: 10AM',
          '10AM :: 11AM',
          '11:15AM :: 12:15PM',
          '12:15PM :: 01:15PM',
          '02PM :: 03PM',
          '03PM :: 04PM',
          '04PM :: 05PM'
        ]*/
        $http({
			method: 'GET',
			url: "php/timetable/gethour.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('hour',response.data);
			$scope.hours=response.data;
		},function errorCallback(response) {
			
		})
		$http({
			method: 'GET',
			url: "php/timetable/getdays.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			 $scope.days=response.data;
			 //console.log('days',$scope.days);
			for (var i = 0; i < 5; i++) {
               $scope.noOfDays.push($scope.days[i]);
            }
		},function errorCallback(response) {
		});
		$scope.listOfSubjectName=[{
		name: 'Select Subject',
		value: ''
	    }]
		$scope.sub_name  = $scope.listOfSubjectName[0];
		
		$scope.listOfFacultyName=[{
		name: 'Select Faculty',
		value: ''
	    }]
		$scope.fac_name=$scope.listOfFacultyName[0];
		
		
		$scope.listOfVenueName=[{
		name: 'Select Venue',
		value: ''
	    }]
		$scope.venue_name  = $scope.listOfVenueName[0];
		
		
		
		
		
		////////////////////////////////////////////////////////////////check table data
		
		$scope.checkTableData=function(){
		
		if( $scope.course_name.value != "" && $scope.semester.value != "" && $scope.session_name.value != "" && $scope.section_name.value != "" )
		{
			
			//var userdata={'course_id':$scope.course_name.value,'semester_id':$scope.semester.value,'session_id':$scope.session_name.value,'section_id':$scope.section_name.value};
			//alert("Check data::");
			
			var dataString = "course_id="+$scope.course_name.value+"&semester_id="+$scope.semester.value+"&session_id="+$scope.session_name.value+"&section_id="+$scope.section_name.value;
			//alert("Data:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/timetable/getTimetableData.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
				//alert("Received data");
				var dobj=jQuery.parseJSON(html);
				//alert(":::Subject CNT::"+dobj.subject_count);
				$scope.updateFields(dobj);
				
				//alert("::"+dobj.subject_record);
				//alert("::"+dobj.subject_record1);
			} 
			});
			
			/*
			$http({
			method: 'POST',
			url:"php/timetable/getTimetableData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
			//console.log('subject',response);
			alert("Received data");
			// $scope.tableData=response.data;
			var dobj=jQuery.parseJSON(response);
			alert(":::"+$scope.tableData);
			
			
			},function errorCallback(response) {
			
			});
			
			*/
		
		}
		
		/*
		var userdata={'course_name':$scope.course_name.value,'semester':$scope.semester.value}
		console.log('user',userdata);
		
		
		
		$http({
			method: 'POST',
			url:"php/timetable/getSubjectData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('subject',response);
			angular.forEach(response.data,function(obj1){
			var user={'name':obj1.subject_name,'value':obj1.subject_id};
			$scope.listOfSubjectName.push(user);
		});
			
		},function errorCallback(response) {
			
		});
		
		
		$http({
			method:'GET',
			url:"php/timetable/getFacultyData.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj2){
			var user={'name':obj2.user_name,'value':obj2.user_id};
			$scope.listOfFacultyName.push(user);
		});
		},function errorCallback(response) {
		});
		*/
		
		}
		
		
		
		
		$scope.addTimeTableData=function(){
			console.log('add time',$scope.listOfTimeData);
			for(var i=0;i<= $scope.listOfTimeData.length;i++){
				$http({
					method:'POST',
					url: "php/timetable/addTimeTable.php",
					data:$scope.listOfTimeData[i],
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//alert(response.data['msg']);
				},function errorCallback(response) {
					//alert(response.data['msg']);
				});
			}
		}
		$scope.checkData=function(subname,facname,hour,day){
			//console.log('data',subname,facname,hour,day);
			if(subname.value==''){
				alert("select subject from list");
				$scope.fac_name=$scope.listOfFacultyName[0];
			}else{
				var subdata={'subtype':subname.value};
				$http({
					method:'POST',
					url: "php/timetable/getSubjectType.php",
					data:subdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//console.log('TYPE',response);
					$rootScope.type=response.data[0].subject_type;
					var time={'sub_name':subname.value,'fac_name':facname.value,'hour':hour.id,'day':day.id,'sub_type':$rootScope.type};
			       $scope.listOfTimeData.push(time);
				},function errorCallback(response) {
				});
			}
			//console.log('data',$scope.listOfTimeData);
		}
		
		
		$scope.updateFields=function(dobj){
			$("#detailsstockid").html(dobj.data);
			$("#data_hide").html(dobj.data_hide);
			//alert("::"+dobj.status)
			if(dobj.status == 1)
				document.getElementById("publish_name").value = 1;
			else
				document.getElementById("publish_name").value = 0;
			if(dobj.hod_status == 1)	
				document.getElementById("hod_approve").value = "YES";
			else
				document.getElementById("hod_approve").value = "NO";
			
			if(dobj.principal_status == 1)	
				document.getElementById("principal_approve").value = "YES";
			else
				document.getElementById("principal_approve").value = "NO";
			
			//alert()
			document.getElementById("hod_remarks").value  = dobj.hod_comment;
			document.getElementById("principal_remarks").value  = dobj.principal_comment;
			
		}
		
		

		
		
});



function updatePublish(){
		
		var session_id = document.getElementById("session_name").value;
		var section_id = document.getElementById("section_name").value;
		var course_id = document.getElementById("course_name").value;
		var semester_id = document.getElementById("semester").value;
		if(session_id > 0 && section_id > 0 && course_id > 0 && semester_id > 0)
		{
			if(confirm("Would you like to publish Time Table ?"))
			{
				var value = document.getElementById("publish_name").value;
				var dataString = "session_id="+session_id+"&section_id="+section_id+"&course_id="+course_id+"&semester_id="+semester_id+"&value="+value;
				//alert("Data:"+dataString);
				$.ajax({ 
				type: "POST",url: "php/timetable/updatePublish.php" ,data: dataString,cache: false,
					success: function(html)
					{ 
						
					} 
					});
			}
		}
		
	}
 
function checkFacultyRecord(session_id,day,time,tableid){
	
	var user_id = document.getElementById("faculty_"+day+time).value;
	//alert("New Value:"+user_id);
	
	//document.getElementById("faculty_"+day+time).value = "";
	
	if(user_id == '')
		user_id = 0 ;
		
	var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+time+"&table_id="+tableid+"&faculty_id="+user_id;
	//alert("Data:"+dataString);
	$.ajax({ 
	type: "POST",url: "php/timetable/addFacultyRecord.php" ,data: dataString,cache: false,
	success: function(html)
			{ 
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.results)
				if(dobj.results == 1)
				{
					if(user_id > 0)
						document.getElementById("faculty_"+day+time).style.backgroundColor="#CCF2CC";
					else
						document.getElementById("faculty_"+day+time).style.backgroundColor="#FFE0D9";
				}
				else
				{
					alert(dobj.error);
					document.getElementById("faculty_"+day+time).value = "";
					document.getElementById("faculty_"+day+time).style.backgroundColor="#FFE0D9";
					
				}
			} 
			});
	
	
	}
	
function checkSubjectRecord(day,time,tableid){
	//alert("aaaaaaaaaaaaaaaaaaaaaaaaaa");
	var subject_type = 0;
	var subject_id = document.getElementById("subject_"+day+time).value;
	if(subject_id == '')
		subject_id = 0 ;
	//alert("checkSubjectRecord::"+day+":"+time+":"+tableid+":"+subject_id);
	if(subject_id > 0)
		subject_type = document.getElementById("subjecttype_"+subject_id).value;
	//alert("::"+subject_type);
	//if(time == 1 && subject_type == "Lab" )
	if(false)
	{
		//alert("AAAAA");
		
			time = time;
			var dataString = "day_id="+day+"&time_id="+time+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+subject_type;
			alert("Data:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/timetable/addSubjectRecord.php" ,data: dataString,cache: false,
			success: function(html)
					{ 
						//alert("subject_id:"+subject_id);
						if(subject_id > 0)
							document.getElementById("subject_"+day+time).style.backgroundColor="#CCF2CC";
						else
							document.getElementById("subject_"+day+time).style.backgroundColor="#FFE0D9";
					} 
					});
			
			
			time = time + 1;
			var dataString = "day_id="+day+"&time_id="+time+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+subject_type;
			alert("Data:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/timetable/addSubjectRecord.php" ,data: dataString,cache: false,
			success: function(html)
					{ 
						//alert("subject_id:"+subject_id);
						if(subject_id > 0)
							document.getElementById("subject_"+day+time).style.backgroundColor="#CCF2CC";
						else
							document.getElementById("subject_"+day+time).style.backgroundColor="#FFE0D9";
					} 
					});
			
			
			time = time + 1;
			var dataString = "day_id="+day+"&time_id="+time+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+subject_type;
			alert("Data:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/timetable/addSubjectRecord.php" ,data: dataString,cache: false,
			success: function(html)
					{ 
						//alert("subject_id:"+subject_id);
						if(subject_id > 0)
							document.getElementById("subject_"+day+time).style.backgroundColor="#CCF2CC";
						else
							document.getElementById("subject_"+day+time).style.backgroundColor="#FFE0D9";
					} 
					});
		
	}
	else
	{
		var dataString = "day_id="+day+"&time_id="+time+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+subject_type;
		//alert("Data:"+dataString);
		$.ajax({ 
		type: "POST",url: "php/timetable/addSubjectRecord.php" ,data: dataString,cache: false,
		success: function(html)
				{ 
					//alert("subject_id:"+subject_id);
					if(subject_id > 0)
						document.getElementById("subject_"+day+time).style.backgroundColor="#CCF2CC";
					else
						document.getElementById("subject_"+day+time).style.backgroundColor="#FFE0D9";
				} 
				});
		
	}
	
	
	
	}
	
	
	function checkVenueRecord(session_id,day,time,tableid){
	
	var venue_id = document.getElementById("venue_"+day+time).value;
	if(venue_id == '')
		venue_id = 0 ;
	//alert("checkSubjectRecord::"+day+":"+time+":"+tableid+":"+subject_id);
	//alert("Inside checkSubjectRecord...::"+day+"::"+time+"::"+user_id+":::"+subject_type+":::"+tableid );
	//console.log('data',$scope.listOfTimeData);
	
	var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+time+"&table_id="+tableid+"&venue_id="+venue_id;
	//alert("Data:"+dataString);
	$.ajax({ 
	type: "POST",url: "php/timetable/addVenueRecord.php" ,data: dataString,cache: false,
	success: function(html)
			{ 
				var dobj=jQuery.parseJSON(html);
				if(dobj.results == 1)
				{
					if(venue_id > 0)
						document.getElementById("venue_"+day+time).style.backgroundColor="#CCF2CC";
					else
						document.getElementById("venue_"+day+time).style.backgroundColor="#FFE0D9";
				}
				else
				{
					alert(dobj.error);
					document.getElementById("venue_"+day+time).value = "";
					document.getElementById("venue_"+day+time).style.backgroundColor="#FFE0D9";
					
				}		
					
					
					
			} 
			});
	
	}
	
	
	
		
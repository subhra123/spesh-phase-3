var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('resourcePrincpalSessionController',function($scope,$http,$state,$window,sessionField){
	$scope.buttonName="Add";
	var id='';
	$http({
		method:'GET',
		url:"php/Role/readCollegeSessionData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.sessionData=response.data;
	},function errorCallback(response) {
	});
	$scope.addSessionData=function(){
		if($scope.buttonName=="Add"){
			if($scope.session==null || $scope.session==''){
				alert('Please select your session...');
				sessionField.borderColor('resourcesession');
			}else{
				var userdata={'session':$scope.session};
				$http({
					method:'POST',
					url:"php/Role/addCollegeSessionData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$scope.session=null;
					$scope.sessionData.unshift(response.data);
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.session==response.data.session){
						sessionField.borderColor('resourcesession');
					}else{
					$state.go('principal.resourse.session',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.session==null || $scope.session==''){
				alert('Please select your session...');
				sessionField.borderColor('resourcesession');
			}else{
				var updatedata={'session':$scope.session,'id':id};
				$http({
					method:'POST',
					url:"php/Role/UpdateCollegeSessionData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.resourse.session',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.session==response.data.session){
						sessionField.borderColor('resourcesession');
					}else{
					$state.go('principal.resourse.session',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editSessionData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/Role/editCollegeSessionData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.session=response.data[0].session;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteSessionData=function(sid){
		id=sid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Section');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/Role/deleteCollegeSessionData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data);
			$state.go('principal.resourse.session',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data);
			$state.go('principal.resourse.session',{}, { reload: true });
		});
		}
	}

	$scope.clearField=function(id){
		sessionField.clearBorderColor(id);
	}
	$scope.cancelSessionData=function(){
		$state.go('principal.resourse.session',{}, { reload: true });
	}
});
resourseclass.factory('sessionField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
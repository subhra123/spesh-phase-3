var dashboard=angular.module('Channabasavashwara');
dashboard.controller('adminUserManagementUserController',function($scope,$http,$state,$window,userField){								  
	 $scope.buttonName="Add";
	 var id='';
	 var log_name='';
	 $scope.inputType="password";
	 $scope.hideShowPassword=function(){
		 if($scope.inputType=='password'){
			 $scope.inputType="text";
		 }else{
			 $scope.inputType="password";
		 }
	 }
	 $scope.hidePassAfterLeave=function(){
		 $scope.inputType="password";
	 }
	 /*$scope.isCapsLockOn=false;
	 $scope.checkCapsLock=function(){
		 console.log('caps');
		 if (CapsLock.isOn()){
			 $scope.isCapsLockOn=true;
			 $scope.pass="Capslock is on"
		 }else{
			 $scope.isCapsLockOn=false;
		 }
	 }*/
	 
	 /*$scope.showPassword = '';
     $scope.password = '';
     $scope.checkboxModel = false;
     $scope.showHidePassword = function(checkboxModel) {
          $scope.showPassword = checkboxModel ? $scope.password : '' ;
     }*/
	 $scope.listOfCollege=[{
		name:'Select  College',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	 $http({
		method: 'GET',
		url: "php/user/readUserData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.objUserData=response.data;
	},function errorCallback(response) {
		
	});
	 $scope.addUserData=function(billdata){
		 //console.log('button name',$('#addProfileData')[0].defaultValue);
		 if(billdata.$valid){
		 if($scope.buttonName=="Add"){
			 if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				 alert('please select  college..');
				 userField.borderColor('colg_name');
			 }else if($scope.first_name==null || $scope.first_name==''){
			 alert("first name field can not be blank");
			 userField.borderColor('fstname');
		 }else if($scope.last_name==null || $scope.last_name==''){
			 alert("last name field can not be blank");
			 userField.borderColor('lstname');
		 }else if($scope.user_email==null || $scope.user_email==''){
			 alert("Email field can not be blank");
			 userField.borderColor('uemail');
		 }else if($scope.mob_no==null || $scope.mob_no==''){
			 alert("Mobile no  field can not be blank");
			 userField.borderColor('mobno');
		 }else if($scope.login_name==null || $scope.login_name==''){
			 alert("User Name  field can not be blank");
			 userField.borderColor('uname');
		 }else if($scope.password==null || $scope.password=='' ){
			 alert("password field can not be blank");
			 userField.borderColor('passno');
		 }else if($scope.user_status==null || $scope.user_status==''){
			  alert("Select user status");
			  userField.borderColor('status');
		 }else{
			 var userdata={'colg_name':$scope.colg_name.value,'first_name':$scope.first_name,'last_name':$scope.last_name,'user_email':$scope.user_email,'mob_no':$scope.mob_no,'login_name':$scope.login_name,'password':$scope.password,'user_status':$scope.user_status};
			 //console.log('user',userdata);
			 $http({
				 method: 'POST',
				 url: "php/user/addUserData.php",
				 data: userdata,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
			 }).then(function successCallback(response){
				// console.log(response);
				 alert(response.data['msg']);
				// $scope.colg_name.value='';
			     $scope.first_name=null;
				 $scope.last_name=null;
			     $scope.user_email=null;
			     $scope.mob_no=null;
		         $scope.login_name=null;
				 $scope.password=null;
				 $scope.user_status=null;
			     //$scope.objUserData.unshift(response.data);
				// $state.go('dashboard.user.usermanagement',{}, { reload: true });
				if($scope.objUserData=='null'){
						$scope.objUserData=[];
						$scope.objUserData.push(response.data);
					}else{
						$scope.objUserData.unshift(response.data);
					}
			 },function errorCallback(response) {
				 alert(response.data['msg']);
				 if($scope.user_email==response.data.email){
					 userField.borderColor('uemail');
				 }else if($scope.mob_no==response.data.mob_no){
					userField.borderColor('mobno'); 
				 }else if($scope.login_name==response.data.login_name){
					 userField.borderColor('uname');
				 }else{
				  $state.go('dashboard.user.usermanagement',{}, { reload: true });
				 }
			 });
		 }
		 }
		 if($scope.buttonName=="Update"){
			 if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				 alert('please select college..');
				 userField.borderColor('colg_name');
			 }else if($scope.first_name==null || $scope.first_name==''){
			 alert("first name field can not be blank");
			 userField.borderColor('fstname');
		 }else if($scope.last_name==null || $scope.last_name==''){
			 alert("last name field can not be blank");
			 userField.borderColor('lstname');
		 }else if($scope.user_email==null || $scope.user_email==''){
			 alert("Email field can not be blank");
			 userField.borderColor('uemail');
		 }else if($scope.mob_no==null || $scope.mob_no==''){
			 alert("Mobile no  field can not be blank");
			 userField.borderColor('mobno');
		 }else if($scope.login_name==null || $scope.login_name==''){
			 alert("User Name  field can not be blank");
			 userField.borderColor('uname');
		 }else if($scope.showpass==false && ($scope.password==null || $scope.password=='')){
			 alert("password field can not be blank");
			 userField.borderColor('passno');
		 }else if($scope.user_status==null || $scope.user_status==''){
			  alert("Select user status");
			  userField.borderColor('status');
		 }else{
			 var updatedata={'colg_name':$scope.colg_name.value,'first_name':$scope.first_name,'last_name':$scope.last_name,'user_email':$scope.user_email,'mob_no':$scope.mob_no,'login_name':$scope.login_name,'password':$scope.password,'user_status':$scope.user_status,'user_id':id,'log_name':log_name};
			 console.log('user',updatedata);
			 $http({
				 method: 'POST',
				 url: "php/user/updateUserData.php",
				 data: updatedata,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
			 }).then(function successCallback(response){
				 //console.log('updated',response);
				 alert(response.data['msg']);
			     $state.go('dashboard.user.usermanagement',{}, { reload: true });
			 },function errorCallback(response) {
				 console.log('err response',response);
				alert(response.data['msg']);
				if($scope.user_email==response.data.email){
					 userField.borderColor('uemail');
				 }else if($scope.mob_no==response.data.mob_no){
					userField.borderColor('mobno'); 
				 }else if($scope.login_name==response.data.login_name){
					 userField.borderColor('uname');
				 }else{
				  $state.go('dashboard.user.usermanagement',{}, { reload: true });
				 }
			 });
		 }
		 }
		 }else{
			 if(billdata.email.$invalid){
			  alert('Please enter valid email id');
			 }
			 if(billdata.uname.$invalid){
				 alert('Please enter valid user name');
			 }
			 if(billdata.mobno.$invalid ){
				 alert('Please enter valid Mobile no');
			 }
			 if(billdata.pass.$invalid){
				 alert('Please enter valid password');
			 }
		 }
	 }
	 $scope.editUserData=function(uid){
		 id=uid;
		 var userdata={'userid':id};
		 $http({
			method: 'POST',
			url: "php/user/editUserData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			$scope.colg_name.value=response.data[0].colg_id;
           // $scope.selectEditDept(response.data[0].dept_id)
			$scope.first_name=response.data[0].first_name;
			$scope.last_name=response.data[0].last_name;
			$scope.mob_no=response.data[0].mob_no;
			$scope.user_email=response.data[0].email;
			$scope.login_name=response.data[0].login_name;
			log_name=response.data[0].login_name;
			$scope.user_status=response.data[0].user_status;
			$scope.buttonName="Update";
			$scope.readcolg=true;
			$scope.colgdis=true;
			$scope.showpass=true;
			$scope.hidebtn=true;
			$scope.cancelbuttonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	 }
	 $scope.deleteUserData=function(uid){
		 var deleteUser=$window.confirm('Are you  sure to delete this User?');
		 if(deleteUser){
			 id=uid;
			 var userdata={'userid':id};
			 $http({
			method: 'POST',
			url: "php/user/deleteUserData.php",
			data:userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('delete',response);
			alert(response.data['msg']);
			$state.go('dashboard.user.usermanagement',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			$state.go('dashboard.user.usermanagement',{}, { reload: true });
		});
		 }
	 }
	 $scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			userField.clearBorderColor(id);
		}
		$scope.listOfDepartment=null;
		$scope.listOfDepartment=[{
		name:'Select your Department',
		value:''
	    }]
	    $scope.dept_name=$scope.listOfDepartment[0];
		var colgid={'colg_id':$scope.colg_name.value};
		$http({
			method:'POST',
			url:"php/user/GetCollegeDataUser.php",
			data:colgid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.dept_name,'value':obj.dept_id};
				$scope.listOfDepartment.push(data);
			});
		},function errorCallback(response) {
		})
	}
	$scope.clearField=function(id){
		userField.clearBorderColor(id);
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.getDept=function(){
		$scope.listOfSearchdept=null;
		$scope.listOfSearchdept=[];
		var colgid={'colg_id':$scope.colgSearch.value}
	$http({
		method:'POST',
		url:"php/department/readDeptValue.php",
		data:colgid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.dept_name,'value':obj.dept_id};
			$scope.listOfSearchdept.push(data);
		});
	},function errorCallback(response) {
	});
	}
	$scope.cancelUserData=function(){
		$state.go('dashboard.user.usermanagement',{}, { reload: true });
	}
	$scope.listOfDepartment=[{
		name:'Select your Department',
		value:''
	}]
	$scope.dept_name=$scope.listOfDepartment[0];
	$scope.clearBorder=function(id,mod){
		if(mod.value!=''){
			userField.clearBorderColor(id);
		}
	}
	$scope.selectEditDept=function(value){
		$scope.listOfDepartment=null;
		$scope.listOfDepartment=[{
		name:'Select your Department',
		value:''
	    }]
	    $scope.dept_name=$scope.listOfDepartment[0];
		var colgid={'colg_id':$scope.colg_name.value};
		$http({
			method:'POST',
			url:"php/user/GetCollegeDataUser.php",
			data:colgid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.dept_name,'value':obj.dept_id};
				$scope.listOfDepartment.push(data);
				$scope.dept_name.value=value;
			});
		},function errorCallback(response) {
		})
	}
	$scope.resetPasswod=function(){
		$scope.hidebtn=false;
		$scope.showpass=false;
		$scope.rejectpass=true;
	}
	$scope.closePass=function(){
		$scope.showpass=true;
		$scope.rejectpass=false;
		$scope.hidebtn=true;
	}
})
dashboard.factory('userField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
/*dashboard.directive('confirmOnExit', function() {
	return {
  require:'form',
        link: function($scope, elem, attrs,formController) {
            window.onbeforeunload = function(){
                if (formController.$dirty) {
                    return "The form is not saved, do you want to stay on the page?";
                }
            }
            $scope.$on('$locationChangeStart', function (event, next, current) {
            if (formController.$dirty) {
                if (!confirm("The form is dirty, do you want to stay on the page?")) {
                    event.preventDefault();
                }
            }
        });
          }
    };
});*/
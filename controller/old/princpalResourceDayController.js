var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('princpalResourceDayController',function($scope,$http,$state,$window,venueField){
	$scope.buttonName="Add";
	var id='';
	$scope.listOfCollege=[{
		name:'Select college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'GET',
		url:"php/Role/readCollegeDayData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.venueData=response.data;
	},function errorCallback(response) {
	});
	$scope.addSessionData=function(){
		if($scope.buttonName=="Add"){
			if($scope.day==null || $scope.day==''){
				alert('Please select Lecture Day per Week...');
				venueField.borderColor('day');
			}else if($scope.firstday==null || $scope.firstday==''){
				alert('Please select First Day...');
				venueField.borderColor('firstday');
			}else if($scope.period==null || $scope.period==''){
				alert('Please select classes per Day...');
				venueField.borderColor('period');
			}
			else{
				var userdata={'days':$scope.day , 'firstday':$scope.firstday , 'periods':$scope.period };
				$http({
					method:'POST',
					url:"php/Role/addCollegeDayData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.resourse.day',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.day==null || $scope.day==''){
				alert('Please select Lecture Day per Week...');
				venueField.borderColor('day');
			}else if($scope.firstday==null || $scope.firstday==''){
				alert('Please select First Day...');
				venueField.borderColor('firstday');
			}else if($scope.period==null || $scope.period==''){
				alert('Please select classes per Day...');
				venueField.borderColor('period');
			}else{
				var updatedata={'days':$scope.day , 'firstday':$scope.firstday , 'periods':$scope.period,'id':id};
				$http({
					method:'POST',
					url:"php/Role/UpdateCollegeDayData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.resourse.day',{}, { reload: true });
				},function errorCallback(response) {
					console.log('msg',response);
					alert(response.data['msg']);
				});
			}
		}
	}
	$scope.editData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/Role/editCollegeDayData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.day=response.data[0].days;
			$scope.firstday=response.data[0].first_day;
			$scope.period=response.data[0].periods;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	
	$scope.removeBorder=function(id){
		//console.log('sc',mod);
		if(id.value!= ''){
			venueField.clearBorderColor(id);
		}
		
	}

	$scope.clearField=function(id){
		venueField.clearBorderColor(id);
	}
	$scope.cancelSessionData=function(){
		$state.go('principal.resourse.day',{}, { reload: true });
	}
});
resourseclass.factory('venueField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
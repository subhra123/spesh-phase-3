var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('adminResourceDayController',function($scope,$http,$state,$window,venueField){
	$scope.buttonName="Add";
	var id='';
	$scope.listOfCollege=[{
		name:'Select college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'GET',
		url:"php/resource/readDayData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.venueData=response.data;
	},function errorCallback(response) {
	});
	$scope.addSessionData=function(){
		if($scope.buttonName=="Add"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('Please select college');
				venueField.borderColor('colg_name');
			}else if($scope.day==null || $scope.day==''){
				alert('Please select Lecture Day per Week...');
				venueField.borderColor('day');
			}else if($scope.firstday==null || $scope.firstday==''){
				alert('Please select First Day...');
				venueField.borderColor('firstday');
			}else if($scope.period==null || $scope.period==''){
				alert('Please select classes per Day...');
				venueField.borderColor('period');
			}
			else{
				var userdata={'clg_id':$scope.colg_name.value,'days':$scope.day , 'firstday':$scope.firstday , 'periods':$scope.period };
				$http({
					method:'POST',
					url:"php/resource/addDayData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$scope.day='';
					$scope.firstday='';
					$scope.period='';
					//$state.go('dashboard.res.day',{}, { reload: true });
					if($scope.venueData=='null'){
						$scope.venueData=[];
						$scope.venueData.push(response.data);
					}else{
						$scope.venueData.unshift(response.data);
					}
				},function errorCallback(response) {
					alert(response.data['msg']);
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('Please select college');
				venueField.borderColor('colg_name');
			}else if($scope.day==null || $scope.day==''){
				alert('Please select Lecture Day per Week...');
				venueField.borderColor('day');
			}else if($scope.firstday==null || $scope.firstday==''){
				alert('Please select First Day...');
				venueField.borderColor('firstday');
			}else if($scope.period==null || $scope.period==''){
				alert('Please select classes per Day...');
				venueField.borderColor('period');
			}else{
				var updatedata={'clg_id':$scope.colg_name.value,'days':$scope.day , 'firstday':$scope.firstday , 'periods':$scope.period,'id':id};
				$http({
					method:'POST',
					url:"php/resource/UpdateDayData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.res.day',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
				});
			}
		}
	}
	$scope.editData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/resource/editDayData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colg_name.value=response.data[0].clg_id;
			$scope.day=response.data[0].days;
			$scope.firstday=response.data[0].first_day;
			$scope.period=response.data[0].periods;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.colgread=true;
			$scope.colgdis=true;
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	
	$scope.removeBorder=function(id){
		//console.log('sc',mod);
		if(id.value!= ''){
			venueField.clearBorderColor(id);
		}
		
	}

	$scope.clearField=function(id){
		venueField.clearBorderColor(id);
	}
	$scope.cancelSessionData=function(){
		$state.go('dashboard.res.day',{}, { reload: true });
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
});
resourseclass.factory('venueField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
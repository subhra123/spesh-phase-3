var resource=angular.module('Channabasavashwara');
resource.controller('princpalResourceUserRoleController',function($scope,$http,$window,$state,focusRoleField){
	$scope.buttonName="Add";
	var id='';
	$http({
		method:'GET',
		url:"php/Role/readCollegeRoleData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log(response);
		$scope.roleData=response.data;
	},function errorCallback(response) {
	});
	$scope.addUserRoleData=function(){
		if($scope.buttonName=="Add"){
			 if($scope.user_role==null || $scope.user_role==''){
				alert('User role field could not be blank');
				focusRoleField.borderColor('resourcerole');
			}else if($scope.user_type.value==null || $scope.user_type.value==''){
				alert('select the user type');
				focusRoleField.borderColor('utype');
			}else{
				var userdata={'user_role':$scope.user_role,'type':$scope.user_type.value};
				//console.log('type data',userdata);
				$http({
					method:'POST',
					url:"php/Role/addCollegeRoleData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//console.log('role',response.data);
					alert(response.data['msg']);
					//$state.go('principal.resourse.userrole',{}, { reload: true });
					$scope.user_role=null;
					$scope.user_type='';
					//$scope.roleData.unshift(response.data);
					if($scope.roleData=='null'){
						$scope.roleData=[];
						$scope.roleData.push(response.data);
					}else{
						$scope.roleData.unshift(response.data);
					}
				},function errorCallback(response) {
					console.log('res',response);
					alert(response.data['msg']);
					if($scope.user_role==response.data.role){
						focusRoleField.borderColor('resourcerole');
					}else{
					//$state.go('principal.resourse.userrole',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			 if($scope.user_role==null || $scope.user_role==''){
				alert('User role field could not be blank');
				focusRoleField.borderColor('resourcerole');
			}else if($scope.user_type.value==null || $scope.user_type.value==''){
				alert('select the user type');
				focusRoleField.borderColor('utype');
			}else{
				var updatedata={'user_role':$scope.user_role,'type':$scope.user_type.value,'id':id};
				$http({
					method:'POST',
					url:"php/Role/updateCollegeRoleData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.resourse.userrole',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.user_role==response.data.role){
						focusRoleField.borderColor('resourcerole');
					}else{
					$state.go('principal.resourse.userrole',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			focusRoleField.clearBorderColor(id);
		}
	}
	$scope.clearField=function(id){
		focusRoleField.clearBorderColor(id);
	}
	$scope.editRoleData=function(rid){
		id=rid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/Role/editCollegeRoleData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.user_role=response.data[0].role;
			//$scope.getUserType(response.data[0].type);
			$scope.user_type.value=response.data[0].type;
			$scope.buttonName="Update";
			$scope.CancelbuttonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteRoleData=function(rid){
		id=rid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Role');
		if(conmesg){
			$http({
				method:'POST',
				url:"php/Role/deleteCollegeRoleData.php",
				data:userid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				$state.go('principal.resourse.userrole',{}, { reload: true });
			},function errorCallback(response) {
				alert(response.data['msg']);
				$state.go('principal.resourse.userrole',{}, { reload: true });
			});
		}
	}
	$scope.cancelUserRoleData=function(){
		$state.go('principal.resourse.userrole',{}, { reload: true });
	}
	$scope.listOftype=[{
		name:'Select user type',
		value:''
	}]
	$scope.user_type=$scope.listOftype[0];
	$http({
		method:'GET',
		url:"php/Role/getCollegeType.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.type_name,'value':obj.type_value};
			$scope.listOftype.push(data);
		});
	},function errorCallback(response) {
	})
	
});
resource.factory('focusRoleField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
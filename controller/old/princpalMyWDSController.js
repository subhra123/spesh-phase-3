var plan=angular.module('Channabasavashwara');
plan.controller('princpalMyWDSController',function($scope,$http,$state,$window,focusStreamField){


	$scope.year="";
	
	$scope.user_readonly= false;
	$scope.buttonName="Add";
	$scope.showAddUnit=false;
	
	
	$scope.topic = "";
	$scope.clearButtonName="Cancel";
	$scope.showCancel=false;
	$scope.showAdd=false;
	
	var id='';
	$scope.listStatus=[];
	$scope.listStatus=[{
		name:'Not Completed',
		value:'0'} ,
		{
		name:'Completed',
		value:'1'}
	];
	
	$scope.status_name=$scope.listStatus[0];
	
	
	$scope.listPlanData=[];
	$scope.listPlanData=[{
		name:'Select Plan',
		value:''
	}
	];
	$scope.plan_name=$scope.listPlanData[0];
	
	
	$scope.listTimeData=[];
	$scope.listTimeData=[{
		name:'Select Time Slot',
		value:''
	}
	];
	
	$scope.time_slot=$scope.listTimeData[0];
	
	
	
	$http({
		method:'GET',
		url:"php/userwds/readPrincipalMyPlanData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('res',response.data);
		$scope.viewmyPlanData=response.data;
	},function errorCallback(response) {
	});
	
	
	
	
	$http({
		method:'GET',
		url:"php/userwds/getTimeSlot.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj2){
			var data={'name':obj2.time_slot,'value':obj2.id};
			$scope.listTimeData.push(data);
		});
	},function errorCallback(response) {
	});
	
	
	
	$scope.listOfCourse=[{
		name:'Select Course',
		value:''
		}];
	$scope.course_name=$scope.listOfCourse[0];
	
	$http({
		method: 'GET',
		url:"php/course/getUserCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.course_name,'value':obj.course_id};
			$scope.listOfCourse.push(data);
		});
	},function errorCallback(response) {
		
	});
		
	
	$scope.listOfLession=[{
		name:'Select Lession plan',
		value:''
	}
	];
	
	$scope.new_unit_name="";
	$scope.viewUnitData=[];
	$scope.viewPlanData=[];
	$scope.listUnitData=[];
	$scope.listUnitData=[{
		name:'Select Unit',
		value:''
	}
	];
	//$scope.unit_name=$scope.listUnitData[0];
	
	
	$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Academic Year',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];
	
	
	/*$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Subject',
		value: ''
	}]*/
	$scope.listOfStream=null;
	$scope.listOfStream=[{
		name:'Select Stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
	
	$scope.listSubject=[{
			name: 'Select Subject',
			value: ''
		}]
	$http({
		method: 'GET',
		url:"php/stream/readCollegeStreamData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.stream_name,'value':obj.stream_id};
			$scope.listOfStream.push(data);
		});
	},function errorCallback(response) {
		
	});
	$scope.sub_name = $scope.listSubject[0];
	
	$http({
		method: 'GET',
		url: "php/plan/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	
	
		$http({
		method:'GET',
		url:"php/userplan/getLession.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.lession_name,'value':obj.lession_id};
			$scope.listOfLession.push(data);
		});
		},function errorCallback(response) {
		});



		$scope.lession=$scope.listOfLession[0];
		
	
		
	
	$scope.noSemesters=[{
		name:'Select Semester',
		value:''
	}];
	
	$scope.semester=$scope.noSemesters[0];
	
	$scope.listOfSubject=[{
		name:'Select Subject',
		value:''
	}];
	
	
	$scope.subject_name=$scope.listOfSubject[0];
	
	$scope.listOfSection=[{
		name:'Select section',
		value:''
	}];
	$http({
		method:'GET',
		url:"php/userplan/getSectionData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj2){
			var data={'name':obj2.section_name,'value':obj2.section_id};
			$scope.listOfSection.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.section=$scope.listOfSection[0];
	$scope.noSemesters=[];
	
	
	$scope.selectedCourse=function(id){
			//alert("::"+id);
			focusStreamField.removeBorderColor(id);
				
		     $scope.noSemesters=null;
			 $scope.noSemesters=[];
			 var key=['I','II','III','IV','V','VI','VII','VIII','IX','X'];
			 var userid={'course_id':$scope.course_name.value};
			 $http({
				 method:'POST',
				 url:"php/course/getCollegeSemester.php",
				 data: userid,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			 }).then(function successCallback(response){
				 if($scope.course_name.name==response.data[0].course_name){
					 for(var i=0;i<response.data[0].semester;i++){
						 var val=i+1;
						 var rom_val=key[i];
						 var sem={'name':rom_val,'value':val};
						 $scope.noSemesters.push(sem);
					 }
				 }
			 },function errorCallback(response) {
				 $scope.noSemesters=null;
			 });
	}
	
	
	$scope.selectedSemester=function(id){
		focusStreamField.removeBorderColor(id);
		
		$scope.listOfSubject=null;
		$scope.listOfSubject=[];
		$scope.listOfSubject=[{
			name: 'Select Subject',
			value: ''
		}]
		var strmid=$scope.stream_name.value;
		var course_id = $scope.course_name.value;
		var semester_id = $scope.semester.value;
		//alert("semester_id::"+semester_id);
		var userdata={'stream_id':strmid,'course_id':course_id,'semester_id':semester_id};
		$http({
			method: 'POST',
			url: "php/userplan/getSubjectData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			var Subject={'name':obj.type+" : "+obj.subject_name , 'value':obj.subject_id};
			$scope.listOfSubject.push(Subject);
			
			});
		},function errorCallback(response) {
		});	
		
	}
	
	
	
	$http({
		method:'GET',
		url:"php/userwds/readPrincipalCompletedUnitData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('res',response.data);
		$scope.viewUnitData=response.data;
	},function errorCallback(response) {
	});
	
	
	
	$scope.addPlanData=function(){
		//alert("::"+$scope.buttonName);
		if($scope.buttonName=="Add"){
		var today=new Date();
		
		if($scope.date==null || $scope.date=="" ){
			alert('Please select completed date');
			focusStreamField.borderColor('date');
		}else if(today < new Date($scope.date.split("-").reverse().join(","))){
			alert('Please select any previous date or today');
			focusStreamField.borderColor('date');
		}else if($scope.time_slot.value==""){
			alert('Please select time slot');
			focusStreamField.borderColor('time_slot');
		}else{
			
			var dataString = "unit_id="+temp_unit_id+"&plan_id="+id+"&date="+$scope.date+"&time_id="+$scope.time_slot.value+"&status="+$scope.status_name.value+"&plan_name="+$scope.lession_plan;
			//alert("dataString:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/userwds/addCompletedPlanData.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.result);
				if(dobj.result == 0)
				{
					alert(dobj.error);
				}
				else
				{
					alert("New completed plan added successfully...");
					$state.go('principal.myplanManagement.myWDS',{}, { reload: true });
				}
			} 
			});
			
			
			
			
		}
		
		
		}
		if($scope.buttonName=="Update"){
			//alert("aaaaaaa");
			if($scope.date==null){
			alert('Please select date');
			}else if($scope.plan==null){
			alert('Please add  plan');
			}else if($scope.topic==null){
			alert('Please add topic');
			}else{
			
			var dataString = "unit_plan_id="+temp_unit_id+"&plan_id="+id+"&date="+$scope.date+"&lession_plan="+$scope.plan+"&topic="+$scope.topic;
			//alert("::"+dataString);
			
			$.ajax({ 
			type: "POST",url: "php/userplan/updatePlanData.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.result);
				if(dobj.result == 0)
				{
					alert(dobj.error);
				}
				else
				{
					var updatedata={'unit_id':temp_unit_id};
		
					$scope.viewPlanData=null;
					$scope.viewPlanData=[];
			
					$http({
						method:'POST',
						url:"php/userplan/readPlanData.php",
						data:updatedata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						console.log('res',response.data);
						$scope.viewPlanData=response.data;
						//document.getElementById("unit_list").style.display = "none";
						//document.getElementById("plan_list").style.display = "block";
					},function errorCallback(response) {
						
						
					});
					
					$scope.date = null;
					$scope.plan = null;
					$scope.topic = null;
					$scope.clearPlanData();
					alert("Plan updated successfully...");
				
				}
			} 
			});
			
			
			
			
			
		}
		}
	}
	
	$scope.getFutureDate=function(){
		var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
        dd='0'+dd
        } 
        if(mm<10){
        mm='0'+mm
        } 
        var today = dd+'-'+mm+'-'+yyyy;
		return today;
	}
	
	
	
	$scope.addUnit=function(){
		if($scope.session_name.value==''){
			alert('Please select Academic Year');
		}else if($scope.course_name.value==''){
			alert('Please select the course name');
		}else if($scope.semester.value==''){
			alert('Please select the semester');
		}else if($scope.subject_name.value==''){
			alert('Please select the subject name');
		}else if($scope.section.value==''){
			alert('Please select the section');
		}else{
				//alert("Add Unit");
				document.getElementById("addunit").style.display ="block";
			}
	}
	
	
	$scope.getPlanData=function(id){
			$scope.clearField(id);
		
			$scope.listPlanData=[];
			$scope.listPlanData=[{
				name:'Select Plan',
				value:''
			}
			];
	
			var updatedata={'unit_id':$scope.unit_name.value };
			//alert("aa::"+$scope.unit_name.value);
			$http({
				method:'POST',
				url:"php/userwds/getPlanName.php",
				data:updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data, function(obj){
				//alert("::"+obj.plan);									
				var Session={'name':obj.plan , 'value':obj.plan_id};
					$scope.listPlanData.push(Session);
				});
				
			},function errorCallback(response) {
				
				
			});
	
	
	}
	
	
	
	$scope.getPlanDetails=function(id){
			$scope.clearField(id);
			//alert("aaaa");
			var updatedata={'unit_id':$scope.unit_name.value , 'plan_id':$scope.plan_name.value };
			$http({
				method:'POST',
				url:"php/userwds/getPlanInfo.php",
				data:updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data, function(obj){
					$scope.date = obj.date;
				});
				
			},function errorCallback(response) {
				
				
			});
	
	
	}
	
	
	
	
	
	
	
	$scope.closeUnitDialog=function(){
			document.getElementById("addunit").style.display ="none";
	}
	
	$scope.addNewUnitName=function(){

		//alert("::"+$scope.new_unit_name);
		var unit_name = $scope.new_unit_name;
		//alert("unit_name::"+unit_name+"::");
		if(unit_name == "")
		{
			alert("Add Unit Name");
			return;
		}
		var session_id = $scope.session_name.value;
		var course_id = $scope.course_name.value;
		var semester_id = $scope.semester.value;
		var subject_id = $scope.subject_name.value;
		var section_id = $scope.section.value;
		
		
		var dataString = "session_id="+session_id+"&course_id="+course_id+"&semester_id="+semester_id+"&subject_id="+subject_id+"&section_id="+section_id+"&unit_name="+unit_name;
			
			$.ajax({ 
			type: "POST",url: "php/userplan/addUnitName.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.result);
				if(dobj.result == 0)
				{
					alert(dobj.error);
				}
				else
				{
					//$scope.checkTableData();
					document.getElementById("addunit").style.display ="none";
					
					$scope.viewUnitData= null;
					$http({
					method:'GET',
					url:"php/userplan/readUnitData.php",
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
					//console.log('res',response.data);
					$scope.viewUnitData=response.data;
					},function errorCallback(response) {
					});
					
					
					
					
				}
			} 
			});
	}
	
	
	
	$scope.editPlanData=function(pid,unit_id){
		id=pid;
		temp_unit_id = unit_id;
		var planid={'plan_id':id};
		$http({
			method:'POST',
			url:"php/userwds/editPrincipalPlanData.php",
			data:planid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			
			$scope.year=response.data[0].session;
			$scope.unit_name=response.data[0].unit_name;
			$scope.lession_plan=response.data[0].plan;
			$scope.info=response.data[0].dept_name+" / "+response.data[0].short_name+" / "+response.data[0].semester+" / "+response.data[0].subject_name+" / "+response.data[0].section_name;
			$scope.plandate=response.data[0].date;
			
			//alert("aaa");
			$scope.showCancel =  true;
			$scope.showAdd =  true;
			//alert("aaa");
			document.getElementById("show1").style.display ="block";
			document.getElementById("show2").style.display ="block";
			//alert("aaa");
		
		},function errorCallback(response) {
		});
		
	}
	
	
			
	$scope.viewPlanData=null;
	$scope.viewPlanData=[];
	$http({
			method:'GET',
			url:"php/userwds/readCompletePlanData.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('res',response.data);
			$scope.viewPlanData=response.data;
			document.getElementById("unit_list").style.display = "none";
			document.getElementById("plan_list").style.display = "block";
		},function errorCallback(response) {
			
				
	});

	
	$scope.showUnitData=function(){
		document.getElementById("unit_list").style.display = "block";
		document.getElementById("plan_list").style.display = "none";
	
	}
	
	$scope.showCompletedPlanData=function(unit_id){
		//alert("unit_id:"+unit_id);
		
		var updatedata={'unit_id':unit_id};
		
		$scope.viewPlanData=null;
		$scope.viewPlanData=[];
			
			$http({
				method:'POST',
				url:"php/userwds/getCompletedPlanData.php",
				data:updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('res',response.data);
				$scope.viewPlanData=response.data;
				document.getElementById("unit_list").style.display = "none";
				document.getElementById("plan_list").style.display = "block";
			},function errorCallback(response) {
				
				
			});
	
	}
	
	
	
	
	
	$scope.deletePlanData=function(pid,unit_id){
		id=pid;
		var planid={'plan_id':id , 'unit_id':unit_id};
		var deleteUser = $window.confirm('Are you sure you want to delete?');
		if(deleteUser){
			$http({
			method: 'POST',
			url: "php/userplan/deletePlanData.php",
			data:planid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			
				var updatedata={'unit_id':unit_id};
		
					$scope.viewPlanData=null;
					$scope.viewPlanData=[];
			
					$http({
						method:'POST',
						url:"php/userplan/readPlanData.php",
						data:updatedata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						//console.log('res',response.data);
						$scope.viewPlanData=response.data;
					},function errorCallback(response) {
						
						
					});
		
					
		
		
		},function errorCallback(response) {
			//alert(response.data);
			//$state.go('user.plan',{}, { reload: true });
		});
		}
	}
	$scope.clearPlanData=function(){
		
		//$scope.user_readonly= false;
		//$scope.showCancel =  false;
		//$scope.buttonName ="Add";
		$state.go('principal.myplanManagement.myWDS',{}, { reload: true });
	}
	
	$scope.listSearchSession=[];
	$http({
		method: 'GET',
		url: "php/plan/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSearchSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	$scope.listOfSearchCourse=[];
	$http({
		method:'GET',
		url:"php/userplan/getCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj1){
			var data={'name':obj1.short_name,'value':obj1.course_id};
			$scope.listOfSearchCourse.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.selectedSearchCourse=function(){
		     $scope.listOfSearchSemester=null;
			 $scope.listOfSearchSemester=[];
			 var key=['I','II','III','IV','V','VI','VII','VIII','IX','X'];
			 var userid={'id':$scope.courseSearch.value};
			 //console.log('cid',userid);
			 $http({
				 method:'POST',
				 url:"php/userplan/getSemesterData.php",
				 data: userid,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			 }).then(function successCallback(response){
				// console.log('cid res',response.data);
				 if($scope.courseSearch.name==response.data[0].short_name){
					 for(var i=0;i<response.data[0].semester;i++){
						 var val=i+1;
						 var rom_val=key[i];
						 var sem={'name':rom_val,'value':val};
						 $scope.listOfSearchSemester.push(sem);
					 }
				 }
			 },function errorCallback(response) {
				 $scope.noSemesters=null;
			 });
	}
	$scope.selectedSearchSemester=function(){
		$scope.listOfSearchSubject=null;
		$scope.listOfSearchSubject=[];
		
		var course_id = $scope.courseSearch.value;
		var semester_id = $scope.semesterSearch.value;
		//alert("semester_id::"+semester_id);
		var userdata={'course_id':course_id,'semester_id':semester_id};
		$http({
			method: 'POST',
			url: "php/userplan/getSubjectData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			//alert("::"+obj.subject_name);									
			var Subject={'name':obj.subject_name , 'value':obj.subject_id};
			$scope.listOfSearchSubject.push(Subject);
			
			});
			
			
			
		},function errorCallback(response) {
		});	
	}
	$scope.listOfSearchSection=[];
	$http({
		method:'GET',
		url:"php/userplan/getSectionData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj2){
			var data={'name':obj2.section_name,'value':obj2.section_id};
			$scope.listOfSearchSection.push(data);
		});
	},function errorCallback(response) {
	});
	
	$scope.listOfSearchUnit=[];
	$http({
				method:'POST',
				url:"php/userplan/getFilterUnitData.php",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('unit',response.data);
				angular.forEach(response.data,function(obj1){
					var unitvalue={'name':obj1.unit_name,'value':obj1.unit_name};
					$scope.listOfSearchUnit.push(unitvalue)
				})
			},function errorCallback(response) {
			});
	
	$scope.clearField=function(id){
		//alert("Clear:"+id);
		focusStreamField.removeBorderColor(id);
	}
	
	$scope.removeBorder=function(id){
		if($scope.colg_name != ''){
			focusStreamField.removeBorderColor(id);
		}
	}
	
});


/*$scope.listOfSearchUnit=[];
	$http({
				method:'POST',
				url:"php/userplan/getFilterUnitData.php",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data,function(obj1){
					var unitvalue={'name':obj1.unit_name,'value':obj1.unit_name};
					$scope.listOfSearchUnit.push(unitvalue)
				})
			},function errorCallback(response) {
			});
	
	$scope.listOfSearchPlan=[];
	$http({
		method:'GET',
		url:"php/userplan/getLessionPlanData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		alert("aaaaaaaaaaaaaaaaa");
		angular.forEach(response.data,function(obj1){
					var planvalue={'name':obj1.plan,'value':obj1.plan};
					$scope.listOfSearchPlan.push(planvalue)
				})
	},function errorCallback(response) {
	});
	*/
	
	
	
	
	
stream.factory('focusStreamField',function($timeout,$window){
	return{
		borderColor:function(id){
			var element = $window.document.getElementById(id);
			 if(element){
				 element.focus();
			     element.style.borderColor = "red";
			 }
		},
		removeBorderColor:function(id){
			$timeout(function(){
				var element = $window.document.getElementById(id);
				if(element){
					 element.style.borderColor = "#cccccc";
				}
			})
		}
	}
});
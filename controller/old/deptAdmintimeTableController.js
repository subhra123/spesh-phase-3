var dashboard=angular.module('Channabasavashwara');
dashboard.controller('deptAdmintimeTableController',function($scope,$http,$state,$window,$rootScope){
	//$scope.session="2015/16";
	//$scope.type;
	$scope.aprvprinc=true;
	$scope.aprvhod=true;
	
	
	
	$scope.listOfCourseName=[{
		name: 'Select Couse',
		value: ''
	}]
	$scope.courseName=$scope.listOfCourseName[0];
	
	$scope.listSession=[{
		name: 'Select Academic Year',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];	
	
	
	$scope.listPublish=[];
	$scope.listPublish=[{
		name: 'NO',
		value: '0'
	},{
		name: 'YES',
		value: '1'
	}]
	
	$scope.publish_name=$scope.listPublish[0];	
	
	
	$scope.listSection=[];
	$scope.listSection=[{
		name: 'Select Section',
		value: ''
	}]
	$scope.section_name=$scope.listSection[0];
	
	
	$scope.noSemesters=[];
	$scope.noSemesters = [{
    name :'Select Semester',
    value: ''
    }];	
	
	$scope.semester=$scope.noSemesters[0];
	
	$scope.noCourse=null;
	$scope.noCourse=[{
		name:'Select Course',
		value:''
	}];
	
	
	
	$http({
		method: 'GET',
		url:"php/course/getHODCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log("course"+response);
		angular.forEach(response.data,function(obj){
			var data={'name':obj.course_name,'value':obj.course_id};
			$scope.listOfCourseName.push(data);
		});
	},function errorCallback(response) {
		
	});
	
	
	

	
	$scope.selectSemester=function(){
		
		$scope.noSemesters=null;
		$scope.noSemesters=[{
			name:'Select Semester',
			value:''
		}]
		
		$scope.semester=$scope.noSemesters[0];
		var semid={'course_id':$scope.courseName.value};
		console.log('semid',semid);
		$http({
			method:'POST',
			url:"php/course/getPrincipalPlanCollegeSemester.php",
			data:semid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data, function(obj){
				var no_semester = obj.semester;
				//alert("no_semester:"+no_semester);
				var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
				for(var i = 0; i < no_semester; i++){
					var val = i+1;
					var roman_val = key[i];
					var seme = {'name':roman_val , 'value':val};
					$scope.noSemesters.push(seme);
				}
			});
		},function errorCallback(response) {
		});
		//check data
		$scope.checkTableData();
	}
	
	
	 $scope.noOfDays = [];
     $scope.listOfTimeData=[];
       /* $scope.days = {
          '0': "Monday",
          '1': 'Tuesday',
          '2': 'Wednesday',
          '3': 'Thursday',
          '4': 'Friday'
        }*/
        
        /*$scope.hours = [
          '9AM :: 10AM',
          '10AM :: 11AM',
          '11:15AM :: 12:15PM',
          '12:15PM :: 01:15PM',
          '02PM :: 03PM',
          '03PM :: 04PM',
          '04PM :: 05PM'
        ]*/
        $http({
			method: 'GET',
			url: "php/timetable/gethour.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('hour',response.data);
			$scope.hours=response.data;
		},function errorCallback(response) {
			
		})
		$http({
			method: 'GET',
			url: "php/timetable/getdays.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			 $scope.days=response.data;
			 //console.log('days',$scope.days);
			for (var i = 0; i < 5; i++) {
               $scope.noOfDays.push($scope.days[i]);
            }
		},function errorCallback(response) {
		});
		$scope.listOfSubjectName=[{
		name: 'Select Subject',
		value: ''
	    }]
		$scope.sub_name  = $scope.listOfSubjectName[0];
		
		$scope.listOfFacultyName=[{
		name: 'Select Faculty',
		value: ''
	    }]
		$scope.fac_name=$scope.listOfFacultyName[0];
		
		
		$scope.listOfVenueName=[{
		name: 'Select Venue',
		value: ''
	    }]
		$scope.venue_name  = $scope.listOfVenueName[0];
		
		
		
		
		
		////////////////////////////////////////////////////////////////check table data
		
		$scope.checkTableData=function(){
		
		if($scope.courseName.value != "" && $scope.semester.value != "" && $scope.session_name.value != "" && $scope.section_name.value != "" )
		{
			
			var dataString = "stream_id="+$scope.stream_name.value+"&course_id="+$scope.courseName.value+"&semester_id="+$scope.semester.value+"&session_id="+$scope.session_name.value+"&section_id="+$scope.section_name.value;
			//console.log("Data:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/timetable/getHODPlanTimetableData.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
				var dobj=jQuery.parseJSON(html);
				//console.log('table data',dobj);
				$scope.updateFields(dobj);
			} 
			});

			
		}
		else
		{
			$("#detailsstockid").html("");
			$("#data_hide").html("");
			document.getElementById("hod_status_id").style.display  = "none";
			document.getElementById("principal_status_id").style.display  = "none";
			document.getElementById("saveData").style.display  = "none";
	
		}
		
		}
		
		
		
		
		$scope.addTimeTableData=function(){
			console.log('add time',$scope.listOfTimeData);
			for(var i=0;i<= $scope.listOfTimeData.length;i++){
				$http({
					method:'POST',
					url: "php/timetable/addTimeTable.php",
					data:$scope.listOfTimeData[i],
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//alert(response.data['msg']);
				},function errorCallback(response) {
					//alert(response.data['msg']);
				});
			}
		}
		$scope.checkData=function(subname,facname,hour,day){
			//console.log('data',subname,facname,hour,day);
			if(subname.value==''){
				alert("select subject from list");
				$scope.fac_name=$scope.listOfFacultyName[0];
			}else{
				var subdata={'subtype':subname.value};
				$http({
					method:'POST',
					url: "php/timetable/getSubjectType.php",
					data:subdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//console.log('TYPE',response);
					$rootScope.type=response.data[0].subject_type;
					var time={'sub_name':subname.value,'fac_name':facname.value,'hour':hour.id,'day':day.id,'sub_type':$rootScope.type};
			       $scope.listOfTimeData.push(time);
				},function errorCallback(response) {
				});
			}
			//console.log('data',$scope.listOfTimeData);
		}
		
		
		$scope.updateFields=function(dobj){
			$("#detailsstockid").html(dobj.data);
			$("#data_hide").html(dobj.data_hide);
			//alert("::"+dobj.status)
			if(dobj.status == 1)
				document.getElementById("publish_name").value = 1;
			else
				document.getElementById("publish_name").value = 0;
			if(dobj.hod_status == 1)	
				document.getElementById("hod_approve").value = "1";
			else
				document.getElementById("hod_approve").value = "0";
			
			if(dobj.principal_status == 1)	
				document.getElementById("principal_approve").value = "1";
			else
				document.getElementById("principal_approve").value = "0";
			
			//alert()
			document.getElementById("tableid").value  = dobj.table_id;
			document.getElementById("hod_remarks").value  = dobj.hod_comment;
			document.getElementById("principal_remarks").value  = dobj.principal_comment;
			
			document.getElementById("hod_status_id").style.display  = "block";
			document.getElementById("principal_status_id").style.display  = "block";
			document.getElementById("saveData").style.display  = "block";
			
		}
		
		
 
	
	$scope.listOfStream=[{
		name:'Select Stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
	/*$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});*/
		$http({
		method: 'GET',
		url:"php/stream/readPricpalStreamData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.stream_name,'value':obj.stream_id};
			$scope.listOfStream.push(data);
		});
	},function errorCallback(response) {
		
	});
	$http({
		method: 'GET',
		url: "php/timetable/readPricpalPlanSession.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	

/*
	$http({
		method: 'GET',
		url: "php/timetable/readVenue.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.venue_name);									
			var Venue={'name':obj.venue_name , 'value':obj.venue_id};
			$scope.listVenues.push(Venue);
		});
	},function errorCallback(response) {
		
	});
	*/
	$http({
		method: 'GET',
		url: "php/timetable/readPricpalPlanSection.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.section_name);									
			var Section={'name':obj.section_name , 'value':obj.section_id};
			$scope.listSection.push(Section);
		});
	},function errorCallback(response) {
		
	});		
	
});
var GoFastoApp=angular.module('GofastoHome',['ngRoute']);
GoFastoApp.config(function($routeProvider,$locationProvider) {
	$routeProvider
	.when('/',{
		templateUrl : 'view/home.html',
		controller  : 'homecontroller'
	})
	.when('/deptinfo',{
		templateUrl : 'view/info.html',
		controller  : 'infocontroller'
	})
	.when('/TimeTable',{
		templateUrl : 'view/time.html',
		controller  : 'timecontroller'
	})
	.when('/course',{
		templateUrl : 'view/course.html',
		controller  : 'coursecontroller'
	})
	.when('/subject',{
		templateUrl : 'view/subject.html',
		controller  : 'subjectcontroller'
	})
	.when('/hod',{
		templateUrl : 'view/hod.html',
		controller  : 'hodcontroller'
	})
	.when('/faculty',{
		templateUrl : 'view/faculty.html',
		controller  : 'facultycontroller'
	});
	/*$locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    });*/
})
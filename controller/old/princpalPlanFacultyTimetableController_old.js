var dashboard=angular.module('Channabasavashwara');
dashboard.controller('princpalPlanFacultyTimetableController',function($scope,$http,$state,$window,$rootScope){
	//$scope.session="2015/16";
	//$scope.type;
	
	$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Session',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];	
	
	$scope.listFaculty=[];
	$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
	$scope.faculty_name=$scope.listFaculty[0];	
	
	
	$http({
		method: 'GET',
		url: "php/facultytimetable/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	
	
	$http({
		method: 'GET',
		url: "php/facultytimetable/readFaculty.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.user_name);									
			var Faculty={'name':obj.role+" : "+obj.user_name , 'value':obj.user_id};
			$scope.listFaculty.push(Faculty);
		});
	},function errorCallback(response) {
		
	});
	


	 $scope.noOfDays = [];
     $scope.listOfTimeData=[];
      
	  $http({
			method: 'GET',
			url: "php/facultytimetable/gethour.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('hour',response.data);
			$scope.hours=response.data;
		},function errorCallback(response) {
			
		})
		
		$http({
			method: 'GET',
			url: "php/facultytimetable/getdays.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			 $scope.days=response.data;
			 //console.log('days',$scope.days);
			for (var i = 0; i < 5; i++) {
               $scope.noOfDays.push($scope.days[i]);
            }
		},function errorCallback(response) {
		});
		
		$scope.listOfSubjectName=[{
		name: 'Select Subject',
		value: ''
	    }]
		
		$scope.sub_name  = $scope.listOfSubjectName[0];
		
		$scope.listOfFacultyName=[{
		name: 'Select Faculty',
		value: ''
	    }]
		
		$scope.fac_name=$scope.listOfFacultyName[0];
		
		
		$scope.listOfVenueName=[{
		name: 'Select Venue',
		value: ''
	    }]
		$scope.venue_name  = $scope.listOfVenueName[0];
		
		
		
		
		
		////////////////////////////////////////////////////////////////check table data
		
		$scope.checkTableData=function(){
		
			if( $scope.session_name.value != "" && $scope.faculty_name.value != "" )
			{
				
				var dataString = "session_id="+$scope.session_name.value+"&faculty_id="+$scope.faculty_name.value;
				//alert("Data:"+dataString);
				$.ajax({ 
				type: "POST",url: "php/facultytimetable/getTimetableData.php" ,data: dataString,cache: false,
				success: function(html)
				{ 
					//alert("Received Data::"+html);
					var dobj=jQuery.parseJSON(html);
					$scope.updateFields(dobj);
				} 
				});
			}
		}
		
		
		
		$scope.updateFields=function(dobj){
			$("#detailsstockid").html(dobj.data);
			//$("#data_hide").html(dobj.data_hide);
			//alert("::"+dobj.status)
		}
		
		

		
		
});




var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('princpalResourceClassController',function($scope,$http,$state,$window,classField){
	$scope.buttonName="Add";
	$scope.classData=[];
	var id='';
	$http({
		method:'GET',
		url:"php/Role/readCollegeClassData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.classData=response.data;
	},function errorCallback(response) {
	});
	$scope.addClassData=function(){
		if($scope.buttonName=="Add"){
			 if($scope.subject==null || $scope.subject==''){
				alert('Please select your subject type...');
				classField.borderColor('resourcesub');
			}else if($scope.period=='' || $scope.period==null){
				alert('Please select the class...');
				classField.borderColor('resourceperiod');
			}else{
				var userdata={'sub_type':$scope.subject,'period':$scope.period};
				$http({
					method:'POST',
					url:"php/Role/addCollegeClassData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$scope.subject=null;
					$scope.period='';
					if($scope.classData=='null'){
						$scope.classData=[];
						$scope.classData.push(response.data);
					}else{
						$scope.classData.unshift(response.data);
					}
					//$scope.classData.unshift(response.data);
					//$state.go('principal.resourse.class',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.subject==response.data.type){
						classField.borderColor('resourcesub');
					}else{
					$state.go('principal.resourse.class',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.subject==null || $scope.subject==''){
				alert('Please select your subject type...');
				classField.borderColor('resourcesub');
			}else if($scope.period=='' || $scope.period==null){
				alert('Please select the class...');
				classField.borderColor('resourceperiod');
			}else{
				var updatedata={'sub_type':$scope.subject,'id':id,'period':$scope.period};
				console.log('user',updatedata);
				$http({
					method:'POST',
					url:"php/Role/UpdateCollegeClassData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.resourse.class',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.subject==response.data.type){
						classField.borderColor('resourcesub');
					}else{
					$state.go('principal.resourse.class',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editSubjectData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/Role/editCollegeClassData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.subject=response.data[0].type;
			$scope.period=response.data[0].period;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteSubjectData=function(sid){
		id=sid;
		var userid={'id':id};

		var conmesg=$window.confirm('Are you sure to remove this Subject type');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/Role/deleteCollegeClassData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('principal.resourse.class',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			$state.go('principal.resourse.class',{}, { reload: true });
		});
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			classField.clearBorderColor(id);
		}
	}
	$scope.clearField=function(id){
		classField.clearBorderColor(id);
	}
	$scope.cancelClassData=function(){
		$state.go('principal.resourse.class',{}, { reload: true });
	}
});
resoursecourse.factory('classField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
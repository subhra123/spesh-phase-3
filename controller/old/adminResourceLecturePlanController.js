var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('adminResourceLecturePlanController',function($scope,$http,$state,$window,lecturePlanField){
	$scope.buttonName="Add";
	var id='';
	$scope.listOfCollege=[{
		name:'Select college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'GET',
		url:"php/resource/readLecturePlanData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.lecturePlanData=response.data;
	},function errorCallback(response) {
	});
	$scope.addLecturePlanData=function(){
		if($scope.buttonName=="Add"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select college name');
				lecturePlanField.borderColor('colg_name');
			}else if($scope.lect_plan==null || $scope.lect_plan==''){
				alert('Please add Lecture Plan...');
				lecturePlanField.borderColor('resourcelectplan');
			}else{
				var userdata={'colg_name':$scope.colg_name.value,'lect_plan':$scope.lect_plan};
				$http({
					method:'POST',
					url:"php/resource/addLecturePlanData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$scope.lect_plan=null;
					//$state.go('dashboard.res.lecturePlan',{}, { reload: true });
					if($scope.lecturePlanData=='null'){
						$scope.lecturePlanData=[];
						$scope.lecturePlanData.push(response.data);
					}else{
						$scope.lecturePlanData.unshift(response.data);
					}
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.lect_plan==response.data.plan_name){
						lecturePlanField.borderColor('resourcelectplan');
					}else{
					//$state.go('dashboard.res.section',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select your college name');
				lecturePlanField.borderColor('colg_name');
			}else if($scope.lect_plan==null || $scope.lect_plan==''){
				alert('Please add Lecture Plan...');
				lecturePlanField.borderColor('resourcelectplan');
			}else{
				var updatedata={'colg_name':$scope.colg_name.value,'lect_plan':$scope.lect_plan,'id':id};
				$http({
					method:'POST',
					url:"php/resource/UpdateLecturePlanData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.res.lecturePlan',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.lect_plan==response.data.plan_name){
						lecturePlanField.borderColor('resourcelectplan');
					}else{
						//$state.go('dashboard.res.section',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editLecturePlanData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/resource/editLecturePlanData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colg_name.value=response.data[0].clg_id;
			$scope.lect_plan=response.data[0].plan_name;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.colgread=true;
			$scope.colgdis=true;
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteLecturePlanData=function(sid){
		id=sid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Lecture Plan');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/resource/deleteLecturePlanData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('dashboard.res.lecturePlan',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			$state.go('dashboard.res.lecturePlan',{}, { reload: true });
		});
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			lecturePlanField.clearBorderColor(id);
		}
	}
	$scope.clearField=function(id){
		lecturePlanField.clearBorderColor(id);
	}
	$scope.cancelLecturePlanData=function(){
		$state.go('dashboard.res.lecturePlan',{}, { reload: true });
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
});
resourseclass.factory('lecturePlanField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
var stream=angular.module('Channabasavashwara');
stream.controller('pricipalDeptStreamController',function($scope,$state,$window,$http,focusStreamField){
	$scope.buttonName="Add";
	$scope.clearButtonName="Cancel";
	$scope.showCancel= false;
	var id='';
	$scope.listOfSearchCollege=[];
	
	$http({
		method:'GET',
		url:"php/stream/readCollegeStreamData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('read',response);
		if(response.data != "null"){
		$scope.listOfStreamData=response.data;
		}
	},function errorCallback(response) {
	})
	
	
	
	$scope.addStreamData=function(){
		
		if($scope.stream_name==null || $scope.stream_name==''){
				alert('Stream name field could not blank');
				focusStreamField.borderColor('stream_name');
				return;
			}else if($scope.stream_code==null || $scope.stream_code==''){
				alert('Stream code field could not be blank');
				focusStreamField.borderColor('stream_code');
				return;
			}
			
			
		if($scope.buttonName=="Add"){
				//console.log('data',$scope.colg_name.name,$scope.stream_name);
				var userdata={'stream_name':$scope.stream_name,'stream_code':$scope.stream_code};
				$http({
					method:'POST',
					url:"php/stream/addCollegeStreamData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.dept.stream',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
				})
		}
		
		else if($scope.buttonName=="Update"){
				var userdata={'stream_name':$scope.stream_name,'stream_code':$scope.stream_code,'id':id};
				$http({
					method:'POST',
					url:"php/stream/updateCollegeStreamData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.dept.stream',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					//$state.go('dashboard.stream',{}, { reload: true });
				})
		}
	}
	
	
	$scope.editStreamData=function(sid){
		//alert("Edit..");
		id=sid;
		var userid={'streamid':id};
		$http({
			method:'POST',
			url:"php/stream/editStreamData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//$scope.colg_name.value=response.data[0].colg_name;
			$scope.stream_name=response.data[0].stream_name;
			$scope.stream_code=response.data[0].stream_code;
			$scope.buttonName="Update";
			$scope.showCancel= true;
		},function errorCallback(response) {
		});
	}
	
	
	$scope.clearSubjectData=function(){
		//alert("Edit..");
		//$scope.showCancel= true;
		$state.go('principal.dept.stream',{}, { reload: true });
	}
	
	$scope.deleteStreamData=function(sid){
		id=sid;
		var userid={'streamid':id};
		var conmesg=$window.confirm('Are you sure to remove this Stream data');
		if(conmesg){
			$http({
				method:'POST',
				url:"php/stream/deleteStreamData.php",
				data:userid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				$state.go('principal.dept.stream',{}, { reload: true });
			},function errorCallback(response) {
				alert(response.data['msg']);
				$state.go('principal.dept.stream',{}, { reload: true });
			});
		}
	}
	$scope.clearField=function(id){
		focusStreamField.removeBorderColor(id);
	}
	$scope.removeBorder=function(id){
		if($scope.colg_name != ''){
			focusStreamField.removeBorderColor(id);
		}
	}
});
stream.factory('focusStreamField',function($timeout,$window){
	return{
		borderColor:function(id){
			var element = $window.document.getElementById(id);
			 if(element){
				 element.focus();
			     element.style.borderColor = "red";
			 }
		},
		removeBorderColor:function(id){
			$timeout(function(){
				var element = $window.document.getElementById(id);
				if(element){
					 element.style.borderColor = "#555555";
				}
			})
		}
	}
});
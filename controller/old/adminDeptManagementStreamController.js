var stream=angular.module('Channabasavashwara');
stream.controller('adminDeptManagementStreamController',function($scope,$state,$window,$http,focusStreamField){
	$scope.buttonName="Add";
	var id='';
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/readStreamData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('read',response);
		if(response.data != "null"){
		$scope.listOfStreamData=response.data;
		}
	},function errorCallback(response) {
	})
	$scope.listOfCollege=[{
		name:'select  college ',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.addStreamData=function(){
		if($scope.buttonName=="Add"){
			//console.log('data',$scope.colg_name.name,$scope.stream_name);
			if($scope.colg_name.value==''){
				alert('Please select  college name');
				focusStreamField.borderColor('colg_name');
			}else if($scope.stream_name==null || $scope.stream_name==''){
				alert('Stream name field could not blank');
				focusStreamField.borderColor('stream_name');
			}else if($scope.stream_code==null || $scope.stream_code==''){
				alert('Stream code field could not be blank');
				focusStreamField.borderColor('stream_code');
			}else{
				var userdata={'colgname':$scope.colg_name.value,'stream_name':$scope.stream_name,'stream_code':$scope.stream_code};
				$http({
					method:'POST',
					url:"php/stream/addStreamData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//console.log('retriv',response);
					alert(response.data['msg']);
					//$scope.colg_name.value='';
					$scope.stream_name=null;
					$scope.stream_code=null;
					//$state.go('dashboard.deptmanagement.stream',{}, { reload: true });
					if($scope.listOfStreamData=='null'){
						$scope.listOfStreamData=[];
						$scope.listOfStreamData.push(response.data);
					}else{
						$scope.listOfStreamData.unshift(response.data);
					}
				},function errorCallback(response) {
					//console.log('strm res',response.data);
					alert(response.data['msg']);
					if($scope.stream_name==response.data.stream_name){
						focusStreamField.borderColor('stream_name');
					}else if($scope.stream_code==response.data.stream_code){
						focusStreamField.borderColor('stream_code');
					}else{
						$state.go('dashboard.deptmanagement.stream',{}, { reload: true });
					}
				})
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.colg_name.value==''){
				alert('Please select your college name');
				focusField.borderColor('colg_name');
			}else if($scope.stream_name==null){
				alert('Stream name field could not blank');
				focusField.borderColor('stream_name');
			}else if($scope.stream_code==null){
				alert('Stream code field could not be blank');
				focusField.borderColor('stream_code');
			}else{
				var userdata={'colgname':$scope.colg_name.value,'stream_name':$scope.stream_name,'stream_code':$scope.stream_code,'id':id};
				$http({
					method:'POST',
					url:"php/stream/updateStreamData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.deptmanagement.stream',{}, { reload: true });
				},function errorCallback(response) {
					console.log('err',response.data);
					alert(response.data['msg']);
					if($scope.stream_name==response.data.stream_name){
						focusStreamField.borderColor('stream_name');
					}else if($scope.stream_code==response.data.stream_code){
						focusStreamField.borderColor('stream_code');
					}else{
						$state.go('dashboard.deptmanagement.stream',{}, { reload: true });
					}
				})
			}
		}
	}
	$scope.editStreamData=function(sid){
		id=sid;
		var userid={'streamid':id};
		$http({
			method:'POST',
			url:"php/stream/editStreamData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('edited data',response);
			$scope.colg_name.value=response.data[0].clg_id;
			$scope.stream_name=response.data[0].stream_name;
			$scope.stream_code=response.data[0].stream_code;
			$scope.readcolg=true;
			$scope.colgdis=true;
			$scope.buttonName="Update";
			$scope.CancelbuttonName="Cancel";
			$scope.CancelButton=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteStreamData=function(sid){
		id=sid;
		var userid={'streamid':id};
		var conmesg=$window.confirm('Are you sure to remove this Stream data');
		if(conmesg){
			$http({
				method:'POST',
				url:"php/stream/deleteStreamData.php",
				data:userid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				$state.go('dashboard.deptmanagement.stream',{}, { reload: true });
			},function errorCallback(response) {
				alert(response.data['msg']);
				$state.go('dashboard.deptmanagement.stream',{}, { reload: true });
			});
		}
	}
	$scope.clearField=function(id){
		focusStreamField.removeBorderColor(id);
	}
	$scope.removeBorder=function(id){
		if($scope.colg_name != ''){
			focusStreamField.removeBorderColor(id);
		}
	}
	$scope.cancelStreamData=function(){
		$state.go('dashboard.deptmanagement.stream',{}, { reload: true });
	}
});
stream.factory('focusStreamField',function($timeout,$window){
	return{
		borderColor:function(id){
			var element = $window.document.getElementById(id);
			 if(element){
				 element.focus();
			     element.style.borderColor = "red";
			 }
		},
		removeBorderColor:function(id){
			$timeout(function(){
				var element = $window.document.getElementById(id);
				if(element){
					 element.style.borderColor = "#cccccc";
				}
			})
		}
	}
});
var dashboard=angular.module('Channabasavashwara');
dashboard.controller('dashboardfacultyTimeTableController',function($scope,$http,$state,$window,$rootScope){
	//$scope.session="2015/16";
	//$scope.type;
	
	$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Session',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];	
	
	$scope.listFaculty=[];
	$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
	$scope.faculty_name=$scope.listFaculty[0];	
	
	
	/*$http({
		method: 'GET',
		url: "php/facultytimetable/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});*/
	
	$scope.selectFaculty=function(){
		$scope.listFaculty=null;
		$scope.listFaculty=[];
		$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
	var deptid={'colg_id':$scope.colg_name.value,'deptid':$scope.dept_name.value}
	$http({
		method: 'POST',
		url: "php/princpaltime/readPlanFaculty.php",
		data:deptid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		if(response.data=="null"){
			$scope.listFaculty=null;
		$scope.listFaculty=[];
		$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
		}else{
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.user_name);									
			var Faculty={'name':obj.role+" : "+obj.user_name , 'value':obj.user_id};
			$scope.listFaculty.push(Faculty);
		});
		}
	},function errorCallback(response) {
		
	});
	
	}

	 $scope.noOfDays = [];
     $scope.listOfTimeData=[];
      
	  $http({
			method: 'GET',
			url: "php/facultytimetable/gethour.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('hour',response.data);
			$scope.hours=response.data;
		},function errorCallback(response) {
			
		})
		
		$http({
			method: 'GET',
			url: "php/facultytimetable/getdays.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			 $scope.days=response.data;
			 //console.log('days',$scope.days);
			for (var i = 0; i < 5; i++) {
               $scope.noOfDays.push($scope.days[i]);
            }
		},function errorCallback(response) {
		});
		
		$scope.listOfSubjectName=[{
		name: 'Select Subject',
		value: ''
	    }]
		
		$scope.sub_name  = $scope.listOfSubjectName[0];
		
		$scope.listOfFacultyName=[{
		name: 'Select Faculty',
		value: ''
	    }]
		
		$scope.fac_name=$scope.listOfFacultyName[0];
		
		
		$scope.listOfVenueName=[{
		name: 'Select Venue',
		value: ''
	    }]
		$scope.venue_name  = $scope.listOfVenueName[0];
		
		
		
		
		
		////////////////////////////////////////////////////////////////check table data
		
		$scope.checkTableData=function(){
		
			if( $scope.colg_name.value != "" && $scope.session_name.value != "" && $scope.faculty_name.value != "" && $scope.dept_name.value != "" )
			{
				
				var dataString = "colg_id="+$scope.colg_name.value+"&session_id="+$scope.session_name.value+"&faculty_id="+$scope.faculty_name.value+"&dept_id="+$scope.dept_name.value;
				//console.log("Data:"+dataString);
				$.ajax({ 
				type: "POST",url: "php/dashboardtime/getdashboardFacultyTimetableData.php" ,data: dataString,cache: false,
				success: function(html)
				{ 
					//console.log("Received Data::"+html);
					var dobj=jQuery.parseJSON(html);
					$scope.updateFields(dobj);
				} 
				});
			}
		}
		
		
		
		$scope.updateFields=function(dobj){
			$("#detailsstockid").html(dobj.data);
			//$("#data_hide").html(dobj.data_hide);
			//alert("::"+dobj.status)
		}
		
		

	$scope.listOfDept=[{
		name:'Select department Name',
		value:''
	}]
	/*$http({
		method:'GET',
		url:"php/princpaltime/getDeptData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		angular.forEach(response.data,function(dept){
			var data={'name':dept.dept_name,'value':dept.dept_id};
			$scope.listOfDept.push(data);
		});
	},function errorCallback(response) {
	});*/
	$scope.dept_name=$scope.listOfDept[0];	
	
		
     $scope.listOfCollege=[{
		name:'Select your college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$scope.listOfStream=[{
		name:'Select your stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.selectStream=function(){
	$scope.listOfStream=null;
	$scope.listOfStream=[{
		name:'Select Stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
	var colgid={'colg_name':$scope.colg_name.value};
		$http({
		method: 'POST',
		url:"php/department/readStreamData.php",
		data:colgid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.stream_name,'value':obj.stream_id};
			$scope.listOfStream.push(data);
		});
	},function errorCallback(response) {
		
	});
	$scope.listSession=null;
	$scope.listSession=[{
		name: 'Select Session',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];	
	var clgid={'colg_id':$scope.colg_name.value};
	$http({
		method: 'POST',
		url: "php/timetable/readPlanSession.php",
		data:clgid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		console.log('session',response);
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	}
	$scope.GetDeptData=function(){
		$scope.listOfdept=null;
		$scope.listOfdept=[{
		name:'Select Department Name',
		value:''
	    }]
	    $scope.dept_name=$scope.listOfdept[0];
		
		var courseid={'colg_id':$scope.colg_name.value,'stream_id':$scope.stream_name.value}
		$http({
			method:'POST',
			url:"php/deptsubject/readDeptValue.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('dept',response);
			angular.forEach(response.data,function(obj){
				var data={'name':obj.dept_name,'value':obj.dept_id};
				$scope.listOfDept.push(data);
			});
		},function errorCallback(response) {
		});
	}
		
});

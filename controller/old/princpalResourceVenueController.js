var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('princpalResourceVenueController',function($scope,$http,$state,$window,venueField){
	$scope.buttonName="Add";
	var id='';
	$http({
		method:'GET',
		url:"php/Role/readCollegeVenueData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.venueData=response.data;
	},function errorCallback(response) {
	});
	$scope.addSessionData=function(){
		if($scope.buttonName=="Add"){
			if($scope.venue==null || $scope.venue==''){
				alert('Please select  Venue...');
				venueField.borderColor('resourcevenue');
			}else{
				var userdata={'venue':$scope.venue};
				$http({
					method:'POST',
					url:"php/Role/addCollegeVenueData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					//$scope.venue=null;
					//$scope.venueData.unshift(response.data);
					$state.go('principal.resourse.venue',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.venue==response.data.venue_name){
						venueField.borderColor('resourcevenue');
					}else{
					$state.go('principal.resourse.venue',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.venue==null || $scope.venue==''){
				alert('Please select your Venue...');
				venueField.borderColor('resourcevenue');
			}else{
				var updatedata={'venue':$scope.venue,'id':id};
				$http({
					method:'POST',
					url:"php/Role/UpdateCollegeVenueData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.resourse.venue',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.venue==response.data.venue_name){
						venueField.borderColor('resourcevenue');
					}else{
					$state.go('principal.resourse.venue',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editSessionData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/Role/editCollegeVenueData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.venue=response.data[0].venue_name;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			//$scope.colgread=true;
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteSessionData=function(sid){
		id=sid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Venue');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/Role/deleteCollegeVenueData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('principal.resourse.venue',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			$state.go('principal.resourse.venue',{}, { reload: true });
		});
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			venueField.clearBorderColor(id);
		}
		
	}

	$scope.clearField=function(id){
		venueField.clearBorderColor(id);
	}
	$scope.cancelSessionData=function(){
		$state.go('principal.resourse.venue',{}, { reload: true });
	}
});
resourseclass.factory('venueField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
var deptsub=angular.module('Channabasavashwara');
deptsub.controller('deptsubjectController',function($http,$scope,$window,$state,subjectField){
	$scope.buttonName="Add";
	var id='';
	$scope.listOfCollege=[{
		name:'Select your college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$scope.listOfStream=[{
		name:'Select your stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
	$scope.listOfdept=[{
		name:'Select your department',
		value:''
	}]
	$scope.dept_name=$scope.listOfdept[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.noCourse=[{
			name:'select your course',
			value:''
		}]
	$scope.courseName=$scope.noCourse[0];
	$scope.noSubjectType=[{
			name:'Select your subject type',
			value:''
	}]
	$scope.subtype=$scope.noSubjectType[0];
	$scope.selectCourse=function(mod){
		if(mod != ''){
			subjectField.clearBorderColor('colg_name');
		}
		$scope.noSubjectType=null;
		$scope.noSubjectType=[{
			name:'Select your subject type',
			value:''
		}]
		$scope.subtype=$scope.noSubjectType[0];
		var courseid={'id':$scope.colg_name.value};
		$http({
			method:'POST',
			url:"php/deptsubject/getSubjectType.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.type,'value':obj.id};
				$scope.noSubjectType.push(data);
			});
		},function errorCallback(response) {
		});
		$scope.listOfStream=null;
		$scope.listOfStream=[{
		name:'Select your stream',
		value:''
	    }]
		var colgid={'colg_name':$scope.colg_name.value};
			$http({
				method:'POST',
				url:"php/department/readStreamData.php",
				data:colgid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data,function(obj){
					var data={'name':obj.stream_name,'value':obj.stream_id};
					$scope.listOfStream.push(data);
				});
			},function errorCallback(response) {
			});
	    $scope.stream_name=$scope.listOfStream[0];
	}
	$scope.noSemesters=null;
	$scope.noSemesters=[{
			name:'select your semester',
			value:''
	}]
	$scope.semester=$scope.noSemesters[0];
	$scope.selectSemester=function(mod){
		if(mod != ''){
			subjectField.clearBorderColor('coursename');
		}
		$scope.noSemesters=null;
		$scope.noSemesters=[{
			name:'select your semester',
			value:''
		}]
		$scope.semester=$scope.noSemesters[0];
		var semid={'colg_id':$scope.colg_name.value,'course_id':$scope.courseName.value};
		$http({
			method:'POST',
			url:"php/deptsubject/getSemester.php",
			data:semid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data, function(obj){
				var no_semester = obj.semester;
				var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
				for(var i = 0; i < no_semester; i++){
					var val = i+1;
					var roman_val = key[i];
					var seme = {'name':roman_val , 'value':val};
					$scope.noSemesters.push(seme);
				}
			});
		},function errorCallback(response) {
		});
	}
	$scope.selectdept=function(mod){
		if(mod != ''){
			subjectField.clearBorderColor('dept_name');
		}
	}
	$scope.addSubjectData=function(){
		if($scope.buttonName=="Add"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('Please select your college...');
				subjectField.borderColor('colg_name');
			}else if($scope.stream_name.value=='' || $scope.stream_name.value==null){
				alert('Please select your Stream..');
				subjectField.borderColor('stream_name');
			}else if($scope.dept_name.value=='' || $scope.dept_name.value==null){
				alert('Please select your department..');
				subjectField.borderColor('dept_name');
			}else if($scope.courseName.value=='' || $scope.courseName.value==null){
				alert('Please select your course..');
				subjectField.borderColor('coursename');
			}else if($scope.semester.value=='' || $scope.semester.value==null){
				alert('Please select your semester..');
				subjectField.borderColor('sem');
			}else if($scope.subtype.value=='' || $scope.subtype.value==null){
				alert('Please select your subject type... ');
				subjectField.borderColor('sub_type');
			}else if($scope.subjectname=='' || $scope.subjectname==null){
				alert('Subject name field could not be blank...');
				subjectField.borderColor('subname');
			}else if($scope.subject_code=='' || $scope.subject_code==null){
				alert('Subject code field could not be blank..');
				subjectField.borderColor('subcode');
			}else{
				var adddata={'colg_name':$scope.colg_name.value,'stream_name':$scope.stream_name.value,'dept_name':$scope.dept_name.value,'course_name':$scope.courseName.value,'semester':$scope.semester.value,'sub_type':$scope.subtype.value,'sub_name':$scope.subjectname,'sub_code':$scope.subject_code};
				$http({
					method:'POST',
					url:"php/deptsubject/addSubjectData.php",
					data:adddata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$scope.colg_name.value='';
					$scope.stream_name.value='';
					$scope.dept_name.value='';
					$scope.semester.value='';
					$scope.subtype.value='';
					$scope.subjectname=null;
					$scope.subject_code=null;
					console.log('shift',$scope.subjectData);
			        $scope.subjectData.unshift(response.data);
				},function errorCallback(response) {
					//console.log('err',response);
					alert(response.data['msg']);
					if($scope.subjectname==response.data.subject_name){
						subjectField.borderColor('subname');
					}else if($scope.subject_code==response.data.short_name){
						subjectField.borderColor('subcode');
					}else{
					$state.go('dashboard.deptmanagement.sub',{},{reload:true});
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('Please select your college...');
				subjectField.borderColor('colg_name');
			}else if($scope.stream_name.value=='' || $scope.stream_name.value==null){
				alert('Please select your Stream..');
				subjectField.borderColor('stream_name');
			}else if($scope.dept_name.value=='' || $scope.dept_name.value==null){
				alert('Please select your department..');
				subjectField.borderColor('dept_name');
			}else if($scope.courseName.value=='' || $scope.courseName.value==null){
				alert('Please select your course..');
				subjectField.borderColor('coursename');
			}else if($scope.semester.value=='' || $scope.semester.value==null){
				alert('Please select your semester..');
				subjectField.borderColor('sem');
			}else if($scope.subtype.value=='' || $scope.subtype.value==null){
				alert('Please select your subject type... ');
				subjectField.borderColor('sub_type');
			}else if($scope.subjectname=='' || $scope.subjectname==null){
				alert('Subject name field could not be blank...');
				subjectField.borderColor('subname');
			}else if($scope.subject_code=='' || $scope.subject_code==null){
				alert('Subject code field could not be blank..');
				subjectField.borderColor('subcode');
			}else{
				var updatedata={'colg_name':$scope.colg_name.value,'stream_name':$scope.stream_name.value,'dept_name':$scope.dept_name.value,'course_name':$scope.courseName.value,'semester':$scope.semester.value,'sub_type':$scope.subtype.value,'sub_name':$scope.subjectname,'sub_code':$scope.subject_code,'id':id};
				$http({
					method:'POST',
					url:"php/deptsubject/updateSubjectData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.deptmanagement.sub',{},{reload:true});
				},function errorCallback(response) {
					//console.log('err1',response);
					alert(response.data['msg']);
					if($scope.subjectname==response.data.subject_name){
						subjectField.borderColor('subname');
					}else if($scope.subject_code==response.data.short_name){
						subjectField.borderColor('subcode');
					}else{
					$state.go('dashboard.deptmanagement.sub',{},{reload:true});
					}
				});
			}
		}
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.getDept=function(){
		$scope.listOfSearchdept=null;
		$scope.listOfSearchdept=[];
		var colgid={'colg_id':$scope.colgSearch.value,'stream_id':$scope.streamSearch.value};
	$http({
		method:'POST',
		url:"php/deptsubject/readDeptValue.php",
		data:colgid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.dept_name,'value':obj.dept_id};
			$scope.listOfSearchdept.push(data);
		});
	},function errorCallback(response) {
	});
	}
	$scope.getCourse=function(){
		$scope.noCourseTable=null;
		$scope.noCourseTable=[];
		var colgid={'dept_id':$scope.deptSearch.value,'id':$scope.colgSearch.value};
		$http({
			method:'POST',
			url:"php/deptsubject/getCourse.php",
			data:colgid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				data={'name':obj.course_name,'value':obj.course_id};
				$scope.noCourseTable.push(data);
			});
		},function errorCallback(response) {
		});
	}
	$http({
		method:'GET',
		url:"php/deptsubject/readDeptSubjectData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('res',response);
		$scope.subjectData=response.data;
	},function errorCallback(response) {
	});
	$scope.editSubjectData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/deptsubject/editDeptSubjectData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('colg_id',response.data[0].dept_id);
			$scope.colg_name.value=response.data[0].colg_id;
			$scope.selectEditStream(response.data[0].colg_id,response.data[0].stream_id);
			$scope.selectEditCourse(response.data[0].dept_id);
			$scope.selectEditdept(response.data[0].course_name,response.data[0].dept_id);
			//$scope.courseName.value=response.data[0].course_name;
			$scope.selectEditSemester(response.data[0].semester,response.data[0].course_name);
			//$scope.semester.value=response.data[0].semester;
			$scope.selectEditSubject(response.data[0].subject_type);
			//$scope.subtype.value=response.data[0].subject_type;
			$scope.subjectname=response.data[0].subject_name;
			$scope.subject_code=response.data[0].short_name;
			$scope.readcolg=true;
			$scope.readstrm=true;
			$scope.readdept=true;
			$scope.readcors=true;
			$scope.readsem=true;
			$scope.readsub=true;
			$scope.colgdis=true;
			$scope.strmdis=true;
			$scope.deptdis=true;
			$scope.corsdis=true;
			$scope.semdis=true;
			$scope.subdis=true;
			$scope.buttonName="Update";
			$scope.clearButtonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	
	$scope.selectEditCourse=function(value){
		console.log('value',value);
		var courseid={'id':$scope.colg_name.value};
		$scope.listOfdept=null;
		$scope.listOfdept=[{
		name:'Select your department',
		value:''
	    }]
	    $scope.dept_name=$scope.listOfdept[0];
		$http({
			method:'POST',
			url:"php/deptsubject/getDeptData.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.dept_name,'value':obj.dept_id};
				$scope.listOfdept.push(data);
				$scope.dept_name.value=value;
			});
		},function errorCallback(response) {
		})
	}
	$scope.selectEditdept=function(value,dept_id){
		$scope.noCourse=null;
		$scope.noCourse=[{
			name:'select your course',
			value:''
		}]
		$scope.courseName=$scope.noCourse[0];
		var courseid={'id':$scope.colg_name.value,'dept_id':dept_id};
		console.log('edit course',courseid,value);
		$http({
			method:'POST',
			url:"php/deptsubject/getCourse.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.course_name,'value':obj.course_id};
				$scope.noCourse.push(data);
				$scope.courseName.value=value;
			});
		},function errorCallback(response) {
		});
	}
	$scope.selectEditSemester=function(value,course_id){
		$scope.noSemesters=null;
		$scope.noSemesters=[{
			name:'select your semester',
			value:''
		}]
		$scope.semester=$scope.noSemesters[0];
		var semid={'colg_id':$scope.colg_name.value,'course_id':course_id};
		console.log('semester',semid);
		$http({
			method:'POST',
			url:"php/deptsubject/getSemester.php",
			data:semid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data, function(obj){
				var no_semester = obj.semester;
				var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
				for(var i = 0; i < no_semester; i++){
					var val = i+1;
					var roman_val = key[i];
					var seme = {'name':roman_val , 'value':val};
					$scope.noSemesters.push(seme);
					$scope.semester.value=value;
				}
			});
		},function errorCallback(response) {
		});
	}
	$scope.selectEditSubject=function(value){
		var courseid={'id':$scope.colg_name.value};
	$scope.noSubjectType=null;
		$scope.noSubjectType=[{
			name:'Select your subject type',
			value:''
		}]
		$scope.subtype=$scope.noSubjectType[0];
		console.log('get dept',courseid,value)
		$http({
			method:'POST',
			url:"php/deptsubject/getSubjectType.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.type,'value':obj.id};
				$scope.noSubjectType.push(data);
				$scope.subtype.value=value;
			});
		},function errorCallback(response) {
		});
	}
	$scope.deleteSubjectData=function(sid){
		id=sid;
		var userid={'id':id};
		var msg=$window.confirm('Are you want to sure this subject ?');
		if(msg){
		$http({
			method:'POST',
			url:"php/deptsubject/deleteDeptSubject.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('dashboard.deptmanagement.sub',{},{reload:true});
		},function errorCallback(response) {
			alert(response.data['msg']);
			$state.go('dashboard.deptmanagement.sub',{},{reload:true});
		});
		}
	}
	$scope.clearSubjectData=function(){
		$state.go('dashboard.deptmanagement.sub',{},{reload:true});
	}
	$scope.selectSubjectType=function(mod,id){
		subjectField.clearBorderColor(id);
	}
	$scope.removeBorder=function(id){
		subjectField.clearBorderColor(id);
	}
	$scope.GetDeptData=function(id,mod){
		if(mod.value!=''){
			subjectField.clearBorderColor(id);
		}
		var courseid={'colg_name':$scope.colg_name.value,'stream_id':$scope.stream_name.value}
		$scope.listOfdept=null;
		$scope.listOfdept=[{
		name:'Select your department',
		value:''
	    }]
	    $scope.dept_name=$scope.listOfdept[0];
		$http({
			method:'POST',
			url:"php/deptsubject/getDeptDataforSub.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.dept_name,'value':obj.dept_id};
				$scope.listOfdept.push(data);
			});
		},function errorCallback(response) {
		});
		$scope.noCourse=null;
		$scope.noCourse=[{
			name:'select your course',
			value:''
		}]
		$scope.courseName=$scope.noCourse[0];
		var courseid={'id':$scope.colg_name.value,'stream_id':$scope.stream_name.value};
		$http({
			method:'POST',
			url:"php/deptsubject/getCourse.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.course_name,'value':obj.course_id};
				$scope.noCourse.push(data);
			});
		},function errorCallback(response) {
		});
	}
	$scope.getStream=function(){
		$scope.listOfSearchStream=null;
		$scope.listOfSearchStream=[];
		var colgid={'colg_name':$scope.colgSearch.value};
		$http({
			method:'POST',
			url:"php/department/readStreamData.php",
			data:colgid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.stream_name,'value':obj.stream_id};
				$scope.listOfSearchStream.push(data);
			});
		},function errorCallback(response) {
		});
	}
	$scope.selectEditStream=function(value,streamid){
		$scope.listOfStream=null;
		$scope.listOfStream=[{
		name:'Select your stream',
		value:''
	    }]
		var colgid={'colg_name':value};
			$http({
				method:'POST',
				url:"php/department/readStreamData.php",
				data:colgid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data,function(obj){
					var data={'name':obj.stream_name,'value':obj.stream_id};
					$scope.listOfStream.push(data);
					$scope.stream_name.value=streamid;
				});
			},function errorCallback(response) {
			});
	    $scope.stream_name=$scope.listOfStream[0];
	}
});
deptsub.factory('subjectField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
var dashboard=angular.module('Channabasavashwara');
dashboard.controller('planController',function($scope,$http,$state,$window,$rootScope){
	//$scope.session="2015/16";
	//$scope.type;
	$scope.listOfCourseName=[{
		name: 'Select Couse Name',
		value: ''
	}]
	
	$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Session',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];	
	
	
	$scope.listPublish=[];
	$scope.listPublish=[{
		name: 'NO',
		value: '0'
	},{
		name: 'YES',
		value: '1'
	}]
	
	$scope.publish_name=$scope.listPublish[0];	
	
	
	$scope.listSection=[];
	$scope.listSection=[{
		name: 'Select Section',
		value: ''
	}]
	$scope.section_name=$scope.listSection[0];
	
	
	$scope.listSubject=[];
	$scope.listSubject=[{
		name: 'Select Subject',
		value: ''
	}]
	$scope.subject_name=$scope.listSubject[0];
	
	
	$scope.noSemesters=[];
	$scope.noSemesters = [{
    name :'Select Semester',
    value: ''
    }];	
	
	$scope.semester=$scope.noSemesters[0];
	
	
	$scope.listFaculty=[];
	$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
	$scope.faculty_name=$scope.listFaculty[0];	
	
	
	$http({
		method: 'GET',
		url: "php/plan/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	
	$http({
		method: 'GET',
		url: "php/plan/readFaculty.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.user_name);									
			var Faculty={'name':obj.role+" : "+obj.user_name , 'value':obj.user_id};
			$scope.listFaculty.push(Faculty);
		});
	},function errorCallback(response) {
		
	});
	
	
	/*
	$http({
		method: 'GET',
		url: "php/plan/getSubjectData.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.user_name);									
			var Faculty={'name':obj.role+" : "+obj.user_name , 'value':obj.user_id};
			$scope.listFaculty.push(Faculty);
		});
	},function errorCallback(response) {
		
	});
	
*/
	
	$http({
		method: 'GET',
		url: "php/plan/readSection.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.section_name);									
			var Section={'name':obj.section_name , 'value':obj.section_id};
			$scope.listSection.push(Section);
		});
	},function errorCallback(response) {
		
	});
	
	
	
	$http({
		method: 'GET',
		url: "php/plan/getCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var user={'name':obj.course_name,'value':obj.course_id};
			$scope.listOfCourseName.push(user);
		});
	},function errorCallback(response) {
	});
	
	$scope.course_name  = $scope.listOfCourseName[0];
	
	$scope.selectedCourse=function(){
		
		$scope.noSemesters=null;
		$scope.noSemesters=[];
		$scope.noSemesters = [{
    	name :'Select Semester',
    	value: ''
    	}];	
		
		var id = $scope.course_name.value;
		//alert("id::"+id);
		var userdata={'course_id':id};
		$http({
			method: 'POST',
			url: "php/subject/readSemester.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			var no_semester = obj.semester;
			//alert(""+no_semester);
			var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
			for(var i = 0; i < no_semester; i++)
			{
				var val = i+1;
				var roman_val = key[i];
				//alert("::"+val+":::"+roman_val);
				var seme = {'name':roman_val , 'value':val};
				$scope.noSemesters.push(seme);
			}
			//var semester={'name':obj.course_name , 'value':obj.course_id};
			//$scope.noSemesters.push(semester);
			
			});
			
			
			
		},function errorCallback(response) {
		});	
		
	}
	
	
	$scope.selectedSemester=function(){
		
		$scope.listSubject=[];
		$scope.listSubject=[{
			name: 'Select Subject',
			value: ''
		}]
		
		var course_id = $scope.course_name.value;
		var semester_id = $scope.semester.value;
		//alert("semester_id::"+semester_id);
		var userdata={'course_id':course_id,'semester_id':semester_id};
		$http({
			method: 'POST',
			url: "php/plan/getSubjectData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			//alert("::"+obj.section_name);									
			var Subject={'name':obj.subject_type+" : "+obj.subject_name , 'value':obj.subject_id};
			$scope.listSubject.push(Subject);
			
			});
			
			
			
		},function errorCallback(response) {
		});	
		
	}
	
	
	 $scope.noOfDays = [];
     $scope.listOfTimeData=[];
      
        $http({
			method: 'GET',
			url: "php/plan/gethour.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('hour',response.data);
			$scope.hours=response.data;
		},function errorCallback(response) {
			
		})
		$http({
			method: 'GET',
			url: "php/plan/getdays.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			 $scope.days=response.data;
			 //console.log('days',$scope.days);
			for (var i = 0; i < 5; i++) {
               $scope.noOfDays.push($scope.days[i]);
            }
		},function errorCallback(response) {
		});
		$scope.listOfSubjectName=[{
		name: 'Select Subject',
		value: ''
	    }]
		$scope.sub_name  = $scope.listOfSubjectName[0];
		
		$scope.listOfFacultyName=[{
		name: 'Select Faculty',
		value: ''
	    }]
		$scope.fac_name=$scope.listOfFacultyName[0];
		
		
		$scope.listOfVenueName=[{
		name: 'Select Venue',
		value: ''
	    }]
		$scope.venue_name  = $scope.listOfVenueName[0];
		
		
		
		
		
		////////////////////////////////////////////////////////////////check table data
		
		$scope.checkTableData=function(){
		
		if( $scope.subject_name.value != "" && $scope.faculty_name.value != "" && $scope.course_name.value != "" && $scope.semester.value != "" && $scope.session_name.value != "" && $scope.section_name.value != "" )
		{
			//alert("Get data");
			//var userdata={'course_id':$scope.course_name.value,'semester_id':$scope.semester.value,'session_id':$scope.session_name.value,'section_id':$scope.section_name.value};
			//alert("Check data::");
			
			var dataString = "faculty_id="+$scope.faculty_name.value+"&course_id="+$scope.course_name.value+"&semester_id="+$scope.semester.value+"&session_id="+$scope.session_name.value+"&section_id="+$scope.section_name.value+"&subject_id="+$scope.subject_name.value;
			//alert("Data:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/plan/getPlanData.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
				//alert("Received data:"+html);
				var dobj=jQuery.parseJSON(html);
				//alert(":::Subject CNT::"+dobj.subject_count);
				$scope.updateFields(dobj);
				
				//alert("::"+dobj.subject_record);
				//alert("::"+dobj.subject_record1);
			} 
			});
			
			
		
		}
		
				
		}
		
		
		
		
		$scope.addTimeTableData=function(){
			console.log('add time',$scope.listOfTimeData);
			for(var i=0;i<= $scope.listOfTimeData.length;i++){
				$http({
					method:'POST',
					url: "php/plan/addTimeTable.php",
					data:$scope.listOfTimeData[i],
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//alert(response.data['msg']);
				},function errorCallback(response) {
					//alert(response.data['msg']);
				});
			}
		}
		$scope.checkData=function(subname,facname,hour,day){
			//console.log('data',subname,facname,hour,day);
			if(subname.value==''){
				alert("select subject from list");
				$scope.fac_name=$scope.listOfFacultyName[0];
			}else{
				var subdata={'subtype':subname.value};
				$http({
					method:'POST',
					url: "php/plan/getSubjectType.php",
					data:subdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//console.log('TYPE',response);
					$rootScope.type=response.data[0].subject_type;
					var time={'sub_name':subname.value,'fac_name':facname.value,'hour':hour.id,'day':day.id,'sub_type':$rootScope.type};
			       $scope.listOfTimeData.push(time);
				},function errorCallback(response) {
				});
			}
			//console.log('data',$scope.listOfTimeData);
		}
		
		
		$scope.updateFields=function(dobj){
			$("#detailsstockid").html(dobj.data);
			
		}
		
		

		
		
});



	
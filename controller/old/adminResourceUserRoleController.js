var resource=angular.module('Channabasavashwara');
resource.controller('adminResourceUserRoleController',function($scope,$http,$window,$state,focusRoleField){
	$scope.buttonName="Add";
	var id='';
	$scope.listOfCollege=[{
		name:'Select college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'GET',
		url:"php/resource/readRoleData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.roleData=response.data;
	},function errorCallback(response) {
	});
	$scope.addUserRoleData=function(){
		if($scope.buttonName=="Add"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('Please select the college name');
				focusRoleField.borderColor('colg_name');
			}else if($scope.user_role==null || $scope.user_role==''){
				alert('User role field could not be blank');
				focusRoleField.borderColor('resourcerole');
			}else if($scope.user_type.value==null || $scope.user_type.value==''){
				alert('select the user type');
				focusRoleField.borderColor('utype');
			}else{
				var userdata={'colg_name':$scope.colg_name.value,'user_role':$scope.user_role,'type':$scope.user_type.value};
				$http({
					method:'POST',
					url:"php/resource/addResourceRoleData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//console.log(response);
					$scope.user_role=null;
					$scope.user_type.value='';
					alert(response.data['msg']);
					if($scope.roleData=='null'){
						$scope.roleData=[];
						$scope.roleData.push(response.data);
					}else{
						$scope.roleData.unshift(response.data);
					}
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.user_role==response.data.role){
						focusRoleField.borderColor('resourcerole');
					}else{
					//$state.go('dashboard.res.userrole',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('Please select the college name');
				focusRoleField.borderColor('colg_name');
			}else if($scope.user_role==null || $scope.user_role==''){
				alert('User role field could not be blank');
				focusRoleField.borderColor('resourcerole');
			}else if($scope.user_type.value==null || $scope.user_type.value==''){
				alert('select the user type');
				focusRoleField.borderColor('utype');
			}else{
				var updatedata={'colg_name':$scope.colg_name.value,'user_role':$scope.user_role,'type':$scope.user_type.value,'id':id};
				$http({
					method:'POST',
					url:"php/resource/updateResourceRoleData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.res.userrole',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.user_role==response.data.role){
						focusRoleField.borderColor('resourcerole');
					}else{
					//$state.go('dashboard.res.userrole',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			focusRoleField.clearBorderColor(id);
		}
	}
	$scope.clearField=function(id){
		focusRoleField.clearBorderColor(id);
	}
	$scope.editRoleData=function(rid){
		id=rid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/resource/editResourceRoleData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colg_name.value=response.data[0].colg_id;
			$scope.user_role=response.data[0].role;
			//$scope.getUserType(response.data[0].type);
			$scope.user_type.value=response.data[0].type;
			$scope.buttonName="Update";
			$scope.CancelbuttonName="Cancel";
			$scope.colgread=true;
			$scope.userread=true;
			$scope.colgdis=true;
			$scope.userdis=true;
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteRoleData=function(rid){
		id=rid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Role');
		if(conmesg){
			$http({
				method:'POST',
				url:"php/resource/deleteResourceRoleData.php",
				data:userid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				$state.go('dashboard.res.userrole',{}, { reload: true });
			},function errorCallback(response) {
				alert(response.data['msg']);
				//$state.go('dashboard.res.userrole',{}, { reload: true });
			});
		}
	}
	$scope.cancelUserRoleData=function(){
		$state.go('dashboard.res.userrole',{}, { reload: true });
	}
	$scope.listOftype=[{
		name:'Select user type',
		value:''
	}]
	$scope.user_type=$scope.listOftype[0];
	$http({
		method:'GET',
		url:"php/resource/getType.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.type_name,'value':obj.type_value};
			$scope.listOftype.push(data);
		});
	},function errorCallback(response) {
	})
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
	
});
resource.factory('focusRoleField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
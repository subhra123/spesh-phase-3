var hodsubject=angular.module('Channabasavashwara');
hodsubject.controller('hodSubjectController',function($state,$http,$scope,$window,subjectField){
	$scope.buttonName="Add";
	var id='';
	var streamid='';
	$scope.noCourse=null;
	$scope.noCourse=[{
			name:'Select Course',
			value:''
	}]
	$scope.courseName=$scope.noCourse[0];
	$scope.noSemesters=[{
			name:'select Semester',
			value:''
	}]
	$scope.semester=$scope.noSemesters[0];
	$scope.noSubjectType=[{
			name:'Select Subject Type',
			value:''
	}]
	$scope.subtype=$scope.noSubjectType[0];
	$scope.noCourseTable=[];
	$http({
		method:'GET',
		url:"php/stream/getStreamForHodSub.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('strm',response);
		streamid=response.data[0].stream_id;
		$scope.getCourse(streamid);
	},function errorCallback(response) {
	});
	$scope.getCourse=function(streamid){
	var strmid={'stream_id':streamid};
	//console.log('strmid',strmid);
	$http({
		method:'POST',
		url:"php/course/getCourseForHod.php",
		data:strmid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.course_name,'value':obj.course_id};
			$scope.noCourse.push(data);
			$scope.noCourseTable.push(data);
		});
	},function errorCallback(response) {
	});
	}
	$scope.selectSemester=function(id,mod){
		if(mod.value != ''){
			subjectField.clearBorderColor(id);
		}
		$scope.noSemesters=null;
		$scope.noSemesters=[{
			name:'select Semester',
			value:''
		}]
		
		$scope.semester=$scope.noSemesters[0];
		var semid={'course_id':$scope.courseName.value};
		$http({
			method:'POST',
			url:"php/course/getCollegeSemester.php",
			data:semid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data, function(obj){
				var no_semester = obj.semester;
				//alert("no_semester:"+no_semester);
				var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
				for(var i = 0; i < no_semester; i++){
					var val = i+1;
					var roman_val = key[i];
					var seme = {'name':roman_val , 'value':val};
					$scope.noSemesters.push(seme);
				}
			});
		},function errorCallback(response) {
		});
	}
	$http({
		method: 'GET',
		url:"php/college/getCollegeSubjectType.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
				var data={'name':obj.type,'value':obj.id};
				$scope.noSubjectType.push(data);
			});
		},function errorCallback(response) {
		
		});
	$scope.selectSubjectType=function(id,mod){
		if(mod.value !=''){
			subjectField.clearBorderColor(id);
		}
	}
	$http({
		method:'GET',
		url:"php/subject/readHodSubjectData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('res',response);
		$scope.subjectData=response.data;
	},function errorCallback(response) {
	});
	$scope.addSubjectData=function(){
		if($scope.courseName.value=='' || $scope.courseName.value==null){
				alert('Please select Course..');
				subjectField.borderColor('coursename');
				return;
			}else if($scope.semester.value=='' || $scope.semester.value==null){
				alert('Please select Semester..');
				subjectField.borderColor('sem');
				return;
			}else if($scope.subtype.value=='' || $scope.subtype.value==null){
				alert('Please select Subject Type... ');
				subjectField.borderColor('sub_type');
				return;
			}else if($scope.subjectname=='' || $scope.subjectname==null){
				alert('Subject name field could not be blank...');
				subjectField.borderColor('subname');
				return;
			}else if($scope.subject_code=='' || $scope.subject_code==null){
				alert('Subject code field could not be blank..');
				subjectField.borderColor('subcode');
				return;
			}else{
				if($scope.buttonName=="Add"){
					var adddata={'stream_id':streamid,'course_name':$scope.courseName.value,'semester':$scope.semester.value,'sub_type':$scope.subtype.value,'sub_name':$scope.subjectname,'sub_code':$scope.subject_code};
					$http({
						method:'POST',
						url:"php/subject/addHodSubjectData.php",
						data:adddata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						alert(response.data['msg']);
						$scope.subjectname=null;
						$scope.subject_code=null;
						if($scope.subjectData=='null'){
						$scope.subjectData=[];
						$scope.subjectData.push(response.data);
					}else{
						$scope.subjectData.unshift(response.data);
					}
					   //$state.go('hod.subject',{},{reload:true});
					},function errorCallback(response) {
						alert(response.data['msg']);
					if(response.data['type']== 1){
						subjectField.borderColor('subname');
					}else if(response.data['type']== 2){
						subjectField.borderColor('subcode');
					}else{
						//$state.go('dashboard.deptmanagement.sub',{},{reload:true});
					}
					});
				}
				if($scope.buttonName=="Update"){
					var updatedata={'stream_name':streamid,'course_name':$scope.courseName.value,'semester':$scope.semester.value,'sub_type':$scope.subtype.value,'sub_name':$scope.subjectname,'sub_code':$scope.subject_code,'id':id};
					//console.log('update',updatedata);
				$http({
					method:'POST',
					url:"php/subject/updateHodSubjectData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					 $state.go('hod.subject',{},{reload:true});
				},function errorCallback(response) {
					//console.log('err1',response);
					alert(response.data['msg']);
					if($scope.subjectname==response.data.subject_name){
						subjectField.borderColor('subname');
					}else if($scope.subject_code==response.data.short_name){
						subjectField.borderColor('subcode');
					}else{
					//$state.go('dashboard.deptmanagement.sub',{},{reload:true});
					}
				});
				}
			}
	}
	$scope.removeBorder=function(id){
		subjectField.clearBorderColor(id);
	}
	$scope.selectEditSemester=function(value,course_id){
		$scope.noSemesters=null;
		$scope.noSemesters=[{
			name:'select semester',
			value:''
		}]
		$scope.semester=$scope.noSemesters[0];
		var semid={'course_id':course_id};
		//console.log('semester',semid);
		$http({
			method:'POST',
			url:"php/course/getCollegeSemester.php",
			data:semid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data, function(obj){
				var no_semester = obj.semester;
				var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
				for(var i = 0; i < no_semester; i++){
					var val = i+1;
					var roman_val = key[i];
					var seme = {'name':roman_val , 'value':val};
					$scope.noSemesters.push(seme);
					
				}
			});
			$scope.semester.value=value;
		},function errorCallback(response) {
		});
	}
	$scope.editSubjectData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/subject/editDeptSubjectData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('colg_id',response.data[0].dept_id);
			$scope.courseName.value=response.data[0].course_name;
			$scope.selectEditSemester(response.data[0].semester,response.data[0].course_name);
			$scope.subtype.value=response.data[0].subject_type;
			$scope.subjectname=response.data[0].subject_name;
			$scope.subject_code=response.data[0].short_name;
			$scope.readcors=true;
			$scope.readsem=true;
			//$scope.readsub=true;
			$scope.corsdis=true;
			$scope.semdis=true;
			//$scope.subdis=true;
			$scope.buttonName="Update";
			$scope.clearButtonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteSubjectData=function(sid){
		id=sid;
		var userid={'id':id};
		var msg=$window.confirm('Are you  sure to delete this subject ?');
		if(msg){
		$http({
			method:'POST',
			url:"php/deptsubject/deleteDeptSubject.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('hod.subject',{},{reload:true});
		},function errorCallback(response) {
			alert(response.data['msg']);
			//$state.go('principal.dept.subject',{},{reload:true});
		});
		}
	}
	$scope.clearSubjectData=function(){
		$state.go('hod.subject',{},{reload:true});
	}
});
hodsubject.factory('subjectField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});

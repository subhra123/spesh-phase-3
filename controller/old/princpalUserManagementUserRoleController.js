var dashboard=angular.module('Channabasavashwara');
dashboard.controller('princpalUserManagementUserRoleController',function($scope,$http,$state,$window,roleField){
	$scope.buttonName="Add";
	$scope.cancelbuttonName="Cancel";
	var id='';
	var user_type='';
	$scope.listOfDept=[];
	$scope.listOfName=[];
	$scope.listOfRole=[];
	$scope.listOfDept=[{
		name: 'Department Name',
		value: ''
	}]
	$scope.deptName=$scope.listOfDept[0];
	$scope.listOfName=[{
		name: 'Select  Name',
		value: ''
	}]
	
	$scope.listOfRole=[{
		name: 'Select Role',
		value: ''
	}]
	$scope.user_role = $scope.listOfRole[0];
	$http({
		method: 'GET',
		url: "php/userrole/readCollegeRoleData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('read role',response);
		$scope.userRoleValue=response.data;
	},function errorCallback(response) {
	});
	$scope.user_name = $scope.listOfName[0];
	
	$http({
			method: 'GET',
			url: "php/userrole/getCollegeDeptData.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.showDept=true;
			$scope.userdept=false;
			$scope.deptdis=false;
			angular.forEach(response.data, function(obj){
			var dept={'name':obj.dept_name , 'value':obj.dept_id};
			     $scope.listOfDept.push(dept);
				 
		    });
		},function errorCallback(response) {
			
		});
	
	$scope.getUserRole=function(){
		roleField.clearBorderColor('userrole');
		var roleid={'id':$scope.user_role.value};
		$http({
			method:'POST',
			url:"php/userrole/getroleType.php",
			data:roleid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			var type=response.data[0].type;
			$scope.chooseDept(type)
		},function errorCallback(response) {
		});
		/*if($scope.user_role.value=='1'){
			$scope.showDept=false;
		}else{
			$scope.listOfDept=null;
			$scope.listOfDept=[{
		name: 'Department Name',
		value: ''
	}]
	$scope.deptName=$scope.listOfDept[0];
			var userid={'id':$scope.colg_name.value};
		$http({
			method: 'POST',
			url: "php/userrole/getDeptData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.showDept=true;
			angular.forEach(response.data, function(obj){
			var dept={'name':obj.dept_name , 'value':obj.dept_id};
			     $scope.listOfDept.push(dept);
		    });
		},function errorCallback(response) {
			
		});
		}*/
		
		
	/*if($scope.user_role.name=="PRINCPAL" || $scope.user_role.name=="princpal" ){
		console.log('if role of user',$scope.user_role.name);
		$scope.showDept=false;
	}else{
		console.log('else role of user',$scope.user_role.name);
		var userid={'id':$scope.colg_name.value};
		$http({
			method: 'POST',
			url: "php/userrole/getDeptData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.showDept=true;
			angular.forEach(response.data, function(obj){
			var dept={'name':obj.dept_name , 'value':obj.dept_id};
			     $scope.listOfDept.push(dept);
		    });
		},function errorCallback(response) {
			
		});
	}*/
	}
	$scope.getEditUserRole=function(type,value,userid){
		var roleid={'id':type};
		$http({
			method:'POST',
			url:"php/userrole/getroleType.php",
			data:roleid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			var type=response.data[0].type;
			$scope.chooseEditDept(type,value,userid)
		},function errorCallback(response) {
		});
		
	}
	$scope.chooseEditDept=function(type,value,userid){
		console.log('ed',userid);
		var user_id=userid;
		if(type=='1'){
			$scope.showDept=false;
			user_type=2;
		}else{
			$scope.listOfDept=null;
			$scope.listOfDept=[{
		name: 'Department Name',
		value: ''
	}]
	$scope.deptName=$scope.listOfDept[0];
		$http({
			method: 'GET',
			url: "php/userrole/getCollegeDeptData.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.showDept=true;
			$scope.userdept=false;
			$scope.deptdis=false;
			angular.forEach(response.data, function(obj){
			var dept={'name':obj.dept_name , 'value':obj.dept_id};
			     $scope.listOfDept.push(dept);
				 $scope.deptName.value=value;
		    });
		},function errorCallback(response) {
			
		});
		}
		if(type==2){
			user_type=3;
		}
		if(type==3){
			user_type=5;
		}
		 $scope.listOfName=null;
	 $scope.listOfName=[];
	 $scope.listOfName=[{
		name: 'Select  Name',
		value: ''
	}]
	$scope.user_name=$scope.listOfName[0];
	$http({
		method:'GET',
		url:"php/userrole/readCollegeuserdata.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('ures',response.data,userid);
		angular.forEach(response.data,function(obj){
			var data={'name':obj.user_name,'value':obj.user_id}
			$scope.listOfName.push(data);
			$scope.user_name.value=user_id;
		});
	},function errorCallback(response) {
	});
		
		
	}
	$scope.chooseDept=function(type){
		if(type=='1'){
			$scope.showDept=false;
			user_type=2;
		}/*else{
			$scope.userdept=false;
			$scope.deptdis=false;
			$scope.listOfDept=null;
			$scope.listOfDept=[{
		name: 'Department Name',
		value: ''
	}]
	$scope.deptName=$scope.listOfDept[0];
		$http({
			method: 'GET',
			url: "php/userrole/getCollegeDeptData.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.showDept=true;
			angular.forEach(response.data, function(obj){
			var dept={'name':obj.dept_name , 'value':obj.dept_id};
			     $scope.listOfDept.push(dept);
		    });
		},function errorCallback(response) {
			
		});
		}*/
		if(type==2){
			user_type=3;
		}
		if(type==3){
			user_type=5;
		}
	}
	
	
	
	$scope.setOtherDropDown=function(userid,roleid){
	//console.log('ids',id,userid,roleid);
		$scope.listOfRole=null;
		$scope.listOfRole=[];	
		$scope.listOfRole=[{
		name: 'Select Role',
		value: ''
	}]
	$scope.user_role = $scope.listOfRole[0];
	$http({
		method: 'GET',
		url: "php/userrole/getCollegeRole.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj1){
			var role={'name':obj1.role,'value':obj1.role_id};
			$scope.listOfRole.push(role);
			$scope.user_role.value=roleid;
		});
	},function errorCallback(response) {
		
	});
  }
	
	$scope.getUserRole123=function(name,clgid,dept_id){
		//alert(""+$scope.user_role.name);
	if(name=='HOD'){
		$scope.listOfDept=null;
		$scope.listOfDept=[];
		$scope.listOfDept=[{
		name: 'Department Name',
		value: ''
	}]
	$scope.deptName=$scope.listOfDept[0];
		var colgid={'id':clgid};
		$http({
			method: 'POST',
			url: "php/userrole/getDeptData.php",
			data:colgid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.showDept=true;
			angular.forEach(response.data, function(obj){
			var dept={'name':obj.dept_name , 'value':obj.dept_id};
			     $scope.listOfDept.push(dept);
				 $scope.deptName.value=dept_id;
		    });
		},function errorCallback(response) {
			
		});
	}else{
		$scope.showDept=false;
	}
	}
	
	$scope.addwithDept=function(){
		if($scope.user_name.value=='' || $scope.user_name.value==null){
			alert('select  name from list');
			roleField.borderColor('user_name');
		}else if($scope.deptName.value=='' || $scope.deptName.value==null){
			alert('select department from list');
			roleField.borderColor('deptname');
		}else if($scope.user_role.value==null || $scope.user_role.value==''){
			alert('select user role value');
			roleField.borderColor('userrole');
		}else{
			//alert("::"+$scope.user_name.value);
			//alert("::"+$scope.deptName.value);
			//alert("::"+$scope.user_role.value);
			var userdata={'user_id':$scope.user_name.value,'dept_id':$scope.deptName.value,'role_id':$scope.user_role.value,'user_type':user_type};
			$http({
				method: 'POST',
				url: "php/userrole/addCollegeRoleData.php",
				data: userdata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				//$scope.user_name.value='';
				//$scope.deptName.value='';
				//$scope.user_role=null;
				//$scope.userRoleValue.unshift(response.data);
				$state.go('principal.user.userrole',{}, { reload: true });
			},function errorCallback(response) {
				alert(response.data['msg']);
				if($scope.user_role.value==response.data.role_id){
					roleField.borderColor('userrole');
				}else{
					//$state.go('principal.user.userrole',{}, { reload: true });
				}
			});
		}
	};
	$scope.addwithOutDept=function(){
		if($scope.user_name.value=='' || $scope.user_name.value==null){
			alert('select  name from list');
			roleField.borderColor('user_name');
		}else if($scope.user_role.value==null || $scope.user_role.value==''){
			alert('select user role value');
			roleField.borderColor('userrole');
		}else{
			//alert("aaaaaaaaaaaa");
			//alert("::"+$scope.user_name.value);
			//alert("::"+$scope.deptName.value);
			//alert("::"+$scope.user_role.value);
			var userdata={'user_id':$scope.user_name.value,'dept_id':$scope.deptName.value,'role_id':$scope.user_role.value,'user_type':user_type};
			$http({
				method: 'POST',
				url: "php/userrole/addCollegeRoleData.php",
				data: userdata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('succ',response);
				alert(response.data['msg']);
				$scope.user_name.value='';
				$scope.user_role=null;
				$scope.userRoleValue.unshift(response.data);
				//$state.go('dashboard.user.role',{}, { reload: true });
			},function errorCallback(response) {
				//console.log('err',response);
				alert(response.data['msg']);
				if($scope.user_role.value==response.data.role_id){
					roleField.borderColor('userrole');
				}else{
					//$state.go('principal.user.userrole',{}, { reload: true });
				}
			});
		}
	};
	$scope.updatewithDept=function(){
		if($scope.user_name.value=='' || $scope.user_name.value==null){
			alert('select  name from list');
			roleField.borderColor('user_name');
		}else if($scope.deptName.value=='' || $scope.deptName.value==null){
			alert('select department from list');
			roleField.borderColor('deptname');
		}else if($scope.user_role.value==null || $scope.user_role.value==''){
			alert('select user role value');
			roleField.borderColor('userrole');
		}else{
			var updatedata={'user_id':$scope.user_name.value,'dept_id':$scope.deptName.value,'role_id':$scope.user_role.value,'user_type':user_type};
			//console.log('updatewithDept',updatedata);
			$http({
				method: 'POST',
				url: "php/userrole/updateCollegeRoleData.php",
				data: updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				//$scope.user_name.value='';
				//$scope.deptName.value='';
				//$scope.user_role=null;
				$state.go('principal.user.userrole',{}, { reload: true })
			},function errorCallback(response) {
				alert(response.data['msg']);
				if($scope.user_role.value==response.data.role_id){
					roleField.borderColor('userrole');
				}else{
					//$state.go('principal.user.userrole',{}, { reload: true });
				}
			});
		}
	}
	$scope.updatewithoutDept=function(){
		if($scope.user_name.value=='' || $scope.user_name.value==null){
			alert('select  name from list');
			roleField.borderColor('user_name');
		}else if($scope.user_role.value==null || $scope.user_role.value==''){
			alert('select user role value');
			roleField.borderColor('userrole');
		}else{
			var updatedata={'user_id':$scope.user_name.value,'dept_id':$scope.deptName.value,'role_id':$scope.user_role.value,'user_type':user_type};
			console.log('updatewithoutDept',updatedata);
			$http({
				method: 'POST',
				url: "php/userrole/updateCollegeRoleData.php",
				data: updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				//console.log('updated',response);
				alert(response.data['msg']);
				//$scope.user_name.value='';
				//$scope.deptName.value='';
				//$scope.user_role=null;
				$state.go('principal.user.userrole',{}, { reload: true });
			},function errorCallback(response) {
				alert(response.data['msg']);
				if($scope.user_role.value==response.data.role_id){
					roleField.borderColor('userrole');
				}else{
					//$state.go('principal.user.userrole',{}, { reload: true });
				}
			});
		}
	}
	$scope.addUserRoleData=function(){
		if($scope.buttonName=="Add"){
			if($scope.showDept){
				//console.log('display dept');
				$scope.addwithDept();
			}else{
				//console.log('can not display dept');
				$scope.addwithOutDept();
			}
		}
		if($scope.buttonName=='Update'){
			if($scope.showDept){
				//console.log('display dept');
				$scope.updatewithDept();
			}else{
				//console.log('can not display dept');
				$scope.updatewithoutDept();
			}
		}
	}
	
	$scope.cancelEdit=function(){
		$state.go('principal.user.userrole',{}, { reload: true });
	}
	$scope.editRoleData=function(rid){
		console.log('id f edit',rid);
		id=rid;
		var userdata={'userid':id};
		//alert(":::"+id);
		$http({
			method: 'POST',
			url: "php/userrole/editCollegeRoleData.php",
			data:userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('edited data',response);
			if(response.data[0].dept_name == null)
				response.data[0].dept_name = '';
			//alert(":"+response.data[0].dept_id+":");
			if(response.data[0].dept_name!=''){
				//$scope.user_name.value=response.data[0].user_id;
				//$scope.colg_name.value=response.data[0].colg_id;
				//$scope.setOtherDropDown($scope.colg_name.value,response.data[0].user_id,response.data[0].role_id);
				$scope.user_role.value=response.data[0].role_id;
				$scope.getEditUserRole(response.data[0].role_id,response.data[0].dept_id,response.data[0].user_id);
				//$scope.user_name.value=response.data[0].user_id;
				//$scope.user_role.value=response.data[0].role_id;
				//$scope.user_role.name=response.data[0].role;
				//$scope.setRole(response.data[0].colg_id,response.data[0].role_id);
				
				//$scope.getUserRole123(response.data[0].role,response.data[0].colg_id,response.data[0].dept_id);
				
				//$scope.deptName.name=response.data[0].dept_name;
			}else{
				$scope.showDept=false;
				$scope.setOtherDropDown(response.data[0].user_id,response.data[0].role_id);
				$scope.user_name.value=response.data[0].user_id;
				$scope.user_name.name=response.data[0].user_name;
				//$scope.user_role.name=response.data[0].role;
				//$scope.user_role.value=response.data[0].role_id;
			}
			$scope.buttonName="Update";
			$scope.userdept=true;
			$scope.userread=true;
			$scope.userdis=true;
			$scope.deptdis=true;
			$scope.cancelbuttonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteRoleData=function(rid){
		var deleteUser = $window.confirm('Are you sure want to delete user role?');
		if(deleteUser){
		id=rid;
		var userdata={'userid':id};
		$http({
			method: 'POST',
			url: "php/userrole/deleteCollegeRoleData.php",
			data:userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('delete',response);
			alert(response.data['msg']);
			$state.go('principal.user.userrole',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			$state.go('principal.user.userrole',{}, { reload: true });
		});
	}
	}
	$scope.clearField=function(id){
		roleField.clearBorderColor(id);
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
	
	
	//after edit button click
	
  $scope.clearBorder=function(id,mod){
	  if(mod.value!=''){
		  roleField.clearBorderColor(id);
	  }
  }
  $scope.setRole=function(id){
	  var userid={'colg_name':id};
	  $scope.listOfRole=null;
		$scope.listOfRole=[];	
		$scope.listOfRole=[{
		name: 'Select Role',
		value: ''
	}]
	$scope.user_role = $scope.listOfRole[0];
	$http({
		method: 'POST',
		url: "php/userrole/getRole.php",
		data:userid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj1){
			var role={'name':obj1.role,'value':obj1.role_id};
			$scope.listOfRole.push(role);
			
		});
	},function errorCallback(response) {
		
	});
  }




	$http({
		method: 'GET',
		url: "php/userrole/getCollegeUserData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj1){
			var user={'name':obj1.user_name,'value':obj1.user_id};
			$scope.listOfName.push(user);
		});
	},function errorCallback(response) {
		
	});
	$http({
		method: 'GET',
		url: "php/userrole/getCollegeRole.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj1){
			var role={'name':obj1.role,'value':obj1.role_id};
			$scope.listOfRole.push(role);
		});
	},function errorCallback(response) {
		
	});
	
	
	
});
dashboard.factory('roleField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
/*dashboard.directive('ngConfirmClick', [
  function() {
    return {
      link: function(scope, element, attr) {
        var msg = attr.ngConfirmClick || "Are you sure?";
        var clickAction = attr.ngClick;
        attr.ngClick = "";
        element.bind('click', function(event) {
          if (window.confirm(msg)) {
            scope.$eval(clickAction)
          }
        });
      }
    };
  }
])*/
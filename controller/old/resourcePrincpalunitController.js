var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('resourcePrincpalunitController',function($scope,$http,$state,$window,unitField){
	$scope.buttonName="Add";
	var id='';
	$http({
		method:'GET',
		url:"php/Role/readCollegeUnitData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.unitData=response.data;
	},function errorCallback(response) {
	});
	$scope.addUnitData=function(){
		if($scope.buttonName=="Add"){
			if($scope.unitname==null || $scope.unitname==''){
				alert('Unit field could not be blank...');
				unitField.borderColor('resourceunit');
			}else{
				var userdata={'unit':$scope.unitname};
				$http({
					method:'POST',
					url:"php/Role/addCollegeUnitData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$scope.unitname=null;
					$scope.unitData.unshift(response.data);
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.unitname==response.data.unit_name){
						unitField.borderColor('resourceunit');
					}else{
					$state.go('principal.resourse.unit',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.unitname==null || $scope.unitname==''){
				alert('Unit field could not be blank...');
				unitField.borderColor('resourceunit');
			}else{
				var updatedata={'unit':$scope.unitname,'id':id};
				$http({
					method:'POST',
					url:"php/Role/UpdateCollegeUnitData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.resourse.unit',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.unitname==response.data.unit_name){
						unitField.borderColor('resourceunit');
					}else{
					$state.go('principal.resourse.unit',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editUnitData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/Role/editCollegeUnitData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.unitname=response.data[0].unit_name;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteUnitData=function(sid){
		id=sid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Section');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/Role/deleteCollegeUnitData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data);
			$state.go('principal.resourse.unit',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data);
			$state.go('principal.resourse.unit',{}, { reload: true });
		});
		}
	}

	$scope.clearField=function(id){
		unitField.clearBorderColor(id);
	}
	$scope.cancelUnitData=function(){
		$state.go('principal.resourse.unit',{}, { reload: true });
	}
});
resourseclass.factory('unitField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('princpalResourceLecturePlanController',function($scope,$http,$state,$window,lecturePlanField){
	$scope.buttonName="Add";
	var id='';
	$http({
		method:'GET',
		url:"php/Role/readCollegeLecturePlanData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.lecturePlanData=response.data;
	},function errorCallback(response) {
	});
	$scope.addLecturePlanData=function(){
		if($scope.buttonName=="Add"){
			if($scope.lect_plan==null || $scope.lect_plan==''){
				alert('Please add Lecture Plan...');
				lecturePlanField.borderColor('resourcelectplan');
			}else{
				var userdata={'lect_plan':$scope.lect_plan};
				$http({
					method:'POST',
					url:"php/Role/addCollegeLecturePlanData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.resourse.lecturePlan',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.lect_plan==response.data.plan_name){
						lecturePlanField.borderColor('resourcelectplan');
					}else{
					//$state.go('dashboard.res.section',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.lect_plan==null || $scope.lect_plan==''){
				alert('Please add Lecture Plan...');
				lecturePlanField.borderColor('resourcelectplan');
			}else{
				var updatedata={'lect_plan':$scope.lect_plan,'id':id};
				$http({
					method:'POST',
					url:"php/Role/UpdateCollegeLecturePlanData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.resourse.lecturePlan',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.lect_plan==response.data.plan_name){
						lecturePlanField.borderColor('resourcelectplan');
					}else{
						//$state.go('dashboard.res.section',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editLecturePlanData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/Role/editCollegeLecturePlanData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.lect_plan=response.data[0].plan_name;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteLecturePlanData=function(sid){
		id=sid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Lecture Plan');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/Role/deleteCollegeLecturePlanData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('principal.resourse.lecturePlan',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			$state.go('principal.resourse.lecturePlan',{}, { reload: true });
		});
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			lecturePlanField.clearBorderColor(id);
		}
	}
	$scope.clearField=function(id){
		lecturePlanField.clearBorderColor(id);
	}
	$scope.cancelLecturePlanData=function(){
		$state.go('principal.resourse.lecturePlan',{}, { reload: true });
	}
	
});
resourseclass.factory('lecturePlanField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
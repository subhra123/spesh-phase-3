var addImage=angular.module('Spesh');
addImage.controller('adminCustomerAddImageController',function($http,$state,$scope,$window,$timeout,$filter,Upload){
    $scope.formDiv=true;
    $scope.checkedValue = [];
    $scope.reCheckedValue=[];
    $scope.totalImageArr=[];
    $scope.diffValue='';
    $scope.prevFromday='';
    $scope.buttonName="Add";
    var galFlag=false;
    var galImg=false;
    var galImgSave=false;
    $scope.localIMage=true;
    var typefind=0;
    var prevSubCatId=[];
    var prevChildIds=[];
    $scope.openGallery=function(){
       // console.log('sub',$scope.subcat);
        if($scope.example16model.length==0){
            alert('Please select the subcategory');
        }else{
          // console.log('checked gallery',$scope.checkedValue);
                var imageData=$.param({'action':'getGallImage','subcat_id':$scope.example16model});
                $http({
                    method:'POST',
                    url:"php/customerInfo.php",
                    data:imageData,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).then(function successCallback(response){
                   console.log('ressucc',response);
                    $scope.formDiv=false;
                    $scope.gallery=true;
                    //$scope.galComnt=true;
                   // $scope.galEDit="Edit";
                   // var str_array = response.data[0].image.split(',');
                   // console.log('ressucc',response.data.length==$scope.checkedValue.length);
                    $scope.galleryDatas=[];
                    var sflag=false;
                    if (galFlag==true) {
                      for(var j=0;j<$scope.example16model.length;j++){
                        if ($scope.example16model[j]['id']==prevSubCatId[0]['id']) {
                          sflag=true;
                          break;
                        }
                      }
                    }
                    var tempCheckedVar='';
                    tempCheckedVar=$scope.checkedValue;
                    for(var i=0;i<response.data.length;i++){
                      if(galFlag==true){
                        if (prevSubCatId.length > 0 && $scope.example16model.length > 0) {
                          if (prevSubCatId.length==$scope.example16model.length) {
                            if (prevSubCatId[0]['id']==$scope.example16model[0]['id']) {
                              if (response.data.length==$scope.checkedValue.length) {
                                if(response.data[i].image==$scope.checkedValue[i].image){
                                  var data={'gallery_id':response.data[i].  gallery_id,'subcat_id':response.data[i].subcat_id,'image':response.data[i].image,'description':$scope.checkedValue[i].description,'galComnt':true,'galEDit':"Edit","checked":true,'subcat_name':response.data[i].subcat_name};
                                  $scope.galleryDatas.push(data);
                                }
                              }else{
                                var data={'gallery_id':response.data[i].gallery_id,'subcat_id':response.data[i].subcat_id,'image':response.data[i].image,'description':response.data[i].description,'galComnt':true,'galEDit':"Edit",'subcat_name':response.data[i].subcat_name};
                                $scope.galleryDatas.push(data);
                                $scope.checkedValue.forEach(function(o,i){
                                  $scope.galleryDatas.forEach(function(g,i){
                                    if(o.image == g.image){
                                      g['checked'] = true;
                                      g['description'] = o.description;
                                    }
                                  })
                                })
                              }
                            }else{
                              var data={'gallery_id':response.data[i].gallery_id,'subcat_id':response.data[i].subcat_id,'image':response.data[i].image,'description':response.data[i].description,'galComnt':true,'galEDit':"Edit",'subcat_name':response.data[i].subcat_name};
                              $scope.galleryDatas.push(data);
                            }
                          }else{
                            if (sflag==true) {
                              if (tempCheckedVar.length > 0) {
                                for(var k=0;k < tempCheckedVar.length;k++){
                                  if (response.data[i].image==tempCheckedVar[k].image) {
                                    var data={'gallery_id':response.data[i].gallery_id,'subcat_id':response.data[i].subcat_id,'image':response.data[i].image,'description':tempCheckedVar[k].description,'galComnt':true,'galEDit':"Edit","checked":true,'subcat_name':response.data[i].subcat_name};
                                    $scope.galleryDatas.push(data);
                                    tempCheckedVar.splice(k,1);
                                  }
                                }
                              }else{
                                var data={'gallery_id':response.data[i].gallery_id,'subcat_id':response.data[i].subcat_id,'image':response.data[i].image,'description':response.data[i].description,'galComnt':true,'galEDit':"Edit",'subcat_name':response.data[i].subcat_name};
                                $scope.galleryDatas.push(data);
                              }
                            }else{
                              var data={'gallery_id':response.data[i].gallery_id,'subcat_id':response.data[i].subcat_id,'image':response.data[i].image,'description':response.data[i].description,'galComnt':true,'galEDit':"Edit",'subcat_name':response.data[i].subcat_name};
                              $scope.galleryDatas.push(data);
                            }
                            //console.log('sflag',$scope.checkedValue);
                          }
                        }
                      }else{
                        var data={'gallery_id':response.data[i].gallery_id,'subcat_id':response.data[i].subcat_id,'image':response.data[i].image,'description':response.data[i].description,'subcat_name':response.data[i].subcat_name,'galComnt':true,'galEDit':"Edit"};
                        $scope.galleryDatas.push(data);
                      }
                    }
                },function errorCallback(response) {
                  if(response.data['msg']==0){
                    alert('There are no images present in gallery under this subcategory');
                    $scope.formDiv=true;
                    $scope.gallery=false;
                    $scope.galleryDatas=[];
                  }
                })
        }
    }
    $scope.clearImage=function(){
      $scope.formDiv=true;
      $scope.gallery=false;
      if(galImgSave==false){
        $scope.checkedValue = [];
        galFlag=false;
      }
    }
    $scope.saveGalleryImage=function(){
        if($scope.reCheckedValue.length >0 && $scope.buttonName=="Add"){
            $scope.formDiv=true;
            $scope.gallery=false;
            galImgSave=true;
            $scope.checkedValue=$scope.reCheckedValue;
        }else if(($scope.checkedValue.length > 0 || $scope.reCheckedValue.length >0) && $scope.buttonName=="Update"){
            //console.log('saved value',$scope.checkedValue,$scope.galleryDatas);
            $scope.checkedValue=[];
            angular.forEach($scope.galleryDatas,function(obj){
                if(obj.checked===true){
                    var data={'checked':true,'description':obj.description,'gallery_image':true,'image':obj.image,'subcat_id':obj.subcat_id};
                    $scope.checkedValue.push(data);
                }
            })
             //console.log('changed value',$scope.checkedValue);
            $scope.formDiv=true;
            $scope.gallery=false;
            galImgSave=true;
        }else{
            alert('Please select images');
        }
    }
    $scope.setComment=function(index){
        $scope.galleryDatas[index].galEDit="Update";
    }
    $scope.editComment=function(galid,index,comnt,gl){
       // console.log('data',index,$scope.galleryDatas[index].galComnt);
       // $scope.galleryDatas[0].galComnt=false;
        if($scope.galleryDatas[index].galEDit=="Edit"){
            $scope.galleryDatas[index].galComnt=false;
           // console.log('read', $scope.galleryDatas);
        }
       if($scope.galleryDatas[index].galEDit=="Update"){
            $scope.galleryDatas[index].galComnt=true;
            /*var cData=$.param({'action':'setComment','gallery_id':galid,'comment':comnt});
            $http({
                method:'POST',
                url:'php/customerInfo.php',
                data:cData,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function successCallback(response){
                if(response.data['msg']==1){
                   $scope.galleryDatas[index].galComnt=true;
                   $scope.galleryDatas[index].galEDit="Edit";
                }
            },function errorCallback(response){
            })*/
        }
    }
   /* $scope.listOfRestaurant=[{
		name:'Select Business Name',
		value:''
	}]
	$scope.restaurant=$scope.listOfRestaurant[0];*/
    $scope.example14model = [];
    $scope.example14settings = {
        scrollableHeight: '200px',
        scrollable: true,
        enableSearch: true
    };
    $scope.listOfRestaurant=[];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=restaurant",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.rest_name,'value':obj.member_id};
			$scope.listOfRestaurant.push(data);
		})
	},function errorCallback(response) {
	})
    
    /*$scope.listOfSubcat=[{
		name:'Category:Subcategory',
		value:''
	}]
	$scope.subcat=$scope.listOfSubcat[0];*/
   $scope.listOfSubcat=[];
   /* $http({
        method:'GET',
        url:"php/customerInfo.php?action=getsubcategory",
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).then(function successCallback(response){
       //console.log('res',response.data);
        angular.forEach(response.data,function(obj){
            var data={'name':obj.cat_name+':'+obj.subcat_name,'value':obj.subcat_id};
			$scope.listOfSubcat.push(data);
		})
    },function errorCallback(response){
        
    })*/
    $scope.listOfDays=[{
		name:'Select Day',
		value:''
	}]
	$scope.daysFrom = angular.copy($scope.listOfDays[0]);
  $scope.daysTo = angular.copy($scope.listOfDays[0]);
  $http({
		method:'GET',
		url:"php/customerInfo.php?action=day",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
      var data={'name':obj.day_name,'value':obj.day_id};
      $scope.listOfDays.push(data);    
    })
	},function errorCallback(response) {
	})
  $scope.clickedRow=function(check,index){
    if($scope.checkedValue.length > 0){
      $scope.reCheckedValue=[];
      $scope.reCheckedValue=$scope.checkedValue;
    }
    if(check==true){
      $scope.reCheckedValue.push($scope.galleryDatas[index]);
      galFlag=true;
       // $scope.totalImageArr
    }else{
        var newIndex = $scope.reCheckedValue.map(function(e) { return e.image; }).indexOf($scope.galleryDatas[index].image);
        if(newIndex !== -1){
          $scope.reCheckedValue.splice(newIndex,1);
        }
    }
    console.log('check',$scope.reCheckedValue);
  }
   $scope.mulImage=[];
   $scope.mulImage.push({'image':null,'filename':'','comment':''});
   $scope.addNewImageRow=function(mulImage){
	   mulImage.push({'image':null,'filename':'','comment':''});
	  // console.log('add file',$scope.mulImage);
	   
   }
   $scope.deleteNewImageRow=function(mulImage,index){
	  mulImage.splice(index,1);
	   //console.log('file',$scope.mulImage);
   }
   $scope.onFileSelect1 = function(index) {
	     $scope.mulImage[index]['filename']='';
         galImg=true;
        // if($scope.browser=='Safari'){
             console.log('file',$scope.mulImage,galImg);
        // }
   }
   $http({
       method:'GET',
       url:'php/customerInfo.php?action=getGallerySpecialImage',
       headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
   }).then(function successCallback(response){
       console.log('res',response.data);
        if(response.data.length >0){
            $scope.listOfGallerySpecialImage=response.data;
        }
        if(response.data=='null'){
            $scope.listOfGallerySpecialImage=[];
           // console.log('res',$scope.listOfGallery);
        }
   },function errorCallback(response) {
   })
   $scope.validateImages=function(){
	   var flag;
	   if($scope.mulImage.length >0){
		   for(var i=0;i<$scope.mulImage.length;i++){
			   if($scope.mulImage[i]['image']==null && $scope.mulImage[i]['filename']==''){
				   alert('Please select image'+(i+1));
				   var flag=false;
				   return;
			   }else{
				   flag=true;
			   }
		  }
		  return flag;
	   }
   }
   $scope.sortingSpecial=function(json_object, key_to_sort_by){
      function sortByKey(a, b) {
          var x = a[key_to_sort_by];
          var y = b[key_to_sort_by];
          return ((x < y) ? -1 : ((x > y) ? 1 : 0));
      }

      json_object.sort(sortByKey);
   }
   $scope.example16model = [];
    $scope.example16settings = {
        scrollableHeight: '200px',
        scrollable: true,
        enableSearch: true
    };
   $scope.getDayFromSpecial=function(){
       var restdata=$.param({'rest_name':$scope.restaurant.value,'action':'mulspecialsub'});
       $http({
           method:'POST',
           url:'php/customerInfo.php',
           data:restdata,
           headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
       }).then(function successCallback(response){
            console.log('sub res',response.data);
            $scope.listOfSubcat=[];
            $scope.example16data=[];
            angular.forEach(response.data,function(obj){
               if(obj.type==2){
                    typefind=1;
               }else{
                    typefind=0;
               }
               // var data={'name':obj.cat_name+':'+obj.subcat_name,'value':obj.subcat_id};
               // $scope.listOfSubcat.push(data);
               var data={'label':obj.cat_name+':'+obj.subcat_name,'id':obj.subcat_id};
               $scope.example16data.push(data);
		        })
            $scope.sortingSpecial($scope.example16data, 'label');
        if(typefind==1){
            $scope.localIMage=false;
        }else{
          $scope.localIMage=true;  
        }
       },function errorCallback(response) {
       })
       var childrest=$.param({'rest_name':$scope.restaurant.value,'action':'childrest'});
       $http({
           method:'POST',
           url:'php/customerInfo.php',
           data:childrest,
           headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
       }).then(function successCallback(response){
           //console.log('child',response.data.result);
           if(response.data.isExist==1){
               $scope.example14data = [];
               angular.forEach(response.data.result,function(obj){
                   var data={'label':obj.child_rest_name,'id':obj.child_member_id};
                   $scope.example14data.push(data);
               })
                $scope.showchild=true;
              // console.log('examl',$scope.example14model);
           }else{
               $scope.showchild=false;
           }
       },function errorCallback(response) {
       })
   }
   $scope.example2settings = {
        displayProp: 'id'
    };
   $scope.addGalleryImageDetails=function(billdata){
        //var nDate = new Date($scope.date1); 
       // var today = new Date();
      // var nDate = new Date($scope.date1);
      // var kDate = new Date($scope.date2);
       //console.log('date',nDate >= kDate);
       if(billdata.$valid){
           //var nDate = new Date($scope.date1); 
          // var today = new Date();
           var flag;
           if($scope.restaurant==undefined){
               alert('Please select the business name');
           }/*else if($scope.subcat.value=='' || $scope.subcat.value==null){
               alert('Please select the subcategory value');
           }*/else if($scope.example16model.length==0){
               alert('Please select the subcategory value');
           }else if($scope.daysFrom.value==null || $scope.daysFrom.value==''){
               alert('Please select a day');
           }else if($scope.daysTo.value==null || $scope.daysTo.value==''){
               alert('Please select a day');
           }else{
             //  flag=$scope.validateImages();
             var dComment='';
               var sibBusiness=[];
               if($scope.example14model.length > 0){
                   sibBusiness=$scope.example14model;
               }else{
                   sibBusiness=[];
               }
               var subcatArr=[];
               if ($scope.example16model.length > 0) {
                  subcatArr=$scope.example16model;
               }else{
                  subcatArr=[];
               }
              //console.log('subcatArr',$scope.checkedValue);
             /*  if($scope.date1 !='' || $scope.date1 !=null){
                   var nDate = new Date($scope.date1); 
                   var today = new Date();
                   if(today >= nDate){
                       flag=false;
                       alert('Date From should be tomorrow onwards');
                       return;
                   }else{
                       flag=true;
                   }
               }else{
                   flag=true;
               }
               if($scope.date2 !='' || $scope.date2 !=null){
                  var nDate = new Date($scope.date2); 
                   var today = new Date();
                   if(today >= nDate){
                       flag=false;
                       alert('Date To should be tomorrow onwards');
                       return;
                   }else{
                       flag=true;
                   } 
               }else{
                   flag=true;
               }*/
               if(($scope.date1 =='' && $scope.date2 =='') || ($scope.date1 == null && $scope.date2 ==null)){
                  flag=true;
               }else{
                 if(($scope.date1 !='' || $scope.date1 !=null) && ($scope.date2 =='' || $scope.date2 ==null)){
                    alert("Please add the To Date");
                    flag=false;
                    return;
                   }else{
                     flag=true;
                   }
                   if(($scope.date1 =='' || $scope.date1 ==null) && ($scope.date2 !='' || $scope.date2 !=null)){
                    alert("Please add the From Date");
                    flag=false;
                    return;
                   }else{
                     flag=true;
                   }
               }
               if(($scope.date1 !='' || $scope.date1 !=null) && ($scope.date2 !='' || $scope.date2 !=null)){
                  var nDate = new Date($scope.date1);
                  var kDate = new Date($scope.date2);
                   if(nDate >= kDate){
                       flag=false;
                       alert('To Date should not be before From Date');
                       return;
                   }else{
                       flag=true;
                   }
               }else{
                   flag=true;
               }
               if($scope.dcomment=='' || $scope.dcomment==null){
                dComment='';
               }else{
                dComment=$scope.dcomment;
               }
             if(flag==true){
                   if($scope.buttonName=='Add'){
                        if($scope.checkedValue.length >0 && $scope.mulImage[0].image !=null){
                          for(var i=0;i<$scope.checkedValue.length;i++){
                            var data={'image':$scope.checkedValue[i].image,'comment':$scope.checkedValue[i].description,'gallery_image':$scope.checkedValue[i].checked,'subcat_id':$scope.checkedValue[i].subcat_id};
                            $scope.totalImageArr.push(data);
                          }
                          for(var i=0;i<$scope.mulImage.length;i++){
                            var data={'image':$scope.mulImage[i].image,'comment':$scope.mulImage[i].comment,'gallery_image':false,'subcat_id':''};
                            $scope.totalImageArr.push(data);
                          }
                        }else if($scope.checkedValue.length >0 && $scope.mulImage[0].image ==null){
                          for(var i=0;i<$scope.checkedValue.length;i++){
                            var data={'image':$scope.checkedValue[i].image,'comment':$scope.checkedValue[i].description,'gallery_image':$scope.checkedValue[i].checked,'subcat_id':$scope.checkedValue[i].subcat_id};
                            $scope.totalImageArr.push(data);
                          }
                        }else{
                          for(var i=0;i<$scope.mulImage.length;i++){
                            var data={'image':$scope.mulImage[i].image,'comment':$scope.mulImage[i].comment,'gallery_image':false,'subcat_id':''};
                            $scope.totalImageArr.push(data);
                          }
                        }
                       if($scope.totalImageArr.length>0){
                           var imageString='';
						               var arrImage=[];
						               var imagearr=[];
                           var uploadArrImage=[];
                           if($scope.totalImageArr[0].image !=null || $scope.totalImageArr[0].image !=''){
                                for(var i=0;i<$scope.totalImageArr.length;i++){
                                  if($scope.totalImageArr[i]['image']!=null){
                                      var newmulpath='';
                                      var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
                                      if(typeof($scope.totalImageArr[i]['image'])=='object'){
									                      newmulpath=today+"_"+ $scope.totalImageArr[i]['image'].name;
                                      }else{
                                        newmulpath=$scope.totalImageArr[i]['image'];
                                      }
									//$scope.mulImage[i]['image'].name=newmulpath;
                  										$scope.totalImageArr[i]['image']=Upload.rename($scope.totalImageArr[i]['image'], newmulpath);
                  										arrImage.push({'image':$scope.totalImageArr[i]['image']});
                  										imagearr.push({'imagename':newmulpath,'comnt':$scope.totalImageArr[i]['comment'],'gallery_image':$scope.totalImageArr[i]['gallery_image'],'subcat_id':$scope.totalImageArr[i]['subcat_id']});
                  										if(i==0){
                  											imageString=newmulpath;
                  										}else{
                  											imageString +=","+newmulpath;
                  										}
                                  }else{
                                      var newmulpath='';
                  										newmulpath=$scope.totalImageArr[i]['filename'];
                  										imagearr.push({'imagename':newmulpath,'comnt':$scope.totalImageArr[i]['comment'],'gallery_image':$scope.totalImageArr[i]['gallery_image'],'subcat_id':$scope.totalImageArr[i]['subcat_id']});
                  										if(i==0){
                  											imageString=newmulpath;
                  										}else{
                  											imageString +=","+newmulpath;
                  										}
                                  }
                                    //console.log('arrtype',(typeof(arrImage[i]['image'])=='object'));
                                }
                                console.log('arrImage',imageString);
                                console.log('Imagearr',imagearr);
                                for(var i=0;i<arrImage.length;i++){
                                  if(typeof(arrImage[i]['image'])=='object'){
                                    var data={'image':arrImage[i]['image']};
                                    uploadArrImage.push(data);
                                  }
                                }
                                if(uploadArrImage.length>0){
                                     $scope.upload=Upload.upload({
                                         url: 'php/uploadAll.php',
                                         method: 'POST',
                                         file: uploadArrImage
                                     }).success(function(data, status, headers, config) {
                                         var datefrom='';
                                         var dateto='';
                                         if($scope.daysFrom.value=='1' && $scope.daysTo.value=='7'){
                                              var difference=7;
                                         }else{
                                             // var difference=Math.abs((parseInt($scope.daysTo.value)-parseInt($scope.daysFrom.value))%7)+1;
                                             // var difference=Math.abs((parseInt(parseInt($scope.daysTo.value)/7))*7 + (parseInt($scope.daysTo.value)%7) )-((parseInt($scope.daysFrom.value)%7) )+1;
                                             var difference = Math.abs((parseInt($scope.daysTo.value)) - (parseInt($scope.daysFrom.value))) + 1;
                                         }
                                         if($scope.date1==null || $scope.date1==''){
                                             datefrom='';
                                         }else{
                                             datefrom=$scope.date1;
                                         }
                                         if($scope.date2==null || $scope.date2==''){
                                             dateto='';
                                         }else{
                                             dateto=$scope.date2;
                                         }
                                         var imageData=$.param({'action':'specialImagesGalleryAdd','restaurant':$scope.restaurant.value,'from_dayid':$scope.daysFrom.value,'to_dayid':$scope.daysTo.value,'subcat_id':subcatArr,'multiple_image':imageString,'imageArr':imagearr,'act':'add','diff':difference,'date_from':datefrom,'date_to':dateto,'sibling':sibBusiness,'dComment':dComment});
                                        // console.log('sentData',imageData);
                                         $http({
                                             method:'POST',
                                             url:'php/customerInfo.php',
                                             data:imageData,
                                             headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                         }).then(function successCallback(response){
                                             console.log('succe',response.data);
                                             alert(response.data['msg']);
										                         $state.go('dashboard.customer.addimage',{}, { reload: true });
                                         },function errorCallback(response) {
                                              console.log('err',response.data);
                                              alert(response.data['msg']);
                                         })
                                         
                                     }).error(function(data,status){
                                     })
                                }else{
                                     var datefrom='';
                                     var dateto='';
                                     if($scope.daysFrom.value=='1' && $scope.daysTo.value=='7'){
                                          var difference=7;
                                     }else{
                                          var difference = Math.abs((parseInt($scope.daysTo.value)) - (parseInt($scope.daysFrom.value))) + 1;
                                     }
                                     if($scope.date1==null || $scope.date1==''){
                                         datefrom='';
                                     }else{
                                         datefrom=$scope.date1;
                                     }
                                     if($scope.date2==null || $scope.date2==''){
                                         dateto='';
                                     }else{
                                         dateto=$scope.date2;
                                     }
                                     var imageData=$.param({'action':'specialImagesGalleryAdd','restaurant':$scope.restaurant.value,'from_dayid':$scope.daysFrom.value,'to_dayid':$scope.daysTo.value,'subcat_id':subcatArr,'multiple_image':imageString,'imageArr':imagearr,'act':'add','diff':difference,'date_from':datefrom,'date_to':dateto,'sibling':sibBusiness,'dComment':dComment});
                                    // console.log('sentData',imageData);
                                     $http({
                                         method:'POST',
                                         url:'php/customerInfo.php',
                                         data:imageData,
                                         headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                     }).then(function successCallback(response){
                                         console.log('succe',response.data);
                                         alert(response.data['msg']);
                                         $state.go('dashboard.customer.addimage',{}, { reload: true });
                                     },function errorCallback(response) {
                                          console.log('err',response.data);
                                          alert(response.data['msg']);
                                     })
                                }
                           }
                       }
                   }//add section end
                   if($scope.buttonName=='Update'){
                     // console.log('update flag',$scope.checkedValue,galImg);
                     var chkFlag=false;
                      for (var k = 0; k < $scope.example16model.length; k++) {
                        if (prevSubCatId[0]['id']==$scope.example16model[0]['id']) {
                          chkFlag=true;
                          break;
                        }
                      }
                      if (prevSubCatId.length==$scope.example16model.length) {
                        if (prevSubCatId[0]['id']==$scope.example16model[0]['id']) {
                          prevSubId=[];
                        }else{
                          prevSubId=prevSubCatId;
                          if ($scope.checkedValue.length >0) {
                            for(var i=0;i<$scope.checkedValue.length;i++){
                              if ($scope.checkedValue[i].subcat_id==prevSubCatId[0]['id']) {
                                $scope.checkedValue.splice(i,1);
                              }
                            }
                          }
                        }
                      }else{
                        prevSubId=prevSubCatId;
                        if (chkFlag==false) {
                          if ($scope.checkedValue.length >0) {
                            for(var i=0;i<$scope.checkedValue.length;i++){
                              if ($scope.checkedValue[i].subcat_id==prevSubCatId[0]['id']) {
                                $scope.checkedValue.splice(i,1);
                              }
                            }
                          }
                        }
                      }
                       if(prevChildIds.length >0){
                           prevchildid=prevChildIds;
                       }else{
                            prevchildid=[];
                       }
                       //console.log('update prev child', prevChildIds);
                       //console.log('checked value', $scope.checkedValue);
                       if($scope.checkedValue.length >0 && galImg==true){
                           for(var i=0;i<$scope.checkedValue.length;i++){
                               var data={'image':$scope.checkedValue[i].image,'comment':$scope.checkedValue[i].description,'gallery_image':$scope.checkedValue[i].checked,'subcat_id':$scope.checkedValue[i].subcat_id};
                               $scope.totalImageArr.push(data);
                           }
                           for(var i=0;i<$scope.mulImage.length;i++){
                               if($scope.mulImage[i]['image']!=null){
                                   var data={'image':$scope.mulImage[i].image,'comment':$scope.mulImage[i].comment,'gallery_image':false,'subcat_id':''};
                                   $scope.totalImageArr.push(data);
                               }else{
                                   var data={'image':$scope.mulImage[i]['filename'],'comment':$scope.mulImage[i].comment,'gallery_image':false,'subcat_id':''};
                                   $scope.totalImageArr.push(data);
                               }
                           }
                       }else if($scope.checkedValue.length >0 && galImg==false){
                           for(var i=0;i<$scope.checkedValue.length;i++){
                               var data={'image':$scope.checkedValue[i].image,'comment':$scope.checkedValue[i].description,'gallery_image':$scope.checkedValue[i].checked,'subcat_id':$scope.checkedValue[i].subcat_id};
                               $scope.totalImageArr.push(data);
                           }
                       }else{
                            for(var i=0;i<$scope.mulImage.length;i++){
                               if($scope.mulImage[i]['image']!=null){
                                   var data={'image':$scope.mulImage[i].image,'comment':$scope.mulImage[i].comment,'gallery_image':false,'subcat_id':''};
                                   $scope.totalImageArr.push(data);
                               }else{
                                   var data={'image':$scope.mulImage[i]['filename'],'comment':$scope.mulImage[i].comment,'gallery_image':false,'subcat_id':''};
                                   $scope.totalImageArr.push(data);
                               }
                           }
                       }
                       console.log('total image', $scope.totalImageArr);
                       if($scope.totalImageArr.length>0){
                          var imageString='';
						              var arrImage=[];
						              var imagearr=[];
                           var uploadArrImage=[];
                           if($scope.totalImageArr[0].image !=null || $scope.totalImageArr[0].image !=''){
                               for(var i=0;i<$scope.totalImageArr.length;i++){
                                    if($scope.totalImageArr[i]['image']!=null){
                                        var newmulpath='';
                                        var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
                                        if(typeof($scope.totalImageArr[i]['image'])=='object'){
										                  newmulpath=today+"_"+ $scope.totalImageArr[i]['image'].name;
                                        }else{
                                          newmulpath=$scope.totalImageArr[i]['image'];
                                        }
										//$scope.mulImage[i]['image'].name=newmulpath;
										$scope.totalImageArr[i]['image']=Upload.rename($scope.totalImageArr[i]['image'], newmulpath);
										arrImage.push({'image':$scope.totalImageArr[i]['image']});
										imagearr.push({'imagename':newmulpath,'comnt':$scope.totalImageArr[i]['comment'],'gallery_image':$scope.totalImageArr[i]['gallery_image'],'subcat_id':$scope.totalImageArr[i]['subcat_id']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
                                    }else{
                                        var newmulpath='';
										newmulpath=$scope.totalImageArr[i]['filename'];
										imagearr.push({'imagename':newmulpath,'comnt':$scope.totalImageArr[i]['comment'],'gallery_image':$scope.totalImageArr[i]['gallery_image'],'subcat_id':$scope.totalImageArr[i]['subcat_id']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
                                    }
                                    //console.log('arrtype',(typeof(arrImage[i]['image'])=='object'));
                               }
                               for(var i=0;i<arrImage.length;i++){
                                   if(typeof(arrImage[i]['image'])=='object'){
                                       var data={'image':arrImage[i]['image']};
                                       uploadArrImage.push(data);
                                   }
                               }
                               if(uploadArrImage.length>0){
                                   $scope.upload=Upload.upload({
                                       url: 'php/uploadAll.php',
                                       method: 'POST',
                                       file: uploadArrImage
                                   }).success(function(data, status, headers, config) {
                                       var datefrom='';
                                       var dateto='';
                                      if($scope.daysFrom.value=='1' && $scope.daysTo.value=='7'){
                                              var difference=7;
                                         }else{
                                               var difference = Math.abs((parseInt($scope.daysTo.value)) - (parseInt($scope.daysFrom.value))) + 1;
                                             // var difference=Math.abs((parseInt($scope.daysTo.value)-(parseInt($scope.daysFrom.value))+1;
                                         }
                                      if($scope.date1==null || $scope.date1==''){
                                         datefrom='';
                                      }else{
                                         datefrom=$scope.date1;
                                      }
                                      if($scope.date2==null || $scope.date2==''){
                                         dateto='';
                                      }else{
                                         dateto=$scope.date2;
                                      }
                                        console.log('imgarr,imageString',imagearr,imageString);
                                      var imageUpdateData=$.param({'action':'specialImagesGalleryAdd','restaurant':$scope.restaurant.value,'from_dayid':$scope.daysFrom.value,'to_dayid':$scope.daysTo.value,'subcat_id':subcatArr,'multiple_image':imageString,'imageArr':imagearr,'act':'update','diff':difference,'date_from':datefrom,'date_to':dateto,'diffValue': $scope.diffValue,'prevFromDay':$scope.prevFromday,'sibling':sibBusiness,'prevSub':prevSubId,'prevChildIds':prevchildid,'dComment':dComment});
                                       $http({
                                             method:'POST',
                                             url:'php/customerInfo.php',
                                             data:imageUpdateData,
                                             headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                         }).then(function successCallback(response){
                                             console.log('succeup',response.data);
                                             alert(response.data['msg']);
										                         $state.go('dashboard.customer.addimage',{}, { reload: true });
                                         },function errorCallback(response) {
                                              console.log('errup',response.data);
                                              alert(response.data['msg']);
                                         })
                                   }).error(function(data,status){
                                   })
                               }else{
                                   var datefrom='';
                                   var dateto='';
                                   if($scope.daysFrom.value=='1' && $scope.daysTo.value=='7'){
                                              var difference=7;
                                         }else{
                                              var difference = Math.abs((parseInt($scope.daysTo.value)) - (parseInt($scope.daysFrom.value)))+1;
                                         }
                                  if($scope.date1==null || $scope.date1==''){
                                     datefrom='';
                                  }else{
                                     datefrom=$scope.date1;
                                  }
                                  if($scope.date2==null || $scope.date2==''){
                                     dateto='';
                                  }else{
                                     dateto=$scope.date2;
                                  }
                                   // console.log('diffvalue',$scope.diffValue);
                                  var imageUpdateData=$.param({'action':'specialImagesGalleryAdd','restaurant':$scope.restaurant.value,'from_dayid':$scope.daysFrom.value,'to_dayid':$scope.daysTo.value,'subcat_id':subcatArr,'multiple_image':imageString,'imageArr':imagearr,'act':'update','diff':difference,'date_from':datefrom,'date_to':dateto,'diffValue':$scope.diffValue,'prevFromDay':$scope.prevFromday,'sibling':sibBusiness,'prevSub':prevSubId,'prevChildIds':prevchildid,'dComment':dComment});
                                   console.log('imageUpdateData',sibBusiness,prevchildid);
                                   $http({
                                         method:'POST',
                                         url:'php/customerInfo.php',
                                         data:imageUpdateData,
                                         headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                                     }).then(function successCallback(response){
                                         console.log('succeup',response.data);
                                         alert(response.data['msg']);
                                         $state.go('dashboard.customer.addimage',{}, { reload: true });
                                     },function errorCallback(response) {
                                          console.log('errup',response.data);
                                          alert(response.data['msg']);
                                     })
                               }
                               
                           }
                       }
                   }//update section end
               }
           }
       }
   }
   $scope.viewAllSpecialImages=function(subcatid,memberid){
        var viewData=$.param({'action':'getSpecialImage','subcat_id':subcatid,'member_id':memberid});
        $http({
            method:'POST',
            url:'php/customerInfo.php',
            data:viewData,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function successCallback(response){
            console.log('view',response.data);
            $scope.stepsSpecialModel=[];
            response.data.forEach(function (a) {
                if (!this[a.day_id]) {
                    this[a.day_id] = {day_id:a.day_id, day_name:a.day_name, special:[]};
                     $scope.stepsSpecialModel.push(this[a.day_id]);
                }
                this[a.day_id].special.push({ image: a.image });
            }, Object.create(null));
          //  console.log('output',$scope.stepsSpecialModel);
           /* for(var i=0;i<response.data.length;i++){
                if($scope.stepsSpecialModel.length > 0){
                    for(var j=0;j<$scope.stepsSpecialModel.length;j++){
                        if($scope.stepsSpecialModel[j]['day_id']==response.data[i]['day_id']){
                            var data={'image':response.data[i]['image']};
                             $scope.stepsSpecialModel[j]['special'].push(data);
                        }else{
                            var data={'image':response.data[i]['image']};
                            specImage.push(data);
                            var data1={'day_id':response.data[i]['day_id'],'day_name':response.data[i]['day_name'],'special':specImage};
                            $scope.stepsSpecialModel.push(data1);
                        }
                    }
                }else{
                    var data={'image':response.data[i]['image']};
                    specImage.push(data);
                    var data1={'day_id':response.data[i]['day_id'],'day_name':response.data[i]['day_name'],'special':specImage};
                    $scope.stepsSpecialModel.push(data1);
                }
            }*/
            $scope.showModal = !$scope.showModal;
        },function errorCallback(response) {
        })
   }
   $scope.getPeerBusinessList=function(memid,isPeer){
      // console.log('list',memid,isPeer);
       var restdata=$.param({'rest_name':memid,'action':'mulspecialsub'});
       $http({
           method:'POST',
           url:'php/customerInfo.php',
           data:restdata,
           headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
       }).then(function successCallback(response){
           console.log('sub res',response.data);
           $scope.listOfSubcat=[];
           angular.forEach(response.data,function(obj){
               if(obj.type==2){
                    typefind=1;
               }else{
                    typefind=0;
               }
                var data={'name':obj.cat_name+':'+obj.subcat_name,'value':obj.subcat_id};
                $scope.listOfSubcat.push(data);
		  })
        if(typefind==1){
            $scope.localIMage=false;
        }else{
          $scope.localIMage=true;  
        }
       },function errorCallback(response) {
       })
           var childrest=$.param({'rest_name':memid,'action':'childrest'});
           $http({
               method:'POST',
               url:'php/customerInfo.php',
               data:childrest,
               headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
           }).then(function successCallback(response){
               //console.log('child',response.data.result);
               if(response.data.isExist==1){
                   $scope.example14data = [];
                   angular.forEach(response.data.result,function(obj){
                       var data={'label':obj.child_rest_name,'id':obj.child_member_id};
                       $scope.example14data.push(data);
                   })
                    $scope.showchild=true;
                  // console.log('examl',$scope.example14model);
               }else{
                   $scope.showchild=false;
               }
           },function errorCallback(response) {
           })
   }
   $scope.getCustomComment=function(subcatid,memberid,from_day,to_day){
      if(from_day=='1' && to_day=='7'){
           var difference=7;
      }else{
           var difference = Math.abs((parseInt($scope.daysTo.value)) -(parseInt($scope.daysFrom.value))) + 1;
      }
      var fetchdata=$.param({'action':'getCustomComment','subcat_id':subcatid,'member_id':memberid,'from_day':from_day,'to_day':to_day,'diff':difference});
      $http({
        method:'POST',
        url:'php/customerInfo.php',
        data:fetchdata,
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }).then(function successCallback(response){
       // console.log('commnt data',response.data);
        if(response.data[0].isComment==true){
          $scope.dcomment=response.data[0].comment;
        }else{
          $scope.dcomment='';
        }
      },function errorCallback(response) {
      })
   }
   $scope.getMultipleSpecials=function(memberid,subcatid){
       var restdata=$.param({'rest_name':memberid,'action':'mulspecialsub'});
       $http({
           method:'POST',
           url:'php/customerInfo.php',
           data:restdata,
           headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
       }).then(function successCallback(response){
            console.log('sub res',response.data);
            $scope.listOfSubcat=[];
            $scope.example16data=[];
            angular.forEach(response.data,function(obj){
               if(obj.type==2){
                    typefind=1;
               }else{
                    typefind=0;
               }
               // var data={'name':obj.cat_name+':'+obj.subcat_name,'value':obj.subcat_id};
               // $scope.listOfSubcat.push(data);
               var data={'label':obj.cat_name+':'+obj.subcat_name,'id':obj.subcat_id};
               $scope.example16data.push(data);
            })
            $scope.sortingSpecial($scope.example16data, 'label');
            var data1={'id':subcatid};
            $scope.example16model.push(data1);
            //console.log('example',$scope.example16model);
        if(typefind==1){
            $scope.localIMage=false;
        }else{
          $scope.localIMage=true;  
        }
       },function errorCallback(response) {
       })
   }
   $scope.editSpecialImageData=function(subcatid,memberid){
       var viewData=$.param({'action':'editSpecialGalImage','subcat_id':subcatid,'member_id':memberid});
       $http({
           method:'POST',
           url:'php/customerInfo.php',
           data:viewData,
           headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
       }).then(function successCallback(response){
           console.log('edit',response.data);
           $scope.restaurant={};
           $scope.subcat={};
           prevChildIds=[];
           $scope.restaurant.value=response.data[0].member_id;
           //$scope.subcat.value=response.data[0].subcat_id;
           //var data={'id':response.data[0].subcat_id};
           //$scope.example16model.push(data);
           $scope.getMultipleSpecials(response.data[0].member_id,response.data[0].subcat_id);
           //prevSubCatId=response.data[0].subcat_id;
           var data={'id':response.data[0].subcat_id};
           prevSubCatId.push(data);
           $scope.daysFrom.value=response.data[0].from_day;
           $scope.daysTo.value=response.data[0].to_day;
           $scope.date1=response.data[0].date_from;
           $scope.date2=response.data[0].date_to;
           $scope.getCustomComment(subcatid,memberid,response.data[0].from_day,response.data[0].to_day);
           if(response.data[0].from_day=='1' && response.data[0].to_day=='7'){
               var difference=7;
           }else{
               var difference = Math.abs((parseInt($scope.daysTo.value)) -(parseInt($scope.daysFrom.value))) + 1;
               // var difference=Math.abs((parseInt(parseInt($scope.daysTo.value)/7))*7 + (parseInt($scope.daysTo.value)%7) )-((parseInt($scope.daysFrom.value)%7) )+1;
              //var difference=Math.abs((parseInt($scope.daysTo.value)-parseInt($scope.daysFrom.value))%7)+1;
           }
           $scope.getPeerBusinessList(response.data[0].member_id,response.data[0].peer_business);
           if(response.data[0].peer_business==1){
               var peerIDS=[];
               var str_array=response.data[0].peer_business_ids.split(',');
               angular.forEach(str_array,function(obj){
                  var peerData={'peerids':obj.replace(/^\s*/, "").replace(/\s*$/, "")};
                  peerIDS.push(peerData);
               })
               var childData=$.param({'action':'getChildBusiness','peer':peerIDS});
               $http({
                   method:'POST',
                   url:"php/customerInfo.php",
                   data:childData,
                   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
               }).then(function successCallback(response){
                   //console.log('peer',response.data);
                   angular.forEach(response.data,function(obj){
                       var data={'id':obj.member_id};
                       $scope.example14model.push(data);
                       prevChildIds.push(data);
                   })
                  // console.log('peer',$scope.example14data);
                   $scope.showchild=true;
               },function errorCallback(response) {
               })
           }
          // console.log('prev child', prevChildIds);
           $scope.diffValue=difference;
           $scope.prevFromday= $scope.daysFrom.value;
           var specialImages=[];
           response.data.forEach(function (a) {
                if (!this[a.subcat_id]) {
                    this[a.subcat_id] = {subcat_id:a.subcat_id, special:[]};
                     specialImages.push(this[a.subcat_id]);
                }
                this[a.subcat_id].special.push({ image: a.image,gallery_image:a.gallery_image,comment:a.comment,'subcat_id':a.subcat_id,'subcat_name':a.subcat_name});
            }, Object.create(null));
          // console.log('special',specialImages);
           var outputList=[];
           for(var i = 0; i < specialImages[0]['special'].length; i++){
              // console.log('x',x);
               if (!outputList.some(function (x) { return x.image === specialImages[0]['special'][i].image; })){
               var data={ image: specialImages[0]['special'][i].image,gallery_image:specialImages[0]['special'][i].gallery_image,comment:specialImages[0]['special'][i].comment,subcat_id:specialImages[0]['special'][i].subcat_id,'subcat_name':specialImages[0]['special'][i].subcat_name};
               outputList.push(data);
               }
           }
         // console.log('out',outputList);
          // console.log('tf', $scope.mulImage);
           for(var i=0;i<outputList.length;i++){
               if(i==0){
                   if(outputList[i].gallery_image=='false'){
                       $scope.mulImage[0]['filename']=outputList[i].image;
                       $scope.mulImage[0]['comment']=outputList[i]['comment'];
                       galImg=true;
                   }else{
                       galFlag=true;
                       galImgSave=true;
                       var data={'image':outputList[i].image,'description':outputList[i]['comment'],'gallery_image':outputList[i]['gallery_image'],'checked':outputList[i]['gallery_image'],'subcat_id':outputList[i]['subcat_id'],'subcat_name':outputList[i]['subcat_name']};
                       $scope.checkedValue.push(data);
                   }
               }else{
                   if(outputList[i].gallery_image=='false'){
                       if($scope.mulImage.length==1 &&  $scope.mulImage[0]['filename']==''){
                            $scope.mulImage[0]['filename']=outputList[i].image;
                            $scope.mulImage[0]['comment']=outputList[i]['comment'];
                            galImg=true;
                       }else{
                            $scope.mulImage.push({'image':null,'filename':outputList[i].image,'comment':outputList[i]['comment']});
                       }
                   }else{
                       galFlag=true;
                       galImgSave=true;
                       var data={'image':outputList[i].image,'description':outputList[i]['comment'],'gallery_image':outputList[i]['gallery_image'],'checked':outputList[i]['gallery_image'],'subcat_id':outputList[i]['subcat_id'],'subcat_name':outputList[i]['subcat_name']};
                       $scope.checkedValue.push(data);
                   }
               }
           }
          var temp=[];
          $scope.mulImage=$scope.mulImage.filter(function(x) {
              if (temp.indexOf(x.filename) < 0) {
                   temp.push(x.filename);
                   return true;
             }
             return false;
          })
           console.log('checkedit', $scope.checkedValue);
           $scope.buttonName="Update";
		   $scope.ClearbuttonName="Cancel";
		  $scope.showCancel=true;
       },function errorCallback(response) {
       })
       
       
   }
   $scope.clearImageData=function(){
	    $state.go('dashboard.customer.addimage',{}, { reload: true });
    }
   $scope.deleteProductImageData=function(subcat_id,member_id,from_day,to_day){
       if(from_day=='1' && to_day=='7'){
           var difference=7;
       }else{
           //var difference=Math.abs((parseInt(to_day)%7)-(parseInt(from_day)%7))+1;
           var difference = Math.abs((parseInt(to_day)) -(parseInt(from_day))) + 1;
       }
       var delData=$.param({'action':'delGalSpecImageData','member_id':member_id,'subcat_id':subcat_id,'from_day':from_day,'to_day':to_day,'diff':difference});
       var mesg=$window.confirm("Are you sure to delete this record ?");
       if(mesg){
           $http({
               method:'POST',
               url:'php/customerInfo.php',
               data:delData,
               headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
           }).then(function successCallback(response){
               console.log('del',response.data);
               alert(response.data['msg']);
               $state.go('dashboard.customer.addimage',{}, { reload: true });
           },function errorCallback(response) {
               alert(response.data['msg']);
               $state.go('dashboard.customer.addimage',{}, { reload: true });
           })
       }
   }
})
addImage.filter('startsLetter', function () {
    return function (items, search) {
		//console.log('items',items);
		if(items != undefined){
			if (!search) {
			return items;
		  }
		  search = search.toLowerCase();
		  return items.filter(function(element) {
			return element.rest_name.toLowerCase().indexOf(search) != -1;
		  });
		}
    };
});
